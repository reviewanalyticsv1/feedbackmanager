<!DOCTYPE html>

<html ng-app="pixeladmin">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="Description" content="Meaily. Find Meal Plans. Write Reviews. Ask Questions. ">
  <title page-title> Meaily.</title>
  <meta name="author" content=" Meaily.">
  
   <!-- External stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,800,300&subset=latin" rel="stylesheet" type="text/css" />
         <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
     
  
   <link href="/dependent/css/bootstrap.min.css?v=20201212" rel="stylesheet">
    <link href="/dependent/css/pixeladmin.min.css?v=20201212" rel="stylesheet">
    <link href="/dependent/css/widgets.min.css?v=20201212" rel="stylesheet">
	<link href="/dependent/css/themes/clean.min.css?v=20201212" rel="stylesheet">
        <link href="/dependent/css/landingpage.css?v=20201212" rel="stylesheet" />
        <script src='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.css' rel='stylesheet' />
<link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css" type="text/css" />
        <link href="/dependent/css/style.css?v=20201212" rel="stylesheet" />

</head>
<!-- ControllerAs syntax -->
<body ng-controller="MainCtrl as main">
  <!-- Main view -->
  <!-- You can use current state name for better customization -->
  <span ng-hide="true" id="error">${error}</span>
  <span ng-hide="true" id="logout">${logout}</span>
  <ui-view class="{{ $state.current.name }}"></ui-view>
 
  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- General scripts -->
  <script src="dependent/js/angular.js?v=20201212"></script>
  <script src="dependent/js/bootstrap.js?v=20201212"></script>
  <script src="dependent/js/ui-bootstrap.js?v=20201212"></script>
  <script src="dependent/js/pixeladmin/util.js?v=20201212"></script>
  <script src="dependent/js/pixeladmin/pixeladmin.js?v=20201212"></script>
  
    <!-- Moments.-->
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="/dependent/js/libs/daterangepicker.js?v=20201212"></script>

  <!-- Perfect scrollbar -->
  <script src="dependent/js/libs/perfect-scrollbar.jquery.js?v=20201212"></script>
  <script src="dependent/js/pixeladmin/extensions/perfect-scrollbar.jquery.js?v=20201212"></script>
  <script src="dependent/js/libs/angular-perfect-scrollbar.js?v=20201212"></script>

  <!-- PxNavbar, PxNav and PxFooter -->
  <script src="dependent/js/pixeladmin/plugins/px-navbar.js?v=20201212"></script>
  <script src="dependent/js/pixeladmin/directives/angular-px-navbar.js?v=20201212"></script>
  <script src="dependent/js/pixeladmin/plugins/px-nav.js?v=20201212"></script>
  <script src="dependent/js/pixeladmin/directives/angular-px-nav.js?v=20201212"></script>
  <script src="dependent/js/pixeladmin/plugins/px-footer.js?v=20201212"></script>
  <script src="dependent/js/pixeladmin/directives/angular-px-footer.js?v=20201212"></script>
  
<script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-animate.min.js'></script>
<script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-aria.min.js'></script>
<script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-messages.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.8/angular-material.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-cookies.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
  <!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
<script src="https://unpkg.com/@mapbox/mapbox-sdk/umd/mapbox-sdk.min.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>

  <!-- Application -->
  <script src="dependent/public/js/app.js?v=20201212"></script>
  <script src="dependent/public/js/config.js?v=20201212"></script>
  <script src="dependent/public/js/directives.js?v=20201212"></script>
  <script src="dependent/public/js/controllers/main.js?v=20201212"></script>
<script src="dependent/js/pace/pace.min.js?v=20201212"></script>
</body>
</html>
