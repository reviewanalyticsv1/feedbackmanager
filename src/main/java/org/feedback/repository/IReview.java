package org.feedback.repository;

import java.util.Date;
import java.util.List;

import org.feedback.pojo.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IReview extends JpaRepository<Review, Long> {

	List<Review> findByStoreIdAndEnableTrueAndVerifiedTrueOrderByReviedOnDesc(Long id);

	List<Review> findByReviewerIdAndEnableTrueAndVerifiedTrue(Long id);

	List<Review> findAllByReviedOnGreaterThanEqualAndReviedOnLessThanEqualAndEnableTrueAndVerifiedTrue(Date start, Date end);
	
	List<Review> findByStoreIdAndEnableTrueOrderByReviedOnDesc(Long id);

	List<Review> findByReviewerIdAndEnableTrue(Long id);

	List<Review> findAllByReviedOnGreaterThanEqualAndReviedOnLessThanEqualAndEnableTrue(Date start, Date end);

	}
