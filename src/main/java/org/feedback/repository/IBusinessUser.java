package org.feedback.repository;

import org.feedback.pojo.BusinessUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
public interface IBusinessUser extends JpaRepository<BusinessUser, Long> {

	BusinessUser findByUsername(String username);

	boolean existsByUsername(String username);

}
