package org.feedback.repository;

import java.util.Date;
import java.util.List;

import org.feedback.pojo.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IQuery extends JpaRepository<Query, Long> {

	List<Query> findByStoreIdAndEnableTrueAndVerifiedTrueOrderByQueriedOnDesc(Long valueOf);

	List<Query> findByReviewerIdAndEnableTrueAndVerifiedTrue(Long id);

	List<Query> findAllByQueriedOnGreaterThanEqualAndQueriedOnLessThanEqualAndEnableTrueAndVerifiedTrue(Date start, Date end);

	List<Query> findByStoreIdAndEnableTrueOrderByQueriedOnDesc(Long valueOf);

	List<Query> findByReviewerIdAndEnableTrue(Long id);

	List<Query> findAllByQueriedOnGreaterThanEqualAndQueriedOnLessThanEqualAndEnableTrue(Date start, Date end);
}
