package org.feedback.repository;

import java.util.Date;
import java.util.List;

import org.feedback.pojo.SubscriptionCustomer;
import org.feedback.utility.RMUtil.CUSTOMER_STATUS;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface ISubscriptionCustomer extends JpaRepository<SubscriptionCustomer, Long> {
	List<SubscriptionCustomer> findAllByStoreIdOrderByCreatedOnAsc(Long storeId);

	List<SubscriptionCustomer> findAllByStoreIdAndStatusOrderByCreatedOnAsc(Long id,CUSTOMER_STATUS status);

	List<SubscriptionCustomer> findByCustomerIdOrderByCreatedOnAsc(Long id);

	List<SubscriptionCustomer> findAllByCreatedOnGreaterThanEqualAndCreatedOnLessThanEqual(Date start, Date end);
}
