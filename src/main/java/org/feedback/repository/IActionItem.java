package org.feedback.repository;

import java.util.List;
import java.util.Optional;

import org.feedback.pojo.ActionItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IActionItem extends JpaRepository<ActionItem, Long> {

	List<ActionItem> findByUserIdOrderByAddedOnDesc(Long storeId);

	void deleteByReviewId(Long reviewId);

	Optional<ActionItem> findByReviewId(Long id);

}
