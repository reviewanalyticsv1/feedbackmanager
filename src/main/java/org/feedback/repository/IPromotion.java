package org.feedback.repository;

import java.util.List;

import org.feedback.pojo.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IPromotion extends JpaRepository<Promotion, Long> {

	List<Promotion> findByStoreIdOrderByCreatedOnDesc(Long id);

}
