package org.feedback.repository;

import java.util.List;

import org.feedback.pojo.JobPosting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IJobPosting extends JpaRepository<JobPosting, Long> {

	List<JobPosting> findAllByBusinessUserIdOrderByModifiedOnDesc(Long id);

	boolean existsByName(String name);


}
