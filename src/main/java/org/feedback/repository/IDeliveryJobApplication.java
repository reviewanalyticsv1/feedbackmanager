package org.feedback.repository;

import java.util.List;

import org.feedback.pojo.DeliveryJobApplication;
import org.feedback.utility.RMUtil.JOBSTATUS;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IDeliveryJobApplication extends JpaRepository<DeliveryJobApplication, Long> {

	List<DeliveryJobApplication> findByDeliveryDriverIdOrderByAppliedOnDesc(Long driverId);

	boolean existsByJobPostingIdAndDeliveryDriverId(Long jobId, Long driverId);

	List<DeliveryJobApplication> findByDeliveryDriverIdAndStatusOrderByAppliedOnDesc(Long driverId, JOBSTATUS status);

	List<DeliveryJobApplication> findByJobPostingIdOrderByAppliedOnDesc(Long id);

	List<DeliveryJobApplication> findByBusinessUserIdOrderByAppliedOnDesc(Long id);

	List<DeliveryJobApplication> findByJobPostingIdAndStatusOrderByAppliedOnDesc(Long id, JOBSTATUS status);

}
