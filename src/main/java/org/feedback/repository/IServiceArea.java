package org.feedback.repository;

import java.util.List;
import java.util.Set;

import org.feedback.pojo.ServiceArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IServiceArea extends JpaRepository<ServiceArea, Long> {

	Set<ServiceArea> findByCountryNameContaining(String country);

	Set<ServiceArea> findAllByCountryName(String countryName);

	List<ServiceArea> findAllByCityNameAndStateNameAndCountryName(String cityName,String stateName, String countryName );

	boolean existsByCityNameAndStateNameAndCountryName(String cityName, String stateName, String countryName);
}
