package org.feedback.repository;

import java.util.List;
import java.util.Optional;

import org.feedback.pojo.Reviewer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IReviewer extends JpaRepository<Reviewer, Long> {


	List<Reviewer> findByEmail(String email);

	Optional<Reviewer> findByEmailAndFirstNameAndLastNameAndPhoneNumber(String email, String firstName, String lastName,
			String phoneNumber);

}
