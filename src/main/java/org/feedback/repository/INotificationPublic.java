package org.feedback.repository;


import java.util.List;

import org.feedback.pojo.NotificationPublic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface INotificationPublic extends JpaRepository<NotificationPublic, Long> {

	List<NotificationPublic> findAllByTokenId(Long id);

	List<NotificationPublic> findAllByTokenIdAndIsReadFalse(Long id);

	 

}

