package org.feedback.repository;

import java.util.List;

import org.feedback.pojo.EmailSubscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IEmailSubscription extends JpaRepository<EmailSubscription, Long> {

	List<EmailSubscription> findByStoreId(Long storeId);

	void deleteByEmailAndStoreId(String email, Long client);

	void deleteByEmail(String email);

	boolean existsByEmailAndStoreId(String email, Long client);

	boolean existsByEmail(String email);

	List<EmailSubscription> findByStoreIdOrderByCreatedOnDesc(Long valueOf);

	List<EmailSubscription> findByEmail(String email);

}
