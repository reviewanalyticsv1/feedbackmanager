package org.feedback.repository;

import java.util.List;
import java.util.Optional;

import org.feedback.pojo.DeliveryDriver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IDeliveryDriver extends JpaRepository<DeliveryDriver, Long> {

	DeliveryDriver findByReviewerId(Long id);
    
	Optional<DeliveryDriver> findByUsername(String username);
	
	boolean existsByUsername(String username);

	List<DeliveryDriver> findAllByAvailableTrue();

}
