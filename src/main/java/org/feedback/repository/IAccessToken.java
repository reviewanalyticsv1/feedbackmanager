package org.feedback.repository;

import java.util.Optional;

import org.feedback.pojo.AccessToken;
import org.feedback.utility.RMUtil.ACCESS_TYPE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IAccessToken extends JpaRepository<AccessToken, Long> {

	Optional<AccessToken> findByEmailAndType(String email, ACCESS_TYPE type);

}
