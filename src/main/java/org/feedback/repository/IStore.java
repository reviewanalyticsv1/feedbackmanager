package org.feedback.repository;


import java.util.List;

import org.feedback.pojo.BusinessUser;
import org.feedback.pojo.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IStore extends JpaRepository<Store, Long> {

	List<Store> findAllByStoreNameContainingAndActiveTrue(String storeName);

	List<Store> findByBusinessUserId(Long id);

	boolean existsByStoreNameAndBusinessUser(String storeName,BusinessUser user);



}

