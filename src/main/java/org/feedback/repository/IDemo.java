package org.feedback.repository;

import org.feedback.pojo.Demo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IDemo extends JpaRepository<Demo, Long> {

}
