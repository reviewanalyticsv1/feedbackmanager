package org.feedback.repository;

import java.util.Optional;

import org.feedback.pojo.PromotionCounter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IPromotionCounter extends JpaRepository<PromotionCounter, Long> {

	Optional<PromotionCounter> findByStoreId(Long id);

}
