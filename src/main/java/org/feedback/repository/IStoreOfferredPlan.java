package org.feedback.repository;

import java.util.List;

import org.feedback.pojo.StoreOfferredPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface IStoreOfferredPlan extends JpaRepository<StoreOfferredPlan, Long> {

	List<StoreOfferredPlan> findByStoreIdAndActiveTrue(Long id);

	List<StoreOfferredPlan> findByStoreId(Long id);


}
