package org.feedback.repository;


import java.util.List;

import org.feedback.pojo.NotificationBusiness;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true,rollbackFor = Exception.class)
public interface INotificationBusiness extends JpaRepository<NotificationBusiness, Long> {

	List<NotificationBusiness> findByStoreIdAndIsReadFalseOrderByTimeDesc(Long storeId);

	List<NotificationBusiness> findByJobIdAndIsReadFalseOrderByTimeDesc(Long id);

	List<NotificationBusiness> findByStoreIdOrderByTimeDesc(Long id);
	List<NotificationBusiness> findByJobIdOrderByTimeDesc(Long id);


}

