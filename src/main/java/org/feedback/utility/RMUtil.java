package org.feedback.utility;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.feedback.pojo.BusinessUser;
import org.feedback.pojo.EmailNotificationObject;
import org.feedback.pojo.Review;
import org.feedback.pojo.ServiceArea;
import org.feedback.pojo.StoreOfferredPlan;
import org.feedback.pojo.UserTemp;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class RMUtil {

	public static final String REVIEW_STRING_SPLITTER = "#";
	public static final Double POSITIVE_REVIEW_THRESHOLD = 3.0;
	public static final int PASSWORD_LENGTH = 16;
	public static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
	public static final int SEARCH_KEYWORD_CAPACITY = 15;

	public static enum PLANS {
		SMALL_BUSINESS, ENTERPRISE
	}

	public static enum MEMBERSHIP {
		FREE, BUSINESS
	}

	public static enum ACCESS_TYPE {
		DRIVER, PUBLIC
	}

	public enum NOTIFICATIONS {
		REVIEW, QUERY, MEALPLAN, EMAIL, BILLING, DRIVER
	}

	public enum JOBSTATUS {
		APPLIED, REJECTED, WITHDRAWN, SELECTED
	}

	public enum DEMO_STATUS {
		NEW, COMPLETED, CANCELLED
	}

	public static enum CUSTOMER_STATUS {
		NEW, ACTIVE, INACTIVE, CANCELLED
	}

	public static enum ACCESS_RIGHT {
		GET_JOBS, GET_DRIVERS, ADD_STORE, ADD_SUBSCRIBE_MEAL, ADD_CUSTOMER,
	}

	public static BusinessUser getSessionedUser() {
		BusinessUser sessionedUser = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof BusinessUser) {
			sessionedUser = (BusinessUser) principal;
		}
		return sessionedUser;
	}

	public static String getSearchPageSizes(Integer numberOfPages) {
		String pageString = "";
		if (numberOfPages == 0)
			return "1";

		for (int i = 0; i < numberOfPages + 1; i++)
			pageString += String.valueOf(i + 1) + ",";

		if (pageString.endsWith(","))
			pageString = pageString.substring(0, pageString.length() - 1);

		return pageString;
	}

	public static Map<String, Object> getMap(Object obj) {
		Map<String, Object> data = new HashMap<String, Object>();
		data = gson.fromJson(gson.toJson(obj), Map.class);
		return data;
	}

	public static Review convertToReview(String reviewObjectString) {
		String[] reviewArray = reviewObjectString.split(RMUtil.REVIEW_STRING_SPLITTER);
		Review reviewObject = null;
		/*
		 * if (reviewArray.length >= 11) reviewObject = new Review(reviewArray[0],
		 * reviewArray[1], reviewArray[2], reviewArray[3], reviewArray[4],
		 * reviewArray[5]);
		 */
		return reviewObject;
	}

	public static boolean isExpired(String promotionStartDate, String promotionEndDate) {
		org.joda.time.format.DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("M/d/Y");
		DateTime startTime = DateTime.parse(promotionStartDate, dateTimeFormatter);
		DateTime endTime = DateTime.parse(promotionEndDate, dateTimeFormatter);
		if (startTime.isBeforeNow() && endTime.isAfterNow())
			return true;
		else
			return false;
	}

	public static boolean isExpired(Timestamp expiryDate) {
		return expiryDate.before(new Timestamp(System.currentTimeMillis()));
	}

	public static Timestamp getDate(String date) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		return new Timestamp(simpleDateFormat.parse(date).getTime());

	}

	public static String getEmailTemplate(EmailNotificationObject emailNotificationObject) {
		StringBuffer stringBuffer = new StringBuffer();

		stringBuffer.append(
				"<div style='display: block; border: 1px solid #e2e2e2; background-color: #51bf99!important; padding: 20px; font-family: Tahoma,Geneva,sans-serif;'>")
				.append("<div style='padding: 10px;'>").append("<p><strong>Meaily</strong></p>")
				.append("<p><span style='color: #000000;'>Dear ").append(emailNotificationObject.getSenderName())
				.append(",</span></p>").append("<p><span style='color: #000000;'>")
				.append(emailNotificationObject.getEmailContent()).append("</span></p><p>&nbsp;</p><p>&nbsp;</p>")
				.append("<p><span style='color: #efefef; font-size: 12px;'>")
				.append("This message was sent from Meaily and intended for chirag-parmar@live.com."
						+ "<a href='http://www.meaily.com/unsubscribe?q=" + emailNotificationObject.getSendTo()
						+ "'>Unsubscribe</a>," + "If you prefer not to receive any communication emails from Meaily. "
						+ "If you require any assistance, We can be reachable at <a href='mailto:support@meaily.com'>support@meaily.com</a>. <br>"
						+ "We are strong believers of doing business transperantly. Please access your data stored on our platform at "
						+ "<a href='http://www.meaily.com/login#!/login/authpublic'>Access Your Data</a>.")
				.append("</span></p>").append("</div>").append("</div>");

		return stringBuffer.toString();
	}

	public static BusinessUser getBusinessUser(UserTemp userTemp, String paymentId) {
		BusinessUser businessUser = new BusinessUser();
		businessUser.setAccountNonExpired(userTemp.isAccountNonExpired());
		businessUser.setAccountNonLocked(userTemp.isAccountNonLocked());
		businessUser.setAddress(userTemp.getAddress());
		businessUser.setRoles(userTemp.getRoles());
		businessUser.setClientBusinessPhoneNumber(userTemp.getPhoneNumber());
		businessUser.setClientBusinessUrl(userTemp.getBusinessUrl());
		businessUser.setClientEmail(userTemp.getEmail());
		businessUser.setEmail(userTemp.getEmail());
		businessUser.setClientType(userTemp.getType());
		businessUser.setCreatedOn(userTemp.getCreatedOn());
		businessUser.setCredentialsNonExpired(userTemp.isCredentialsNonExpired());
		businessUser.setEnabled(userTemp.isEnabled());
		businessUser.setId(userTemp.getId());
		businessUser.setModifiedBy(userTemp.getModifiedBy());
		businessUser.setModifiedOn(userTemp.getModifiedOn());
		businessUser.setNotifyAddedAsCompetitor(userTemp.isNotifyAddedAsCompetitor());
		businessUser.setNotifyNewReport(userTemp.isNotifyNewReport());
		businessUser.setNotifyNewReview(userTemp.isNotifyNewReview());
		businessUser.setPassword(userTemp.getPassword());
		businessUser.setSubscription(userTemp.isSubscription());
		businessUser.setUsername(userTemp.getUsername());
		// businessUser.setWorkingHours(userTemp.getWorkingHours());
		businessUser.setTrialCompleted(false);
		businessUser.setPaymentId(paymentId);
		return businessUser;
	}

	public static String getCookie(HttpServletRequest request, String cookieName) {
		String cookieValue = null;
		for (Cookie cook : request.getCookies()) {
			if (cook.getName().equalsIgnoreCase(cookieName)) {
				cookieValue = cook.getValue();
				break;
			}
		}
		return cookieValue;
	}

	public static Cookie createCookie(String name, String value, int duration) {
		Cookie cok = new Cookie(name, value);
		cok.setMaxAge(duration);
		cok.setHttpOnly(true);
		// cok.setDomain("meaily.com");
		return cok;
	}

	public static boolean getServiceArea(Set<ServiceArea> serviceArea, ServiceArea location) {
		return serviceArea.stream()
				.filter(service -> service.getCityName().equalsIgnoreCase(location.getCityName())
						&& service.getStateName().equalsIgnoreCase(location.getStateName())
						&& service.getCountryName().equalsIgnoreCase(location.getCountryName()))
				.count() > 0;
	}

	public static boolean isBetween(Timestamp scheduleDate, Timestamp startDate, Timestamp endDate) {
		return (startDate.equals(scheduleDate) || startDate.before(scheduleDate))
				&& (endDate.equals(scheduleDate) || endDate.after(scheduleDate));
	}

	public static String generateToken() {
		String[] charArray = new String[] { "A", "B", "C", "1", "D", "E", "{", "F", "G", "H", "2", "%", "I", "J", "K",
				"L", "M", "_", "N", "3", "+", "=", "O", "P", "[", "Q", "R", "S", "$", "(", "T", "U", "V", ")", "W", "}",
				"X", "~", "Y", "Z", "a", "b", "c", "d", "9", "e", "]", "f", "g", "h", "i", "j", "k", "l", "@", "6", "m",
				"n", "o", "p", "!", "q", "r", "s", "#", "t", "u", "v", "w", "x", "-", "y", "z" };
		Random random = new Random();
		StringBuffer password = new StringBuffer();
		for (int i = 0; i < RMUtil.PASSWORD_LENGTH; i++)
			password.append(charArray[random.nextInt(charArray.length)]);
		return password.toString();
	}

	public static boolean checkDayAvailability(StoreOfferredPlan storeOfferredPlan, Timestamp scheduleDate) {
		String day = new SimpleDateFormat("EEEE").format(scheduleDate.getTime());
		try {
			return (boolean) StoreOfferredPlan.class.getMethod("isAvailableOn" + day).invoke(storeOfferredPlan);
		} catch (Exception ex) {
			ex.printStackTrace();
			return true;
		}
	}

	public static boolean isAllowed(ACCESS_RIGHT rights, BusinessUser user) {
		boolean flag = true;
		switch (rights) {
		case GET_JOBS:
			flag = user.isAddJobs();
			break;
		case GET_DRIVERS:
			flag = user.isAddDriver();
			break;
		case ADD_CUSTOMER:
			flag = user.isAddCustomer();
			break;
		case ADD_STORE:
			flag = user.isAddStore();
			break;
		case ADD_SUBSCRIBE_MEAL:
			flag = user.isAddMealPlan();
			break;
		default:
			flag = true;
			break;
		}

		return flag;
	}

	public static String getCurrency(String countryName) {
		String currency = "CAD";
		switch (countryName.toUpperCase()) {
		case "CANADA":
			currency = "CAD";
			break;
		case "UNITED STATES":
			currency = "USD";
			break;
		case "AUSTRALIA":
			currency = "AUD";
			break;
		case "INDIA":
			currency = "INR";
			break;
		case "UNITED KINGDOM":
			currency = "GBR";
			break;
		default:
			currency = "CAD";
			break;
		}

		return currency;
	}

}
