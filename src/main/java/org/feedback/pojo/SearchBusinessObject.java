package org.feedback.pojo;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonIgnoreProperties(ignoreUnknown = true, allowGetters = false, allowSetters = false)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class SearchBusinessObject {

	
	@JsonProperty("clientName")
	private String clientName;

	
	@JsonProperty("clientType")
	private String clientType;

	
	@JsonProperty("clientEmail")
	private String clientEmail;

	
	@JsonProperty("clientBusinessPhoneNumber")
	private String clientBusinessPhoneNumber;

	
	@JsonProperty("address")
	private Address address;

	
	public SearchBusinessObject() {
		super();
	}

	
	public String getClientName() {
		return clientName;
	}

	
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	
	public String getClientType() {
		return clientType;
	}

	
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	
	public String getClientEmail() {
		return clientEmail;
	}

	
	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}

	
	public String getClientBusinessPhoneNumber() {
		return clientBusinessPhoneNumber;
	}

	
	public void setClientBusinessPhoneNumber(String clientBusinessPhoneNumber) {
		this.clientBusinessPhoneNumber = clientBusinessPhoneNumber;
	}

	
	public Address getAddress() {
		return address;
	}

	
	public void setAddress(Address address) {
		this.address = address;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((clientBusinessPhoneNumber == null) ? 0 : clientBusinessPhoneNumber.hashCode());
		result = prime * result + ((clientEmail == null) ? 0 : clientEmail.hashCode());
		result = prime * result + ((clientName == null) ? 0 : clientName.hashCode());
		result = prime * result + ((clientType == null) ? 0 : clientType.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchBusinessObject other = (SearchBusinessObject) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (clientBusinessPhoneNumber == null) {
			if (other.clientBusinessPhoneNumber != null)
				return false;
		} else if (!clientBusinessPhoneNumber.equals(other.clientBusinessPhoneNumber))
			return false;
		if (clientEmail == null) {
			if (other.clientEmail != null)
				return false;
		} else if (!clientEmail.equals(other.clientEmail))
			return false;
		if (clientName == null) {
			if (other.clientName != null)
				return false;
		} else if (!clientName.equals(other.clientName))
			return false;
		if (clientType == null) {
			if (other.clientType != null)
				return false;
		} else if (!clientType.equals(other.clientType))
			return false;
		return true;
	}

}
