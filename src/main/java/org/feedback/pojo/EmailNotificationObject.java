package org.feedback.pojo;

import org.apache.commons.lang3.StringUtils;

public class EmailNotificationObject {

	private String sendTo;

	private String senderName;

	private String subject;

	private String emailContent;

	public EmailNotificationObject() {
		super();
	}

	public EmailNotificationObject(String sendTo, String senderName, String subject, String emailContent) {
		super();
		this.sendTo = sendTo;
		this.senderName = senderName;
		this.subject = subject;
		this.emailContent = emailContent;
	}

	public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public String getSenderName() {
		return StringUtils.isEmpty(senderName) ? "User" : senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEmailContent() {
		return StringUtils.isEmpty(emailContent) ? "You have received notification from Meaily portal." : emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

}
