package org.feedback.pojo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = {"businessUser"})
@Entity
@Table(indexes = {@Index(name = "store",columnList = "storeName")})
public class Store implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String storeName;
	
	@Column(length = 3000)
	private String storeDetail;
	
	private String storeContactNumber;
	@URL
	private String storeUrl;
	@Email
	private String storeEmail;
	@OneToOne(cascade = CascadeType.ALL )
	private Address address;

	@OneToOne(cascade = CascadeType.ALL )
	private WorkingHours workingHours;

	@ManyToMany(cascade = { 
            CascadeType.REFRESH
        },fetch = FetchType.EAGER )
	private Set<ServiceArea> serviceArea;
	
	@Column(columnDefinition = "boolean default true")
	private boolean active;

	@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
	private BusinessUser businessUser;
	
	@OneToOne(cascade = CascadeType.ALL )
	private StorePolicies storePolicies;
	
	public Store() {
		super();
	}

	public Store(StoreTemp store) {
		this.address = store.getAddress();
		this.storeName = store.getStoreName();
		this.storeContactNumber = store.getStoreContactNumber();
		this.storeUrl = store.getStoreUrl();
		this.storeEmail = store.getStoreEmail();
		this.id = store.getId();
		this.workingHours = store.getWorkingHours();
		this.active = store.isActive();
		this.serviceArea = store.getServiceArea();
		this.storeDetail = store.getStoreDetail();
		
	}

	public void addBusinessUserStore(BusinessUser businessUser) {
	   this.businessUser = businessUser;	
	}

	

	public StorePolicies getStorePolicies() {
		return storePolicies;
	}

	public void setStorePolicies(StorePolicies storePolicies) {
		this.storePolicies = storePolicies;
	}

	 

	public Address getAddress() {
		return address;
	}

	public Long getId() {
		return id;
	}

	

	public Set<ServiceArea> getServiceArea() {
		return serviceArea;
	}

	public String getStoreContactNumber() {
		return storeContactNumber;
	}

	public String getStoreEmail() {
		return storeEmail;
	}

	public String getStoreName() {
		return storeName;
	}



	
	public String getStoreUrl() {
		return storeUrl;
	}

	

	public WorkingHours getWorkingHours() {
		return workingHours;
	}

 

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public void setServiceArea(Set<ServiceArea> serviceArea) {
		this.serviceArea = serviceArea;
	}

	public void setStoreContactNumber(String storeContactNumber) {
		this.storeContactNumber = storeContactNumber;
	}

	
	public void setStoreEmail(String storeEmail) {
		this.storeEmail = storeEmail;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	public void setStoreDetail(String storeDetail) {
		this.storeDetail = storeDetail;
	}
	public String getStoreDetail() {
		return this.storeDetail;
	}

	

	public void setStoreUrl(String storeUrl) {
		this.storeUrl = storeUrl;
	}

	public void setWorkingHours(WorkingHours workingHours) {
		this.workingHours = workingHours;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((businessUser == null) ? 0 : businessUser.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((serviceArea == null) ? 0 : serviceArea.hashCode());
		result = prime * result + ((storeContactNumber == null) ? 0 : storeContactNumber.hashCode());
		result = prime * result + ((storeDetail == null) ? 0 : storeDetail.hashCode());
		result = prime * result + ((storeEmail == null) ? 0 : storeEmail.hashCode());
		result = prime * result + ((storeName == null) ? 0 : storeName.hashCode());
		result = prime * result + ((storePolicies == null) ? 0 : storePolicies.hashCode());
		result = prime * result + ((storeUrl == null) ? 0 : storeUrl.hashCode());
		result = prime * result + ((workingHours == null) ? 0 : workingHours.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Store other = (Store) obj;
		if (active != other.active)
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (businessUser == null) {
			if (other.businessUser != null)
				return false;
		} else if (!businessUser.equals(other.businessUser))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (serviceArea == null) {
			if (other.serviceArea != null)
				return false;
		} else if (!serviceArea.equals(other.serviceArea))
			return false;
		if (storeContactNumber == null) {
			if (other.storeContactNumber != null)
				return false;
		} else if (!storeContactNumber.equals(other.storeContactNumber))
			return false;
		if (storeDetail == null) {
			if (other.storeDetail != null)
				return false;
		} else if (!storeDetail.equals(other.storeDetail))
			return false;
		if (storeEmail == null) {
			if (other.storeEmail != null)
				return false;
		} else if (!storeEmail.equals(other.storeEmail))
			return false;
		if (storeName == null) {
			if (other.storeName != null)
				return false;
		} else if (!storeName.equals(other.storeName))
			return false;
		if (storePolicies == null) {
			if (other.storePolicies != null)
				return false;
		} else if (!storePolicies.equals(other.storePolicies))
			return false;
		if (storeUrl == null) {
			if (other.storeUrl != null)
				return false;
		} else if (!storeUrl.equals(other.storeUrl))
			return false;
		if (workingHours == null) {
			if (other.workingHours != null)
				return false;
		} else if (!workingHours.equals(other.workingHours))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Store [id=" + id + ", storeName=" + storeName + ", storeDetail=" + storeDetail + ", storeContactNumber="
				+ storeContactNumber + ", storeUrl=" + storeUrl + ", storeEmail=" + storeEmail + ", address=" + address
				+ ", workingHours=" + workingHours + ", serviceArea=" + serviceArea + ", active=" + active
				+ ", businessUser=" + businessUser + ", storePolicies=" + storePolicies + "]";
	}

	 


	

	

}
