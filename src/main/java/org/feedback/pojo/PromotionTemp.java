package org.feedback.pojo;

import java.sql.Timestamp;

public class PromotionTemp {

	private Long id;
	private String title;
	private String detail;
	private Timestamp startDate;
	private Timestamp endDate;

	private Long store;

	public PromotionTemp() {
		super();
	}

	public PromotionTemp( Long id,String title, String detail, Timestamp startDate, Timestamp endDate, Long store) {
		super();
		this.id = id;
		this.title = title;
		this.detail = detail;
		this.startDate = startDate;
		this.endDate = endDate;
		this.store = store;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Long getStore() {
		return store;
	}

	public void setStore(Long store) {
		this.store = store;
	}
	
	public Promotion getPromotion() {
		return new Promotion(this.id,this.title, this.detail, this.startDate, this.endDate);
	}
}
