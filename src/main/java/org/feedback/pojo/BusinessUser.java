package org.feedback.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;

import org.feedback.utility.RMUtil.MEMBERSHIP;
import org.hibernate.validator.constraints.URL;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "password", "accountNonExpired", "accountNonLocked", "credentialsNonExpired", "enabled",
		"authorities", "id", "createdOn", "modifiedBy", "modifiedOn" })
@Entity
public class BusinessUser implements UserDetails, Serializable {

	private static final long serialVersionUID = 1L;

	private boolean accountNonExpired;

	private boolean accountNonLocked;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private Address address;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<>();

	@URL
	private String businessUrl;

	@CreatedDate
	private DateTime createdOn;

	private boolean credentialsNonExpired;

	@Email
	private String email;

	private boolean enabled;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToOne(cascade = CascadeType.MERGE, orphanRemoval = true)
	private BusinessUser modifiedBy;

	private DateTime modifiedOn;

	private String name;

	private boolean notifyAddedAsCompetitor;

	private boolean notifyNewReport;

	private boolean notifyNewReview;

	private String password;
	private String paymentId;
	private String phoneNumber;

	private boolean subscription;

	private boolean trialCompleted;

	private String type;

	private String username;

	@Enumerated(EnumType.STRING)
	private MEMBERSHIP membership = MEMBERSHIP.FREE;

	private boolean addStore;
	private boolean addMealPlan;
	private boolean addCustomer;
	private boolean addJobs;
	private boolean addDriver;

	public BusinessUser() {
		super();
		this.membership = MEMBERSHIP.FREE;
	}

	public MEMBERSHIP getMembership() {
		return this.membership;
	}

	public void setMembership(MEMBERSHIP membership) {
		this.membership = membership;
	}

	public boolean isAddStore() {
		return addStore;
	}

	public void setAddStore(boolean addStore) {
		this.addStore = addStore;
	}

	public boolean isAddMealPlan() {
		return addMealPlan;
	}

	public void setAddMealPlan(boolean addMealPlan) {
		this.addMealPlan = addMealPlan;
	}

	public boolean isAddCustomer() {
		return addCustomer;
	}

	public void setAddCustomer(boolean addCustomer) {
		this.addCustomer = addCustomer;
	}

	public boolean isAddJobs() {
		return addJobs;
	}

	public void setAddJobs(boolean addJobs) {
		this.addJobs = addJobs;
	}

	public boolean isAddDriver() {
		return addDriver;
	}

	public void setAddDriver(boolean addDriver) {
		this.addDriver = addDriver;
	}
	
	
	
	 public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	    public Collection<? extends GrantedAuthority> getAuthorities() {
	        Set<Role> roles = this.getRoles();
	        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
	         
	        for (Role role : roles) {
	            authorities.add(new SimpleGrantedAuthority(role.getName()));
	        }
	         
	        return authorities;
	    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (accountNonExpired ? 1231 : 1237);
		result = prime * result + (accountNonLocked ? 1231 : 1237);
		result = prime * result + (addCustomer ? 1231 : 1237);
		result = prime * result + (addDriver ? 1231 : 1237);
		result = prime * result + (addJobs ? 1231 : 1237);
		result = prime * result + (addMealPlan ? 1231 : 1237);
		result = prime * result + (addStore ? 1231 : 1237);
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((businessUrl == null) ? 0 : businessUrl.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + (credentialsNonExpired ? 1231 : 1237);
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (enabled ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((membership == null) ? 0 : membership.hashCode());
		result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (notifyAddedAsCompetitor ? 1231 : 1237);
		result = prime * result + (notifyNewReport ? 1231 : 1237);
		result = prime * result + (notifyNewReview ? 1231 : 1237);
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((paymentId == null) ? 0 : paymentId.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + (subscription ? 1231 : 1237);
		result = prime * result + (trialCompleted ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BusinessUser other = (BusinessUser) obj;
		if (accountNonExpired != other.accountNonExpired)
			return false;
		if (accountNonLocked != other.accountNonLocked)
			return false;
		if (addCustomer != other.addCustomer)
			return false;
		if (addDriver != other.addDriver)
			return false;
		if (addJobs != other.addJobs)
			return false;
		if (addMealPlan != other.addMealPlan)
			return false;
		if (addStore != other.addStore)
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (businessUrl == null) {
			if (other.businessUrl != null)
				return false;
		} else if (!businessUrl.equals(other.businessUrl))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (credentialsNonExpired != other.credentialsNonExpired)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (enabled != other.enabled)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (membership != other.membership)
			return false;
		if (modifiedBy == null) {
			if (other.modifiedBy != null)
				return false;
		} else if (!modifiedBy.equals(other.modifiedBy))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (notifyAddedAsCompetitor != other.notifyAddedAsCompetitor)
			return false;
		if (notifyNewReport != other.notifyNewReport)
			return false;
		if (notifyNewReview != other.notifyNewReview)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (paymentId == null) {
			if (other.paymentId != null)
				return false;
		} else if (!paymentId.equals(other.paymentId))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (subscription != other.subscription)
			return false;
		if (trialCompleted != other.trialCompleted)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	public Address getAddress() {
		return address;
	}

	 

	public String getBusinessUrl() {
		return businessUrl;
	}

	public String getClientBusinessPhoneNumber() {
		return phoneNumber;
	}

	public String getClientBusinessUrl() {
		return businessUrl;
	}

	public String getClientEmail() {
		return email;
	}

	public String getClientName() {
		return name;
	}

	public String getClientType() {
		return type;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public String getEmail() {
		return email;
	}

	public Long getId() {
		return id;
	}

	public BusinessUser getModifiedBy() {
		return modifiedBy;
	}

	public DateTime getModifiedOn() {
		return modifiedOn;
	}

	public String getName() {
		return name;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getType() {
		return type;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public boolean isNotifyAddedAsCompetitor() {
		return notifyAddedAsCompetitor;
	}

	public boolean isNotifyNewReport() {
		return notifyNewReport;
	}

	public boolean isNotifyNewReview() {
		return notifyNewReview;
	}

	public boolean isSubscription() {
		return subscription;
	}

	public boolean isTrialCompleted() {
		return trialCompleted;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	 

	public void setBusinessUrl(String businessUrl) {
		this.businessUrl = businessUrl;
	}

	public void setClientBusinessPhoneNumber(String clientBusinessPhoneNumber) {
		this.phoneNumber = clientBusinessPhoneNumber;
	}

	public void setClientBusinessUrl(String clientBusinessUrl) {
		this.businessUrl = clientBusinessUrl;
	}

	public void setClientEmail(String clientEmail) {
		this.email = clientEmail;
	}

	public void setClientName(String clientName) {
		this.name = clientName;
	}

	public void setClientType(String clientType) {
		this.type = clientType;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setModifiedBy(BusinessUser modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedOn(DateTime modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNotifyAddedAsCompetitor(boolean notifyAddedAsCompetitor) {
		this.notifyAddedAsCompetitor = notifyAddedAsCompetitor;
	}

	public void setNotifyNewReport(boolean notifyNewReport) {
		this.notifyNewReport = notifyNewReport;
	}

	public void setNotifyNewReview(boolean notifyNewReview) {
		this.notifyNewReview = notifyNewReview;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setSubscription(boolean subscription) {
		this.subscription = subscription;
	}

	public void setTrialCompleted(boolean trialCompleted) {
		this.trialCompleted = trialCompleted;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "BusinessUser [accountNonExpired=" + accountNonExpired + ", accountNonLocked=" + accountNonLocked
				+ ", address=" + address  + ", businessUrl=" + businessUrl
				+ ", createdOn=" + createdOn + ", credentialsNonExpired=" + credentialsNonExpired + ", email=" + email
				+ ", enabled=" + enabled + ", id=" + id + ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn
				+ ", name=" + name + ", notifyAddedAsCompetitor=" + notifyAddedAsCompetitor + ", notifyNewReport="
				+ notifyNewReport + ", notifyNewReview=" + notifyNewReview + ", password=" + password + ", paymentId="
				+ paymentId + ", phoneNumber=" + phoneNumber + ", subscription=" + subscription + ", trialCompleted="
				+ trialCompleted + ", type=" + type + ", username=" + username + ", membership=" + membership
				+ ", addStore=" + addStore + ", addMealPlan=" + addMealPlan + ", addCustomer=" + addCustomer
				+ ", addJobs=" + addJobs + ", addDriver=" + addDriver + "]";
	}

}
