package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(value = {"businessUsers"})
public class DeliveryDriver implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Email
	private String username;

	@ManyToOne(cascade = CascadeType.ALL)
	private Reviewer reviewer;

	@ManyToOne(cascade = CascadeType.ALL)
	private Address address;

	@OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
	private License license;

	@ManyToMany(cascade = CascadeType.MERGE )
	private List<ServiceArea> deliveryAreas;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private List<BusinessUser> businessUsers;
	
	@ColumnDefault("true")
	private boolean available;
	
	private Timestamp createdOn;
	
	private Timestamp modifiedOn;


	public DeliveryDriver() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DeliveryDriver(String username,Reviewer reviewer, Address address, License license, List<ServiceArea> deliveryAreas,
			List<BusinessUser> businessUsers) {
		super();
		this.username = username;
		this.reviewer = reviewer;
		this.address = address;
		this.license = license;
		this.deliveryAreas = deliveryAreas;
		this.businessUsers = businessUsers;
		this.available = true;
		this.createdOn = new Timestamp(System.currentTimeMillis());
		this.modifiedOn = this.createdOn;
	}
	
 

	public List<BusinessUser> getBusinessUsers() {
		return businessUsers;
	}

	public void setBusinessUsers(List<BusinessUser> businessUsers) {
		this.businessUsers = businessUsers;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}

	public List<ServiceArea> getDeliveryAreas() {
		return deliveryAreas;
	}

	public void setDeliveryAreas(List<ServiceArea> deliveryAreas) {
		this.deliveryAreas = deliveryAreas;
	}



	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Override
	public String toString() {
		return  reviewer+"" + address + license + " " + deliveryAreas ;
	}

	 

		

}