package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;

@Entity
public class Invitations implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
 
	@Column(length = 500)
	private String message;
	
	private String inviterName;
	
	@Column(length = 150)
	private String address;
 
	@Email
	private String email;

	private String inviteeName;

	private Timestamp invitationTime;

	public Invitations(String message, String inviterName, String address, @Email String email, String inviteeName) {
		super();
		this.message = message;
		this.inviterName = inviterName;
		this.address = address;
		this.email = email;
		this.inviteeName = inviteeName;
		this.invitationTime = new Timestamp(System.currentTimeMillis());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getInviterName() {
		return inviterName;
	}

	public void setInviterName(String inviterName) {
		this.inviterName = inviterName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInviteeName() {
		return inviteeName;
	}

	public void setInviteeName(String inviteeName) {
		this.inviteeName = inviteeName;
	}

	public Timestamp getInvitationTime() {
		return invitationTime;
	}

	public void setInvitationTime(Timestamp invitationTime) {
		this.invitationTime = invitationTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((invitationTime == null) ? 0 : invitationTime.hashCode());
		result = prime * result + ((inviteeName == null) ? 0 : inviteeName.hashCode());
		result = prime * result + ((inviterName == null) ? 0 : inviterName.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invitations other = (Invitations) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (invitationTime == null) {
			if (other.invitationTime != null)
				return false;
		} else if (!invitationTime.equals(other.invitationTime))
			return false;
		if (inviteeName == null) {
			if (other.inviteeName != null)
				return false;
		} else if (!inviteeName.equals(other.inviteeName))
			return false;
		if (inviterName == null) {
			if (other.inviterName != null)
				return false;
		} else if (!inviterName.equals(other.inviterName))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Invitations [id=" + id + ", message=" + message + ", inviterName=" + inviterName + ", address="
				+ address + ", email=" + email + ", inviteeName=" + inviteeName + ", invitationTime=" + invitationTime
				+ "]";
	}
	
	
 
}
