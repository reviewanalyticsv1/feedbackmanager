package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;

import org.feedback.utility.RMUtil;
import org.feedback.utility.RMUtil.ACCESS_TYPE;
import org.hibernate.validator.constraints.Length;

@Entity
public class AccessToken implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Email
	private String email;
	
	@Length(max = RMUtil.PASSWORD_LENGTH)
	private String token;
	
	private Timestamp reset;
	
	@Enumerated(EnumType.STRING)
	private ACCESS_TYPE type;
	
	private boolean agreeTermsAndCondi;

	public AccessToken( ) {
		super();
	}

	
	public AccessToken( @Email String email, @Length(max = 8) String token, Timestamp reset,ACCESS_TYPE type) {
		super();
		this.email = email;
		this.token = token;
		this.type = type;
		this.reset = reset;
		this.agreeTermsAndCondi = true;
	}

	public ACCESS_TYPE getType() {
		return type;
	}


	public boolean isAgreeTermsAndCondi() {
		return agreeTermsAndCondi;
	}


	public void setAgreeTermsAndCondi(boolean agreeTermsAndCondi) {
		this.agreeTermsAndCondi = agreeTermsAndCondi;
	}


	public void setType(ACCESS_TYPE type) {
		this.type = type;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Timestamp getReset() {
		return reset;
	}

	public void setReset(Timestamp reset) {
		this.reset = reset;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (agreeTermsAndCondi ? 1231 : 1237);
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((reset == null) ? 0 : reset.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccessToken other = (AccessToken) obj;
		if (agreeTermsAndCondi != other.agreeTermsAndCondi)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (reset == null) {
			if (other.reset != null)
				return false;
		} else if (!reset.equals(other.reset))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (type != other.type)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "AccessToken [id=" + id + ", email=" + email + ", token=" + token + ", reset=" + reset + ", type=" + type
				+ ", agreeTermsAndCondi=" + agreeTermsAndCondi + "]";
	}

 
	
}
