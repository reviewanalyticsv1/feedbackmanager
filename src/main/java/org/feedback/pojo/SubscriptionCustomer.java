package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.feedback.utility.RMUtil;
import org.feedback.utility.RMUtil.CUSTOMER_STATUS;

@Entity
public class SubscriptionCustomer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@OneToOne(cascade = CascadeType.MERGE, orphanRemoval = true)
	private Reviewer customer;
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private Address address;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
	@JoinColumn(name = "subscription_customer_id")
	private List<SubscriptionPlan> subscriptions;
	
	@Enumerated(EnumType.STRING)
	private CUSTOMER_STATUS status;
	
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name = "store_id")
	private Store store;
	
	@Column(length = 1000)
	private String customerComment;
	
	@Column(length = 1000)
	private String businessComment;
	
	private Timestamp createdOn;
	
	private Timestamp modifiedOn;
	
	public SubscriptionCustomer() {
		super();
		this.createdOn = new Timestamp(System.currentTimeMillis());
		this.modifiedOn = this.createdOn;
	}
	public SubscriptionCustomer(Long id, Reviewer customer, Address address, List<SubscriptionPlan> subscriptions,Store store) {
		super();
		this.id = id;
		this.customer = customer;
		this.address = address;
		this.subscriptions = subscriptions;
		this.status = RMUtil.CUSTOMER_STATUS.NEW;
		this.createdOn = new Timestamp(System.currentTimeMillis());
		this.modifiedOn = this.createdOn;
	}
	public Long getId() {
		return id;
	}
	
	
	public Timestamp getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public String getCustomerComment() {
		return customerComment;
	}
	public void setCustomerComment(String customerComment) {
		this.customerComment = customerComment;
	}
	public String getBusinessComment() {
		return businessComment;
	}
	public void setBusinessComment(String businessComment) {
		this.businessComment = businessComment;
	}
	public CUSTOMER_STATUS getStatus() {
		return status;
	}
	public void setStatus(CUSTOMER_STATUS status) {
		this.modifiedOn = new Timestamp(System.currentTimeMillis());
		this.status = status;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Reviewer getCustomer() {
		return customer;
	}
	public void setCustomer(Reviewer customer) {
		this.customer = customer;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<SubscriptionPlan> getSubscriptions() {
		return subscriptions;
	}
	public void addSubscriptions(SubscriptionPlan subscription) {
		if(this.subscriptions==null)this.subscriptions = new ArrayList<>();
		this.subscriptions.add(subscription);
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public void setSubscriptions(List<SubscriptionPlan> subscriptions) {
		this.subscriptions = subscriptions;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((businessComment == null) ? 0 : businessComment.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((customerComment == null) ? 0 : customerComment.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((store == null) ? 0 : store.hashCode());
		result = prime * result + ((subscriptions == null) ? 0 : subscriptions.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscriptionCustomer other = (SubscriptionCustomer) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (businessComment == null) {
			if (other.businessComment != null)
				return false;
		} else if (!businessComment.equals(other.businessComment))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		if (customerComment == null) {
			if (other.customerComment != null)
				return false;
		} else if (!customerComment.equals(other.customerComment))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		if (status != other.status)
			return false;
		if (store == null) {
			if (other.store != null)
				return false;
		} else if (!store.equals(other.store))
			return false;
		if (subscriptions == null) {
			if (other.subscriptions != null)
				return false;
		} else if (!subscriptions.equals(other.subscriptions))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SubscriptionCustomer [id=" + id + ", customer=" + customer + ", address=" + address + ", subscriptions="
				+ subscriptions + ", status=" + status + ", store=" + store + ", customerComment=" + customerComment
				+ ", businessComment=" + businessComment + ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn
				+ "]";
	}

	

	

}
