package org.feedback.pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StorePolicies implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(length = 3000)
	private String storePolicy;
	@Column(length = 3000)
	private String cancellationPolicy;
	@Column(length = 3000)
	private String refundPolicy;

	public StorePolicies() {
		super();
	}

	public StorePolicies(String storePolicy, String cancellationPolicy, String refundPolicy) {
		super();
		this.storePolicy = storePolicy;
		this.cancellationPolicy = cancellationPolicy;
		this.refundPolicy = refundPolicy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStorePolicy() {
		return storePolicy;
	}

	public void setStorePolicy(String storePolicy) {
		this.storePolicy = storePolicy;
	}

	public String getCancellationPolicy() {
		return cancellationPolicy;
	}

	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}

	public String getRefundPolicy() {
		return refundPolicy;
	}

	public void setRefundPolicy(String refundPolicy) {
		this.refundPolicy = refundPolicy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cancellationPolicy == null) ? 0 : cancellationPolicy.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((refundPolicy == null) ? 0 : refundPolicy.hashCode());
		result = prime * result + ((storePolicy == null) ? 0 : storePolicy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StorePolicies other = (StorePolicies) obj;
		if (cancellationPolicy == null) {
			if (other.cancellationPolicy != null)
				return false;
		} else if (!cancellationPolicy.equals(other.cancellationPolicy))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (refundPolicy == null) {
			if (other.refundPolicy != null)
				return false;
		} else if (!refundPolicy.equals(other.refundPolicy))
			return false;
		if (storePolicy == null) {
			if (other.storePolicy != null)
				return false;
		} else if (!storePolicy.equals(other.storePolicy))
			return false;
		return true;
	}

}
