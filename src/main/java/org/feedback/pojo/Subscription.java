package org.feedback.pojo;



public class Subscription {

	
	private String creditCardNumber;

	
	private String postalCode;

	
	private String cvvNumber;

	
	private String billingCycle;

	
	public Subscription() {
		super();
	}

	
	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	
	public String getPostalCode() {
		return postalCode;
	}

	
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	
	public String getCvvNumber() {
		return cvvNumber;
	}

	
	public void setCvvNumber(String cvvNumber) {
		this.cvvNumber = cvvNumber;
	}

	
	public String getBillingCycle() {
		return billingCycle;
	}

	
	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((billingCycle == null) ? 0 : billingCycle.hashCode());
		result = prime * result + ((creditCardNumber == null) ? 0 : creditCardNumber.hashCode());
		result = prime * result + ((cvvNumber == null) ? 0 : cvvNumber.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subscription other = (Subscription) obj;
		if (billingCycle == null) {
			if (other.billingCycle != null)
				return false;
		} else if (!billingCycle.equals(other.billingCycle))
			return false;
		if (creditCardNumber == null) {
			if (other.creditCardNumber != null)
				return false;
		} else if (!creditCardNumber.equals(other.creditCardNumber))
			return false;
		if (cvvNumber == null) {
			if (other.cvvNumber != null)
				return false;
		} else if (!cvvNumber.equals(other.cvvNumber))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		return true;
	}

}
