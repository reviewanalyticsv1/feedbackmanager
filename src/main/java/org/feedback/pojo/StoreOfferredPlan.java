package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(value = {"store"})
@Table(indexes = {@Index(name = "store_offerred_plan",columnList = "name,description")})
public class StoreOfferredPlan implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;

	@Column(length = 2500)
	private String description;
	private String imageUrl;
	private Double planPricing;
	
	private String billingPeriod;
	
	@Column(columnDefinition = "boolean default true")
	private boolean active;	
	private boolean homeDelivered;
	private boolean takeOut;
	private boolean foodSample;
	private boolean availableOnMonday;
	private boolean availableOnTuesday;
	private boolean availableOnWednesday;
	private boolean availableOnThursday;
	private boolean availableOnFriday;
	private boolean availableOnSaturday;
	private boolean availableOnSunday;
	
	private Timestamp createdOn;
	private Timestamp modifiedOn;
	
	@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
	private Store store;

	public StoreOfferredPlan() {
		super();
		this.createdOn = new Timestamp(System.currentTimeMillis());
		this.modifiedOn = this.createdOn;
	}

	public StoreOfferredPlan( String name, String description, String imageUrl, Double planPricing,
			String billingPeriod, boolean homeDelivered, boolean takeOut, boolean availableOnMonday,
			boolean availableOnTuesday, boolean availableOnWednesday, boolean availableOnThursday,
			boolean availableOnFriday, boolean availableOnSaturday, boolean availableOnSunday,boolean active,Store store,boolean foodSample) {
		super();
		this.name = name;
		this.description = description;
		this.imageUrl = imageUrl;
		this.planPricing = planPricing;
		this.billingPeriod = billingPeriod;
		this.homeDelivered = homeDelivered;
		this.takeOut = takeOut;
		this.availableOnMonday = availableOnMonday;
		this.availableOnTuesday = availableOnTuesday;
		this.availableOnWednesday = availableOnWednesday;
		this.availableOnThursday = availableOnThursday;
		this.availableOnFriday = availableOnFriday;
		this.availableOnSaturday = availableOnSaturday;
		this.availableOnSunday = availableOnSunday;
		this.active = active;
		this.store = store;
		this.foodSample = foodSample;
		this.createdOn = new Timestamp(System.currentTimeMillis());
		this.modifiedOn = this.createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public boolean isFoodSample() {
		return foodSample;
	}

	public void setFoodSample(boolean foodSample) {
		this.foodSample = foodSample;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Double getPlanPricing() {
		return planPricing;
	}

	public void setPlanPricing(Double planPricing) {
		this.planPricing = planPricing;
	}

	public String getBillingPeriod() {
		return billingPeriod;
	}

	public void setBillingPeriod(String billingPeriod) {
		this.billingPeriod = billingPeriod;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isHomeDelivered() {
		return homeDelivered;
	}

	public void setHomeDelivered(boolean homeDelivered) {
		this.homeDelivered = homeDelivered;
	}

	public boolean isTakeOut() {
		return takeOut;
	}

	public void setTakeOut(boolean takeOut) {
		this.takeOut = takeOut;
	}

	public boolean isAvailableOnMonday() {
		return availableOnMonday;
	}

	public void setAvailableOnMonday(boolean availableOnMonday) {
		this.availableOnMonday = availableOnMonday;
	}

	public boolean isAvailableOnTuesday() {
		return availableOnTuesday;
	}

	public void setAvailableOnTuesday(boolean availableOnTuesday) {
		this.availableOnTuesday = availableOnTuesday;
	}

	public boolean isAvailableOnWednesday() {
		return availableOnWednesday;
	}

	public void setAvailableOnWednesday(boolean availableOnWednesday) {
		this.availableOnWednesday = availableOnWednesday;
	}

	public boolean isAvailableOnThursday() {
		return availableOnThursday;
	}

	public void setAvailableOnThursday(boolean availableOnThursday) {
		this.availableOnThursday = availableOnThursday;
	}

	public boolean isAvailableOnFriday() {
		return availableOnFriday;
	}

	public void setAvailableOnFriday(boolean availableOnFriday) {
		this.availableOnFriday = availableOnFriday;
	}

	public boolean isAvailableOnSaturday() {
		return availableOnSaturday;
	}

	public void setAvailableOnSaturday(boolean availableOnSaturday) {
		this.availableOnSaturday = availableOnSaturday;
	}

	public boolean isAvailableOnSunday() {
		return availableOnSunday;
	}

	public void setAvailableOnSunday(boolean availableOnSunday) {
		this.availableOnSunday = availableOnSunday;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + (availableOnFriday ? 1231 : 1237);
		result = prime * result + (availableOnMonday ? 1231 : 1237);
		result = prime * result + (availableOnSaturday ? 1231 : 1237);
		result = prime * result + (availableOnSunday ? 1231 : 1237);
		result = prime * result + (availableOnThursday ? 1231 : 1237);
		result = prime * result + (availableOnTuesday ? 1231 : 1237);
		result = prime * result + (availableOnWednesday ? 1231 : 1237);
		result = prime * result + ((billingPeriod == null) ? 0 : billingPeriod.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (foodSample ? 1231 : 1237);
		result = prime * result + (homeDelivered ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imageUrl == null) ? 0 : imageUrl.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((planPricing == null) ? 0 : planPricing.hashCode());
		result = prime * result + ((store == null) ? 0 : store.hashCode());
		result = prime * result + (takeOut ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StoreOfferredPlan other = (StoreOfferredPlan) obj;
		if (active != other.active)
			return false;
		if (availableOnFriday != other.availableOnFriday)
			return false;
		if (availableOnMonday != other.availableOnMonday)
			return false;
		if (availableOnSaturday != other.availableOnSaturday)
			return false;
		if (availableOnSunday != other.availableOnSunday)
			return false;
		if (availableOnThursday != other.availableOnThursday)
			return false;
		if (availableOnTuesday != other.availableOnTuesday)
			return false;
		if (availableOnWednesday != other.availableOnWednesday)
			return false;
		if (billingPeriod == null) {
			if (other.billingPeriod != null)
				return false;
		} else if (!billingPeriod.equals(other.billingPeriod))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (foodSample != other.foodSample)
			return false;
		if (homeDelivered != other.homeDelivered)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imageUrl == null) {
			if (other.imageUrl != null)
				return false;
		} else if (!imageUrl.equals(other.imageUrl))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (planPricing == null) {
			if (other.planPricing != null)
				return false;
		} else if (!planPricing.equals(other.planPricing))
			return false;
		if (store == null) {
			if (other.store != null)
				return false;
		} else if (!store.equals(other.store))
			return false;
		if (takeOut != other.takeOut)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return description+" "+name+" "+(homeDelivered?"HomeDelivered":"")+" "+(takeOut?"TakeOut":"");
	}

	

	

}
