package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PromotionCounter implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Long storeId;
	private String storeName;
	private Long requestCounter;
	private Timestamp lastRequest;
	
	public PromotionCounter() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PromotionCounter(Long id, Long storeId, Long requestCounter, Timestamp lastRequest) {
		super();
		this.id = id;
		this.storeId = storeId;
		this.requestCounter = requestCounter;
		this.lastRequest = lastRequest;
	}
	public Long getId() {
		return id;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getStoreId() {
		return storeId;
	}
	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	public Long getRequestCounter() {
		return requestCounter;
	}
	public void resetRequestCounter() {
		this.requestCounter = 0l;
	}
	public void incrRequestCounter() {
		this.requestCounter++;
	}
	public Timestamp getLastRequest() {
		return lastRequest;
	}
	public void setLastRequest(Timestamp lastRequest) {
		this.lastRequest = lastRequest;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastRequest == null) ? 0 : lastRequest.hashCode());
		result = prime * result + ((requestCounter == null) ? 0 : requestCounter.hashCode());
		result = prime * result + ((storeId == null) ? 0 : storeId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PromotionCounter other = (PromotionCounter) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastRequest == null) {
			if (other.lastRequest != null)
				return false;
		} else if (!lastRequest.equals(other.lastRequest))
			return false;
		if (requestCounter == null) {
			if (other.requestCounter != null)
				return false;
		} else if (!requestCounter.equals(other.requestCounter))
			return false;
		if (storeId == null) {
			if (other.storeId != null)
				return false;
		} else if (!storeId.equals(other.storeId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PromotionCounter [id=" + id + ", storeId=" + storeId + ", requestCounter=" + requestCounter
				+ ", lastRequest=" + lastRequest + "]";
	}
	

}
