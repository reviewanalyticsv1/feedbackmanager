package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(indexes = {@Index(name = "query",columnList = "query"), @Index(name="query_store",columnList = "store_id")})
public class Query implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	private Reviewer reviewer;

	@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
	private Store store;

	@OneToOne
	private Reply reply;

	@Column(length = 2500)
	private String query;
	
	private Timestamp queriedOn;
	
	private boolean enable;
	
	private boolean verified;
	
	public Query() {
		super();
		this.enable = true;
		this.verified = false;
	}

	public boolean isEnable() {
		return this.enable;
	}
	public void setEnable(boolean enabled) {
		this.enable = enabled;
	}
	
	public boolean isVerified() {
		return this.verified;
	}
	public void setVerified(boolean verify) {
		this.verified = verify;
	}
	
	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Reply getReply() {
		return reply;
	}

	public void setReply(Reply reply) {
		this.reply = reply;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Timestamp getQueriedOn() {
		return queriedOn;
	}

	public void setQueriedOn(Timestamp date) {
		this.queriedOn = date;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((queriedOn == null) ? 0 : queriedOn.hashCode());
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		result = prime * result + ((reply == null) ? 0 : reply.hashCode());
		result = prime * result + ((reviewer == null) ? 0 : reviewer.hashCode());
		result = prime * result + ((store == null) ? 0 : store.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Query other = (Query) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (queriedOn == null) {
			if (other.queriedOn != null)
				return false;
		} else if (!queriedOn.equals(other.queriedOn))
			return false;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		if (reply == null) {
			if (other.reply != null)
				return false;
		} else if (!reply.equals(other.reply))
			return false;
		if (reviewer == null) {
			if (other.reviewer != null)
				return false;
		} else if (!reviewer.equals(other.reviewer))
			return false;
		if (store == null) {
			if (other.store != null)
				return false;
		} else if (!store.equals(other.store))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "QueryTemp [id=" + id + ", reviewer=" + reviewer + ", store=" + store + ", reply=" + reply + ", query="
				+ query + ", queriedOn=" + queriedOn + "]";
	}
	
	

}
