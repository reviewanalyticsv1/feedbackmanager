package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(indexes = {@Index(name = "review",columnList = "review"), @Index(name="review_store",columnList = "store_id")})
public class Review implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	private Reviewer reviewer;
	
	private Timestamp reviedOn;
	
	private String reviewImage;

	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private Reply reply;
	
	private Double rating;

	@Column(length = 2500)
	private String review;
	
	@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
	private Store store;
	
	private boolean enable;
	
	private boolean verified;

	public Review() {
		super();
		this.enable = true;
		this.verified = false;
	}

	public Review(Long id, Reviewer reviewer, Timestamp reviedOn, String reviewImage, Reply reply, Double rating,String review, Store store) {
		super();
		this.id = id;
		this.reviewer = reviewer;
		this.reviedOn = reviedOn;
		this.reviewImage = reviewImage;
		this.reply = reply;
		this.rating = rating;
		this.review = review;
		this.store = store;
		this.enable = true;
		this.verified = false;
	}

	public boolean isEnable() {
		return this.enable;
	}
	public void setEnable(boolean enabled) {
		this.enable = enabled;
	}
	
	public boolean isVerified() {
		return this.verified;
	}
	public void setVerified(boolean verify) {
		this.verified = verify;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public Timestamp getReviedOn() {
		return reviedOn;
	}

	public void setReviedOn(Timestamp reviedOn) {
		this.reviedOn = reviedOn;
	}

	public String getReviewImage() {
		return reviewImage;
	}

	public void setReviewImage(String reviewImage) {
		this.reviewImage = reviewImage;
	}

	public Reply getReply() {
		return reply;
	}

	public void setReply(Reply reply) {
		this.reply = reply;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((rating == null) ? 0 : rating.hashCode());
		result = prime * result + ((reply == null) ? 0 : reply.hashCode());
		result = prime * result + ((reviedOn == null) ? 0 : reviedOn.hashCode());
		result = prime * result + ((review == null) ? 0 : review.hashCode());
		result = prime * result + ((reviewImage == null) ? 0 : reviewImage.hashCode());
		result = prime * result + ((reviewer == null) ? 0 : reviewer.hashCode());
		result = prime * result + ((store == null) ? 0 : store.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Review other = (Review) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (rating == null) {
			if (other.rating != null)
				return false;
		} else if (!rating.equals(other.rating))
			return false;
		if (reply == null) {
			if (other.reply != null)
				return false;
		} else if (!reply.equals(other.reply))
			return false;
		if (reviedOn == null) {
			if (other.reviedOn != null)
				return false;
		} else if (!reviedOn.equals(other.reviedOn))
			return false;
		if (review == null) {
			if (other.review != null)
				return false;
		} else if (!review.equals(other.review))
			return false;
		if (reviewImage == null) {
			if (other.reviewImage != null)
				return false;
		} else if (!reviewImage.equals(other.reviewImage))
			return false;
		if (reviewer == null) {
			if (other.reviewer != null)
				return false;
		} else if (!reviewer.equals(other.reviewer))
			return false;
		if (store == null) {
			if (other.store != null)
				return false;
		} else if (!store.equals(other.store))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Review [id=" + id + ", reviewer=" + reviewer + ", reviedOn=" + reviedOn + ", reviewImage=" + reviewImage
				+ ", reply=" + reply + ", rating=" + rating + ", review=" + review + ", store=" + store + "]";
	}
	
	

	

	

}
