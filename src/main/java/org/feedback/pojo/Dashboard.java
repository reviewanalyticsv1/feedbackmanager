package org.feedback.pojo;

import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.feedback.utility.RMUtil;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Dashboard {

	
	@JsonProperty(value = "clientId")
	private String clientId;

	
	@JsonProperty("totalRating")
	private Double totalRating;

	
	@JsonProperty("totalPositive")
	private Integer totalPositive;

	
	@JsonProperty("totalNegative")
	private Integer totalNegative;

	
	@JsonProperty("totalNeutral")
	private Integer totalNeutral;

	
	@JsonProperty("dashboardChartData")
	private List<DashboardChartObject> dashboardChartData;

	@JsonProperty("totalUpdated")
	private Timestamp lastUpdated;

	
	public Dashboard() {
		super();
		this.dashboardChartData = new LinkedList<DashboardChartObject>();
	}

	
	public Dashboard(String clientId, Double totalRating, Integer totalPositive, Integer totalNegative,
			Integer totalNeutral, List<DashboardChartObject> dashboardChartData) {
		super();
		this.clientId = clientId;
		this.totalRating = totalRating;
		this.totalPositive = totalPositive;
		this.totalNegative = totalNegative;
		this.totalNeutral = totalNeutral;
		this.dashboardChartData = dashboardChartData;
		this.lastUpdated = new Timestamp(System.currentTimeMillis());
	}

	
	public Integer getTotalPositive() {
		return totalPositive;
	}

	
	public void setTotalPositive(Integer totalPositive) {
		this.totalPositive = totalPositive;
	}

	
	public Integer getTotalNegative() {
		return totalNegative;
	}

	
	public void setTotalNegative(Integer totalNegative) {
		this.totalNegative = totalNegative;
	}

	
	public Integer getTotalNeutral() {
		return totalNeutral;
	}

	
	public void setTotalNeutral(Integer totalNeutral) {
		this.totalNeutral = totalNeutral;
	}

	
	public List<DashboardChartObject> getDashboardChartData() {
		if (dashboardChartData == null) {
			dashboardChartData = new ArrayList<DashboardChartObject>();
		}
		return dashboardChartData;
	}

	
	public void setDashboardChartData(List<DashboardChartObject> dashboardChartData) {
		this.dashboardChartData = dashboardChartData;
	}

	
	public Double getTotalRating() {
		return totalRating;
	}

	
	public void setTotalRating(Double totalRating) {
		this.totalRating = totalRating;
	}

	
	public String getClientId() {
		return clientId;
	}

	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	
	public Map<String, Object> getDashboardMap() {
		return RMUtil.getMap(this);
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
		result = prime * result + ((dashboardChartData == null) ? 0 : dashboardChartData.hashCode());
		result = prime * result + ((lastUpdated == null) ? 0 : lastUpdated.hashCode());
		result = prime * result + ((totalNegative == null) ? 0 : totalNegative.hashCode());
		result = prime * result + ((totalNeutral == null) ? 0 : totalNeutral.hashCode());
		result = prime * result + ((totalPositive == null) ? 0 : totalPositive.hashCode());
		result = prime * result + ((totalRating == null) ? 0 : totalRating.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dashboard other = (Dashboard) obj;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		if (dashboardChartData == null) {
			if (other.dashboardChartData != null)
				return false;
		} else if (!dashboardChartData.equals(other.dashboardChartData))
			return false;
		if (lastUpdated == null) {
			if (other.lastUpdated != null)
				return false;
		} else if (!lastUpdated.equals(other.lastUpdated))
			return false;
		if (totalNegative == null) {
			if (other.totalNegative != null)
				return false;
		} else if (!totalNegative.equals(other.totalNegative))
			return false;
		if (totalNeutral == null) {
			if (other.totalNeutral != null)
				return false;
		} else if (!totalNeutral.equals(other.totalNeutral))
			return false;
		if (totalPositive == null) {
			if (other.totalPositive != null)
				return false;
		} else if (!totalPositive.equals(other.totalPositive))
			return false;
		if (totalRating == null) {
			if (other.totalRating != null)
				return false;
		} else if (!totalRating.equals(other.totalRating))
			return false;
		return true;
	}

}
