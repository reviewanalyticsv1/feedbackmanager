package org.feedback.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StoreTemp {
	private Long id;
	private String storeName;
	private String storeDetail;
	private String storeContactNumber;
	@URL
	private String storeUrl;
	@Email
	private String storeEmail;
	private Address address;

	private WorkingHours workingHours;
	
	private Set<ServiceArea> serviceArea = new HashSet<>();

	private Set<Promotion> promotions;

	private Set<Review> reviews;
	
	private BusinessUser businessUser;

	private boolean active;
	private StorePolicies storePolicies;
	
	public StoreTemp() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	

	public BusinessUser getBusinessUser() {
		return businessUser;
	}

	public void setBusinessUser(BusinessUser businessUser) {
		this.businessUser = businessUser;
	}

	public WorkingHours getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(WorkingHours workingHours) {
		this.workingHours = workingHours;
	}

	public Set<Promotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(Set<Promotion> promotions) {
		this.promotions = promotions;
	}

	public Set<Review> getReviews() {
		return reviews;
	}

	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	public String getStoreContactNumber() {
		return storeContactNumber;
	}

	public void setStoreContactNumber(String storeContactNumber) {
		this.storeContactNumber = storeContactNumber;
	}

	public StorePolicies getStorePolicies() {
		return storePolicies;
	}

	public void setStorePolicies(StorePolicies storePolicies) {
		this.storePolicies = storePolicies;
	}

	public String getStoreUrl() {
		return storeUrl;
	}

	public void setStoreUrl(String storeUrl) {
		this.storeUrl = storeUrl;
	}

	public String getStoreEmail() {
		return storeEmail;
	}

	public void setStoreEmail(String storeEmail) {
		this.storeEmail = storeEmail;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	

	

	public Set<ServiceArea> getStoreServiceArea() {
		return serviceArea;
	}

	public void setStoreServiceArea(Set<ServiceArea> storeServiceArea) {
		this.serviceArea = serviceArea;
	}

	public String getStoreDetail() {
		return storeDetail;
	}

	public void setStoreDetail(String storeDetail) {
		this.storeDetail = storeDetail;
	}

	public Set<ServiceArea> getServiceArea() {
		return serviceArea;
	}

	public void setServiceArea(Set<ServiceArea> serviceArea) {
		this.serviceArea = serviceArea;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((businessUser == null) ? 0 : businessUser.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((promotions == null) ? 0 : promotions.hashCode());
		result = prime * result + ((reviews == null) ? 0 : reviews.hashCode());
		result = prime * result + ((serviceArea == null) ? 0 : serviceArea.hashCode());
		result = prime * result + ((storeContactNumber == null) ? 0 : storeContactNumber.hashCode());
		result = prime * result + ((storeDetail == null) ? 0 : storeDetail.hashCode());
		result = prime * result + ((storeEmail == null) ? 0 : storeEmail.hashCode());
		result = prime * result + ((storeName == null) ? 0 : storeName.hashCode());
		result = prime * result + ((storeUrl == null) ? 0 : storeUrl.hashCode());
		result = prime * result + ((workingHours == null) ? 0 : workingHours.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StoreTemp other = (StoreTemp) obj;
		if (active != other.active)
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (businessUser == null) {
			if (other.businessUser != null)
				return false;
		} else if (!businessUser.equals(other.businessUser))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (promotions == null) {
			if (other.promotions != null)
				return false;
		} else if (!promotions.equals(other.promotions))
			return false;
		if (reviews == null) {
			if (other.reviews != null)
				return false;
		} else if (!reviews.equals(other.reviews))
			return false;
		if (serviceArea == null) {
			if (other.serviceArea != null)
				return false;
		} else if (!serviceArea.equals(other.serviceArea))
			return false;
		if (storeContactNumber == null) {
			if (other.storeContactNumber != null)
				return false;
		} else if (!storeContactNumber.equals(other.storeContactNumber))
			return false;
		if (storeDetail == null) {
			if (other.storeDetail != null)
				return false;
		} else if (!storeDetail.equals(other.storeDetail))
			return false;
		if (storeEmail == null) {
			if (other.storeEmail != null)
				return false;
		} else if (!storeEmail.equals(other.storeEmail))
			return false;
		if (storeName == null) {
			if (other.storeName != null)
				return false;
		} else if (!storeName.equals(other.storeName))
			return false;
		if (storeUrl == null) {
			if (other.storeUrl != null)
				return false;
		} else if (!storeUrl.equals(other.storeUrl))
			return false;
		if (workingHours == null) {
			if (other.workingHours != null)
				return false;
		} else if (!workingHours.equals(other.workingHours))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StoreTemp [id=" + id + ", storeName=" + storeName + ", storeDetail=" + storeDetail
				+ ", storeContactNumber=" + storeContactNumber + ", storeUrl=" + storeUrl + ", storeEmail=" + storeEmail
				+ ", address=" + address + ", workingHours=" + workingHours + ", serviceArea=" + serviceArea
				+ ", promotions=" + promotions + ", reviews=" + reviews + ", businessUser=" + businessUser + ", active="
				+ active + "]";
	}

	 

	

}
