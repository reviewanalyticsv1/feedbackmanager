package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Reply implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(length = 2500)
	private String reply;
	private Timestamp repliedOn;

	private String repliedBy;

	public String getReply() {
		return reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}

	public Timestamp getRepliedOn() {
		return repliedOn;
	}

	public void setRepliedOn(Timestamp date) {
		this.repliedOn = date;
	}

	public String getRepliedBy() {
		return repliedBy;
	}

	public void setRepliedBy(BusinessUser repliedBy) {
		this.repliedBy = repliedBy.getClientName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((repliedBy == null) ? 0 : repliedBy.hashCode());
		result = prime * result + ((repliedOn == null) ? 0 : repliedOn.hashCode());
		result = prime * result + ((reply == null) ? 0 : reply.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reply other = (Reply) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (repliedBy == null) {
			if (other.repliedBy != null)
				return false;
		} else if (!repliedBy.equals(other.repliedBy))
			return false;
		if (repliedOn == null) {
			if (other.repliedOn != null)
				return false;
		} else if (!repliedOn.equals(other.repliedOn))
			return false;
		if (reply == null) {
			if (other.reply != null)
				return false;
		} else if (!reply.equals(other.reply))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Reply [id=" + id + ", reply=" + reply + ", repliedOn=" + repliedOn + ", repliedBy=" + repliedBy + "]";
	}

}
