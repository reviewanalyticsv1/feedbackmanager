package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.feedback.utility.RMUtil.JOBSTATUS;

@Entity
public class DeliveryJobApplication implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToOne
	private JobPosting jobPosting;
	@OneToOne
	private DeliveryDriver deliveryDriver;
	
	private JOBSTATUS status;
	
	@ManyToOne(cascade = CascadeType.DETACH)
	private BusinessUser businessUser;
	
	private Timestamp appliedOn;
	
	private Timestamp updatedOn;

	public DeliveryJobApplication() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DeliveryJobApplication(Long id, JobPosting jobPostring, DeliveryDriver deliveryDriver, JOBSTATUS status) {
		super();
		this.id = id;
		this.jobPosting = jobPostring;
		this.deliveryDriver = deliveryDriver;
		this.status = status;
		this.appliedOn = new Timestamp(System.currentTimeMillis());
		this.updatedOn = this.appliedOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	 

 

	public DeliveryDriver getDeliveryDriver() {
		return deliveryDriver;
	}

	public JobPosting getJobPosting() {
		return jobPosting;
	}

	public void setJobPosting(JobPosting jobPosting) {
		this.jobPosting = jobPosting;
	}

	public BusinessUser getBusinessUser() {
		return businessUser;
	}

	public void setBusinessUser(BusinessUser businessUser) {
		this.businessUser = businessUser;
	}

	public void setDeliveryDriver(DeliveryDriver deliveryDriver) {
		this.deliveryDriver = deliveryDriver;
	}

	public JOBSTATUS getStatus() {
		return status;
	}

	public void setStatus(JOBSTATUS status) {
		this.updatedOn = new Timestamp(System.currentTimeMillis());
		this.status = status;
	}
	

	public Timestamp getAppliedOn() {
		return appliedOn;
	}

	public void setAppliedOn(Timestamp appliedOn) {
		this.appliedOn = appliedOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deliveryDriver == null) ? 0 : deliveryDriver.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((jobPosting == null) ? 0 : jobPosting.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryJobApplication other = (DeliveryJobApplication) obj;
		if (deliveryDriver == null) {
			if (other.deliveryDriver != null)
				return false;
		} else if (!deliveryDriver.equals(other.deliveryDriver))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (jobPosting == null) {
			if (other.jobPosting != null)
				return false;
		} else if (!jobPosting.equals(other.jobPosting))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DeliveryJob [id=" + id + ", jobPostring=" + jobPosting + ", deliveryDriver=" + deliveryDriver
				+ ", status=" + status + "]";
	}
	
	
}
