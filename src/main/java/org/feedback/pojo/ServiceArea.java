package org.feedback.pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.feedback.utility.RMUtil;

@Entity
@Table(indexes = {@Index(name = "service_area",columnList = "cityName,stateName,countryName")})
public class ServiceArea implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String countryName;
	private String stateName;
	private String cityName;

	@Column(columnDefinition = "varchar(8)",nullable = false)
	private String currency;

	public ServiceArea() {
		super();
	}

	public ServiceArea(String cityName, String stateName, String countryName) {
		super();
		this.countryName = countryName.trim();
		this.stateName = stateName.trim();
		this.cityName = cityName.trim();
		this.currency = RMUtil.getCurrency(this.countryName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceArea other = (ServiceArea) obj;
		if (cityName == null) {
			if (other.cityName != null)
				return false;
		} else if (!cityName.equals(other.cityName))
			return false;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (stateName == null) {
			if (other.stateName != null)
				return false;
		} else if (!stateName.equals(other.stateName))
			return false;
		return true;
	}

	public String getCityName() {
		return cityName;
	}

	public String getCountryName() {
		return countryName;
	}

	public String getCurrency() {
		return currency;
	}

	public Long getId() {
		return id;
	}

	public String getStateName() {
		return stateName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cityName == null) ? 0 : cityName.hashCode());
		result = prime * result + ((countryName == null) ? 0 : countryName.hashCode());
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((stateName == null) ? 0 : stateName.hashCode());
		return result;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Override
	public String toString() {
		return "ServiceArea [id=" + id + ", countryName=" + countryName + ", stateName=" + stateName + ", cityName="
				+ cityName + ", currency=" + currency + "]";
	}

}
