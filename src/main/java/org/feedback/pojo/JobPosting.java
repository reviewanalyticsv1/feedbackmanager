package org.feedback.pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(value = {"businessUser"})
public class JobPosting implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(cascade = CascadeType.MERGE)
	private BusinessUser businessUser;

	private String name;
	private Timestamp duration;
	private Timestamp expiryDate;
	
	@ManyToMany(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
	private List<ServiceArea> serviceArea;
	private String wageInformation;
	
	@Column(length = 2000)
	private String description;
	
	private Timestamp createdOn;
	private Timestamp modifiedOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public BusinessUser getBusinessUser() {
		return businessUser;
	}

	public void setBusinessUser(BusinessUser businessUser) {
		this.businessUser = businessUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Timestamp expiryDate) {
		this.expiryDate = expiryDate;
	}

	public List<ServiceArea> getServiceArea() {
		return serviceArea;
	}

	public void setServiceArea(List<ServiceArea> serviceArea) {
		this.serviceArea = serviceArea;
	}

	public String getWageInformation() {
		return wageInformation;
	}

	public void setWageInformation(String wageInformation) {
		this.wageInformation = wageInformation;
	}

	public JobPosting() {
		super();
		this.createdOn = new Timestamp(System.currentTimeMillis());
		this.modifiedOn = this.createdOn;
	}

	 

	public Timestamp getDuration() {
		return duration;
	}

	public void setDuration(Timestamp duration) {
		this.duration = duration;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public JobPosting(Long id, BusinessUser businessUser, String name, Timestamp duration, Timestamp expiryDate,
			List<ServiceArea> serviceArea, String wageInformation, String description) {
		super();
		this.id = id;
		this.businessUser = businessUser;
		this.name = name;
		this.duration = duration;
		this.expiryDate = expiryDate;
		this.serviceArea = serviceArea;
		this.wageInformation = wageInformation;
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((businessUser == null) ? 0 : businessUser.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((duration == null) ? 0 : duration.hashCode());
		result = prime * result + ((expiryDate == null) ? 0 : expiryDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((serviceArea == null) ? 0 : serviceArea.hashCode());
		result = prime * result + ((wageInformation == null) ? 0 : wageInformation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobPosting other = (JobPosting) obj;
		if (businessUser == null) {
			if (other.businessUser != null)
				return false;
		} else if (!businessUser.equals(other.businessUser))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (duration == null) {
			if (other.duration != null)
				return false;
		} else if (!duration.equals(other.duration))
			return false;
		if (expiryDate == null) {
			if (other.expiryDate != null)
				return false;
		} else if (!expiryDate.equals(other.expiryDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (serviceArea == null) {
			if (other.serviceArea != null)
				return false;
		} else if (!serviceArea.equals(other.serviceArea))
			return false;
		if (wageInformation == null) {
			if (other.wageInformation != null)
				return false;
		} else if (!wageInformation.equals(other.wageInformation))
			return false;
		return true;
	}
	 

	@Override
	public String toString() {
		return "JobPosting [id=" + id + ", businessUser=" + businessUser + ", name=" + name + ", duration=" + duration
				+ ", expiryDate=" + expiryDate + ", serviceArea=" + serviceArea + ", wageInformation=" + wageInformation
				+ ", description=" + description + "]";
	}

	 

	 

}
