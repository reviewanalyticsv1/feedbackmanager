package org.feedback.configuration;

import java.security.SecureRandom;

import org.feedback.service.BusinessUserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true, securedEnabled = true)
public class HomeKitchenCloudSecurity {

	@Bean
	public static BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(8, new SecureRandom());
	}
	
	public HomeKitchenCloudSecurity() {
		super();
	}

	@Configuration
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public static class BusinessSecurity extends WebSecurityConfigurerAdapter {

		@Autowired
		BusinessUserDetail businessUserDetail;

		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(businessUserDetail).passwordEncoder(passwordEncoder());
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.antMatcher("/**").csrf().disable().authorizeRequests()
					.antMatchers("/error", "/login", "/logout", "/forgotPassword", "/addUser", "/landing/**")
					.permitAll().and().authorizeRequests().antMatchers("/business/**").authenticated().and().formLogin()
					.loginPage("/login").and().logout().logoutUrl("/logout").and().rememberMe().and()
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).maximumSessions(1);
		}

	}
}
