package org.feedback.service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.internet.MimeMessage;

import org.feedback.pojo.EmailNotificationObject;
import org.feedback.utility.RMUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

	public ExecutorService executorService = Executors.newFixedThreadPool(10);

	private JavaMailSender emailSender;

	@Autowired
	public EmailService(JavaMailSender emailSender) {
		super();
		this.emailSender = emailSender;
	}

	public Map<String, Object> sendEmailNotification(EmailNotificationObject emailNotificationObject) {
		Map<String, Object> data = new HashMap<String, Object>();
		try {

			executorService.submit(() -> {

				/*
				 * SimpleMailMessage message = new SimpleMailMessage();
				 * message.setFrom("support@meaily.com"); message.setTo(); message.setSubject();
				 * message.setText(); 
				 */
				
				MimeMessage message = emailSender.createMimeMessage();
			     
			    MimeMessageHelper helper = new MimeMessageHelper(message, true);
			    
			    helper.setFrom("support@meaily.com", "Meaily Services");
			    helper.setTo(emailNotificationObject.getSendTo());
			    helper.setSubject(emailNotificationObject.getSubject());
			    helper.setText(getMessageType(emailNotificationObject ),true);
			        
				/*
				 * FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
				 * helper.addAttachment("Invoice", file);
				 */

			    emailSender.send(message);
			 
				 
				return "send";

			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public String getMessageType(EmailNotificationObject emailNotificationObject ) {
		return RMUtil.getEmailTemplate(emailNotificationObject );

	}

}