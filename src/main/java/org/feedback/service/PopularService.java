package org.feedback.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.feedback.utility.RMUtil;
import org.springframework.stereotype.Service;

@Service
public class PopularService {

	private Map<Integer,Queue<String>> popularService = new HashMap<Integer, Queue<String>>();

	public PopularService() {
		super();
	}
    
	public boolean addSearchKeyword(Integer location,String keyword) {
		
		Queue<String> keywords = new LinkedList<String>();
		if(popularService.containsKey(location))
			keywords = popularService.get(location);
		
		if(keywords.size()>=RMUtil.SEARCH_KEYWORD_CAPACITY)keywords.remove();
		
		keywords.removeIf(str -> keyword.contains(str));
		keywords.add(keyword); 		

		return popularService.putIfAbsent(location, keywords) != null;
	}
	
	public Queue<String> getSearchKeyword(Integer location) {
		return popularService.get(location);
	}
	 
}
