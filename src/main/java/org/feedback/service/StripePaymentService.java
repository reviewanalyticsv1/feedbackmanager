package org.feedback.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.feedback.pojo.BusinessUser;
import org.feedback.utility.RMUtil;
import org.feedback.utility.RMUtil.PLANS;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceCollection;
import com.stripe.model.Subscription;
import com.stripe.model.SubscriptionItem;
import com.stripe.model.checkout.Session;
import com.stripe.param.SubscriptionUpdateParams;
import com.stripe.param.checkout.SessionCreateParams;
import com.stripe.param.checkout.SessionCreateParams.BillingAddressCollection;
import com.stripe.param.checkout.SessionCreateParams.LineItem;
import com.stripe.param.checkout.SessionCreateParams.SubscriptionData;

@Service
public class StripePaymentService {

	@Value("${meaily.plan.trial.days}")
	private int TRIAL_PERIOD;
	@Value("${meaily.stripe.secret}")
	private String STRIPE_SECRET_KEY;
	@Value("${meaily.stripe.publishable}")
	private String STRIPE_PUBLISHABLE_KEY;
	@Value("${meaily.stripe.canada.baseplan}")
	private String BASE_PLAN_CANADA;
	@Value("${meaily.stripe.canada.payasgoplan}")
	private String PAY_AS_GO_PLAN_CANADA;

	public Session getStripeSession(String paymentId,Boolean trialFlag, PLANS smallBusiness,String customerEmail) throws StripeException {

		Stripe.apiKey = STRIPE_SECRET_KEY;
		SubscriptionData subscriptionData = null;
		LineItem element = null;
		if (trialFlag) {
			subscriptionData = SessionCreateParams.SubscriptionData.builder()
					.addItem(SessionCreateParams.SubscriptionData.Item.builder().setPlan(BASE_PLAN_CANADA).build())
					.build();
		} else {
			subscriptionData = SessionCreateParams.SubscriptionData.builder()
					.setTrialPeriodDays(Long.valueOf(TRIAL_PERIOD))
					.addItem(SessionCreateParams.SubscriptionData.Item.builder().setPlan(BASE_PLAN_CANADA).build())
					.build();
		}

		return Session.create(SessionCreateParams.builder().setCustomer(paymentId)
				.setBillingAddressCollection(BillingAddressCollection.REQUIRED)
				.addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD).setSubscriptionData(subscriptionData)
				.setSuccessUrl("http://mealforest.com/business/#!/pages/account")
				.setCancelUrl("http://mealforest.com/error").build());

	}

	public Customer createStripeUser(BusinessUser newUserRequest) throws Exception {

		Stripe.apiKey = STRIPE_SECRET_KEY;
		Map<String, Object> params = new HashMap<>();
		params.put("name", newUserRequest.getName());
		params.put("description", newUserRequest.getName());
		params.put("email", newUserRequest.getEmail());
		params.put("phone", newUserRequest.getPhoneNumber());
		Map<String, Object> address = new HashMap<>();
		address.put("city", newUserRequest.getAddress().getArea().getCityName());
		address.put("state", newUserRequest.getAddress().getArea().getStateName());
		address.put("country", newUserRequest.getAddress().getArea().getCountryName());
		address.put("postal_code", newUserRequest.getAddress().getPostal());
		address.put("line1", newUserRequest.getAddress().getStreet());
		params.put("address", address);

		return Customer.create(params);
	}

	public Customer getCustomer(BusinessUser user) throws StripeException {
		return Customer.retrieve(user.getPaymentId());
	}

	public InvoiceCollection getInvoices(BusinessUser user) throws StripeException {
		Stripe.apiKey = STRIPE_SECRET_KEY;

		Map<String, Object> params = new HashMap<>();
		params.put("customer", user.getPaymentId());

		return Invoice.list(params);
	}

	public boolean createSubscription(String customerId, int total_customers) {
		boolean flag = true;
		try {
			Stripe.apiKey = STRIPE_SECRET_KEY;
			for (Subscription subscription : Customer.retrieve(customerId).getSubscriptions().getData()) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("quantity", total_customers);
				if (subscription.getItems().getData().stream()
						.filter(sub -> sub.getPlan().getId().equalsIgnoreCase(PAY_AS_GO_PLAN_CANADA)).count() == 0) {
					params.put("subscription", subscription.getId());
					params.put("price", PAY_AS_GO_PLAN_CANADA);
					SubscriptionItem.create(params);
				} else {
					SubscriptionItem.retrieve(subscription.getItems().getData().stream()
							.filter(sub -> sub.getPlan().getId().equalsIgnoreCase(PAY_AS_GO_PLAN_CANADA)).findFirst()
							.get().getId()).update(params);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			flag = false;
		}
		return flag;
	}

	public boolean cancelSubscription(BusinessUser user) throws Exception {

		Stripe.apiKey = STRIPE_SECRET_KEY;

		for (Subscription subscription : this.getCustomer(user).getSubscriptions().getData()) {
			subscription.update(SubscriptionUpdateParams.builder().setCancelAtPeriodEnd(true).build());
		}

		return true;
	}

	public boolean resumeSubscription(BusinessUser user) throws Exception {

		Stripe.apiKey = STRIPE_SECRET_KEY;

		for (Subscription subscription : this.getCustomer(user).getSubscriptions().getData()) {
			subscription.update(SubscriptionUpdateParams.builder().setCancelAtPeriodEnd(false).build());
		}

		return true;
	}

	public List<Subscription> getSubscriptionStatus(BusinessUser user) throws Exception {
		Stripe.apiKey = STRIPE_SECRET_KEY;

		return this.getCustomer(user).getSubscriptions().getData();

	}

	public String getSubscriptionMessage(BusinessUser user) throws Exception {
		StringBuilder message = new StringBuilder();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
		this.getCustomer(user).getSubscriptions().getData().forEach(sub -> {
			if (sub.getCancelAtPeriodEnd())
				message.append("Your subscription is set to be cancelled on "
						+ dateFormat.format(new Timestamp(1000 * sub.getCancelAt()))
						+ ". You will not be charged afterwards.");
		});
		return message.toString();
	}

}
