package org.feedback.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.StringUtils;
import org.feedback.pojo.AccessToken;
import org.feedback.pojo.ActionItem;
import org.feedback.pojo.BusinessUser;
import org.feedback.pojo.DeliveryDriver;
import org.feedback.pojo.DeliveryJobApplication;
import org.feedback.pojo.Demo;
import org.feedback.pojo.EmailSubscription;
import org.feedback.pojo.Errors;
import org.feedback.pojo.Invitations;
import org.feedback.pojo.JobPosting;
import org.feedback.pojo.NotificationBusiness;
import org.feedback.pojo.NotificationPublic;
import org.feedback.pojo.Promotion;
import org.feedback.pojo.PromotionCounter;
import org.feedback.pojo.Query;
import org.feedback.pojo.Reply;
import org.feedback.pojo.Review;
import org.feedback.pojo.Reviewer;
import org.feedback.pojo.Store;
import org.feedback.pojo.StoreOfferredPlan;
import org.feedback.pojo.SubscriptionCustomer;
import org.feedback.pojo.SubscriptionPlan;
import org.feedback.repository.IAccessToken;
import org.feedback.repository.IActionItem;
import org.feedback.repository.IBusinessUser;
import org.feedback.repository.IDeliveryDriver;
import org.feedback.repository.IDeliveryJobApplication;
import org.feedback.repository.IDemo;
import org.feedback.repository.IEmailSubscription;
import org.feedback.repository.IErrors;
import org.feedback.repository.IInvitations;
import org.feedback.repository.IJobPosting;
import org.feedback.repository.INotificationBusiness;
import org.feedback.repository.INotificationPublic;
import org.feedback.repository.IPromotion;
import org.feedback.repository.IPromotionCounter;
import org.feedback.repository.IQuery;
import org.feedback.repository.IReply;
import org.feedback.repository.IReview;
import org.feedback.repository.IReviewer;
import org.feedback.repository.IStore;
import org.feedback.repository.IStoreOfferredPlan;
import org.feedback.repository.ISubscriptionCustomer;
import org.feedback.repository.ISubscriptionPlan;
import org.feedback.utility.RMUtil;
import org.feedback.utility.RMUtil.ACCESS_TYPE;
import org.feedback.utility.RMUtil.CUSTOMER_STATUS;
import org.feedback.utility.RMUtil.JOBSTATUS;
import org.feedback.utility.RMUtil.PLANS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.stripe.exception.StripeException;
import com.stripe.model.Invoice;
import com.stripe.model.Subscription;
import com.stripe.model.checkout.Session;

@Service
public class BusinessService {

	private IActionItem actionItemRepository;
	private IAccessToken accessTokenRepository;
	private IBusinessUser businessUserRepository;
	private IEmailSubscription emailSubscriptionRepository;
	private INotificationBusiness notificationBusinessRepository;
	private INotificationPublic notificationPublicRepository;
	private StripePaymentService paymentService;
	private IPromotionCounter promotionCounterRepository;
	private IPromotion promotionRepository;
	private IQuery queryRepository;
	private IReply replyRepository;
	private IReview reviewRepository;
	private IStoreOfferredPlan storeOfferredPlanRepository;
	private IStore storeRepository;
	private ISubscriptionCustomer subscriptionCustomerRepository;
	private ISubscriptionPlan subscriptionPlanRepository;
	private IJobPosting jobPostingRepository;
	private IDeliveryDriver deliveryDriver;
	private IDeliveryJobApplication deliveryJobApplicationRepository;
	private IReviewer reviewerRepository;
	private IDemo demoRepository;
	private IErrors errorRepository;
	private IInvitations invitationsRepository;

	private ExecutorService parallelService = Executors.newFixedThreadPool(500);

	public BusinessService() {
		super();
	}

	@Autowired(required = true)
	public BusinessService(IActionItem actionItemRepository, IAccessToken accessTokenRepository,
			IBusinessUser businessUserRepository, IEmailSubscription emailSubscriptionRepository,
			INotificationBusiness notificationBusinessRepository, INotificationPublic notificationPublicRepository,
			StripePaymentService paymentService, IPromotionCounter promotionCounterRepository,
			IPromotion promotionRepository, IQuery queryRepository, IReply replyRepository, IReview reviewRepository,
			IStoreOfferredPlan storeOfferredPlanRepository, IStore storeRepository,
			ISubscriptionCustomer subscriptionCustomerRepository, ISubscriptionPlan subscriptionPlanRepository,
			IJobPosting jobPostingRepository, IDeliveryDriver deliveryDriver,
			IDeliveryJobApplication deliveryJobApplicationRepository, IReviewer reviewerRepository,
			IDemo demoRepository, IErrors errorRepository, IInvitations invitationsRepository) {
		super();
		this.actionItemRepository = actionItemRepository;
		this.accessTokenRepository = accessTokenRepository;
		this.businessUserRepository = businessUserRepository;
		this.emailSubscriptionRepository = emailSubscriptionRepository;
		this.notificationBusinessRepository = notificationBusinessRepository;
		this.notificationPublicRepository = notificationPublicRepository;
		this.paymentService = paymentService;
		this.promotionCounterRepository = promotionCounterRepository;
		this.promotionRepository = promotionRepository;
		this.queryRepository = queryRepository;
		this.replyRepository = replyRepository;
		this.reviewRepository = reviewRepository;
		this.storeOfferredPlanRepository = storeOfferredPlanRepository;
		this.storeRepository = storeRepository;
		this.subscriptionCustomerRepository = subscriptionCustomerRepository;
		this.subscriptionPlanRepository = subscriptionPlanRepository;

		this.jobPostingRepository = jobPostingRepository;
		this.deliveryDriver = deliveryDriver;
		this.deliveryJobApplicationRepository = deliveryJobApplicationRepository;
		this.reviewerRepository = reviewerRepository;
		this.demoRepository = demoRepository;
		this.errorRepository = errorRepository;
		this.invitationsRepository = invitationsRepository;
	}

	public boolean cancelSubscription(BusinessUser user) throws Exception {
		return paymentService.cancelSubscription(user);
	}

	@Cacheable(cacheNames = {"invoices"},key = "#user.id")
	public List<Invoice> getInvoices(BusinessUser user) throws StripeException {
		return paymentService.getInvoices(user).getData();
	}

	public Session getStripeSession(String paymentId, boolean trialCompleted, PLANS plan, String email)
			throws StripeException {
		return paymentService.getStripeSession(paymentId, trialCompleted, plan, email);
	}

	public String getSubscriptionMessage(BusinessUser user) throws Exception {
		return paymentService.getSubscriptionMessage(user);
	}

	public List<Subscription> getSubscriptionStatus(BusinessUser user) throws Exception {
		return paymentService.getSubscriptionStatus(user);
	}

	public boolean resumeSubscription(BusinessUser user) throws Exception {
		return paymentService.resumeSubscription(user);
	}

	/* Reply Repository */
	public Reply saveReply(Reply reply) {
		return replyRepository.save(reply);
	}

	/* Subscription Plan Repository */
	public Optional<SubscriptionPlan> subscriptionPlanFindById(Long id) {
		return subscriptionPlanRepository.findById(id);
	}

	public void saveSubscriptionPlan(SubscriptionPlan plan) {
		// parallelService.execute(() -> {
		subscriptionPlanRepository.save(plan);
		// });
	}

	/* Email Subscription Repository */
	public boolean emailSubscriptionExistsByEmailAndStoreId(String email, Long clientId) {
		return emailSubscriptionRepository.existsByEmailAndStoreId(email, clientId);
	}

	public void saveEmailSubscription(EmailSubscription emailSubscription) {
		// parallelService.execute(() -> {
		emailSubscriptionRepository.saveAndFlush(emailSubscription);
		// });
	}

	public void emailSubscriptionDeleteById(Long valueOf) {
		// parallelService.execute(() -> {
		emailSubscriptionRepository.deleteById(valueOf);
		// });
	}

	public List<EmailSubscription> emailSubscriptionFindByEmail(String email) {
		return emailSubscriptionRepository.findByEmail(email);
	}

	@Cacheable(cacheNames = {"store_email_subscriptions"},key = "#id")
	public List<EmailSubscription> emailSubscriptionFindByStoreIdOrderByCreatedOnDesc(Long id) {
		return emailSubscriptionRepository.findByStoreIdOrderByCreatedOnDesc(id);
	}

	/* Business User Repository */
	public boolean businessUserExistsByUsername(String username) {
		return businessUserRepository.existsByUsername(username);
	}

	public Optional<BusinessUser> businessUserFindById(Long id) {
		return businessUserRepository.findById(id);
	}

	public BusinessUser businessUserFindByUsername(String username) {
		return businessUserRepository.findByUsername(username);
	}

	public void saveBusinessUser(BusinessUser businessUserFromDB) {
		// parallelService.execute(() -> {
		businessUserRepository.save(businessUserFromDB);
		// });
	}

	/* Review Repository */
	@Cacheable(cacheNames = {"store_reviews"},key = "#id")
	public List<Review> reviewFindByStoreIdOrderByReviedOnDesc(Long id) {
		return reviewRepository.findByStoreIdAndEnableTrueAndVerifiedTrueOrderByReviedOnDesc(id);
	}

	public Review saveReview(Review review) {
		// //parallelService.execute(() -> {
		return reviewRepository.save(review);
		// //});
	}

	public Optional<Review> reviewFindById(Long reviewId) {
		return reviewRepository.findById(reviewId);
	}

	public void reviewDeleteById(Long id) {
		// reviewRepository.deleteById(id);
		Review obj = this.reviewFindById(id).get();
		obj.setEnable(false);
		this.saveReview(obj);
	}

	public List<Review> reviewFindByReviewerId(Long id, boolean all) {
		return reviewRepository.findByReviewerIdAndEnableTrue(id);
	}

	public List<Review> reviewFindByReviewerId(Long id) {
		return reviewRepository.findByReviewerIdAndEnableTrueAndVerifiedTrue(id);
	}

	/*
	 * Public Repository public boolean publicUserExistsByUsername(String username)
	 * { return publicUserRepository.existsByUsername(username); }
	 */

	/*
	 * public void savePublicUser(PublicUser publicUser) {
	 * publicUserRepository.saveAndFlush(publicUser); }
	 */

	/* Promotion Counter Repository */
	public void savePromotionCounter(PromotionCounter promotionCounter) {
		// parallelService.execute(() -> {
		promotionCounterRepository.save(promotionCounter);
		// });
	}

	public Optional<PromotionCounter> promotionCounterFindByStoreId(Long storeId) {
		return promotionCounterRepository.findByStoreId(storeId);
	}

	public Optional<PromotionCounter> promotionFindByStoreId(Long id) {
		return promotionCounterRepository.findByStoreId(id);
	}

	/* Promotion Repository */
	public void savePromotion(Promotion promotion) {
		// parallelService.execute(() -> {
		promotionRepository.save(promotion);
		// });

	}

	public void promotionDeleteById(Long id) {
		// parallelService.execute(() -> {
		promotionRepository.deleteById(id);
		// });

	}
	@Cacheable(cacheNames = {"store_promotions"},key = "#storeId")
	public List<Promotion> promotionFindByStoreIdOrderByCreatedOnDesc(Long storeId) {
		return promotionRepository.findByStoreIdOrderByCreatedOnDesc(storeId);
	}

	/* Subscription Customer Repository */
	public List<SubscriptionCustomer> subscriptionPlanFindByCustomerIdOrderByCreatedOnAsc(Long id) {
		return subscriptionCustomerRepository.findByCustomerIdOrderByCreatedOnAsc(id);
	}

	public void saveSubscriptionCustomer(SubscriptionCustomer customer) {
		// parallelService.execute(() -> {
		subscriptionCustomerRepository.saveAndFlush(customer);
		// });
	}

	public List<SubscriptionCustomer> subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(Long id,
			CUSTOMER_STATUS status) {
		return subscriptionCustomerRepository.findAllByStoreIdAndStatusOrderByCreatedOnAsc(id, status);
	}

	public List<SubscriptionCustomer> subscriptionCustomerFindAllByStoreIdOrderByCreatedOnAsc(Long id) {
		return subscriptionCustomerRepository.findAllByStoreIdOrderByCreatedOnAsc(id);
	}

	public Optional<SubscriptionCustomer> subscriptionCustomerFindById(Long customerId) {
		return subscriptionCustomerRepository.findById(customerId);
	}

	/* Query Repository */
	@Cacheable(cacheNames = {"store_queries"},key = "#id")
	public List<Query> queryFindByReviewerId(Long id) {
		return queryRepository.findByReviewerIdAndEnableTrueAndVerifiedTrue(id);
	}

	/* Query Repository */
	public List<Query> queryFindByReviewerId(Long id, boolean all) {
		return queryRepository.findByReviewerIdAndEnableTrue(id);
	}

	public void queryDeleteById(Long id) {
		// parallelService.execute(() -> {
		Query obj = this.queryFindById(id).get();
		obj.setEnable(false);
		this.saveQuery(obj);
		// });
	}

	public Query saveQuery(Query q) {
		// //parallelService.execute(() -> {
		return queryRepository.save(q);
		// //});
	}

	public Optional<Query> queryFindById(Long id) {
		return queryRepository.findById(id);
	}

	public List<Query> queryFindByStoreIdOrderByQueriedOnDesc(Long id) {
		return queryRepository.findByStoreIdAndEnableTrueAndVerifiedTrueOrderByQueriedOnDesc(id);
	}

	/* Action Items Repository */
	public void saveActionItem(ActionItem actionItem) {
		// parallelService.execute(() -> {
		actionItemRepository.saveAndFlush(actionItem);
		// });
	}

	public void actionItemDeleteById(Long reviewId) {
		// parallelService.execute(() -> {
		actionItemRepository.deleteById(reviewId);
		// });
	}

	public Optional<ActionItem> actionItemFindByReviewId(Long id) {
		return actionItemRepository.findByReviewId(id);
	}

	public List<ActionItem> actionItemFindByUserIdOrderByAddedOnDesc(Long id) {
		return actionItemRepository.findByUserIdOrderByAddedOnDesc(id);
	}

	/* Notification Repository */
	public Optional<NotificationBusiness> notificationFindById(Long notificationId) {
		return notificationBusinessRepository.findById(notificationId);
	}

	public Optional<NotificationPublic> notificationPublicFindById(Long notificationId) {
		return notificationPublicRepository.findById(notificationId);
	}

	@Cacheable(cacheNames = {"store_notifications"})
	public List<NotificationBusiness> notificationFindByStoreIdOrderByTimeDesc(Long id, boolean all) {
		return all ? notificationBusinessRepository.findByStoreIdOrderByTimeDesc(id)
				: notificationBusinessRepository.findByStoreIdAndIsReadFalseOrderByTimeDesc(id);
	}

	public List<NotificationBusiness> notificationFindByJobIdOrderByTimeDesc(Long id, boolean all) {
		return all ? notificationBusinessRepository.findByJobIdOrderByTimeDesc(id)
				: notificationBusinessRepository.findByJobIdAndIsReadFalseOrderByTimeDesc(id);
	}

	public void saveNotificationBusiness(NotificationBusiness notify) {
		// parallelService.execute(() -> {
		notificationBusinessRepository.save(notify);
		// });

	}

	public void saveNotificationPublic(NotificationPublic notify) {
		// parallelService.execute(() -> {
		notificationPublicRepository.save(notify);
		// });
	}

	public void saveNotificationPublic(String email, ACCESS_TYPE type, String message) {
		parallelService.execute(() -> {
			Optional<AccessToken> aToken = accessTokenRepository.findByEmailAndType(email, type);
			if (aToken.isPresent()) {
				NotificationPublic notify = new NotificationPublic(aToken.get(), message);
				notificationPublicRepository.save(notify);
			}
		});
	}

	@Cacheable(cacheNames = {"public_notifications"},key = "#token.id")
	public List<NotificationPublic> notificationPublic(AccessToken token) {
		return notificationPublicRepository.findAllByTokenIdAndIsReadFalse(token.getId());
	}

	/* Store Offerred Meal Plan Repository */
	public void saveStoreOfferredPlan(StoreOfferredPlan storeOfferredPlan) {
		storeOfferredPlanRepository.save(storeOfferredPlan);
	}

	@Cacheable(cacheNames = {"store_offerred_plan"},key = "#id")
	public List<StoreOfferredPlan> storeFindByStoreIdAndActiveTrue(Long id) {
		return storeOfferredPlanRepository.findByStoreIdAndActiveTrue(id);
	}

	// @Cacheable(value = "mealplan", key = "#planId")
	public Optional<StoreOfferredPlan> storeOfferredPlanFindById(Long planId) {
		return storeOfferredPlanRepository.findById(planId);
	}

	public List<StoreOfferredPlan> storeOfferredPlanFindByStoreId(Long id) {
		return storeOfferredPlanRepository.findByStoreId(id);
	}

	public List<StoreOfferredPlan> storeOfferredPlanFindByStoreIdAndActiveTrue(Long id) {
		return storeOfferredPlanRepository.findByStoreIdAndActiveTrue(id);
	}

	/* Store Repository */
	public void saveStore(Store store) throws Exception {
		if (store.getId() != null) {
			storeRepository.saveAndFlush(store);
		} else if (store.getId() == null
				&& !storeRepository.existsByStoreNameAndBusinessUser(store.getStoreName(), RMUtil.getSessionedUser()))
			storeRepository.saveAndFlush(store);
		else
			throw new Exception("Store name (" + store.getStoreName() + ") already exist");
	}

	public List<Store> storeFindByBusinessUserId(Long id) {
		return storeRepository.findByBusinessUserId(id);
	}

	public Optional<Store> storeFindById(Long storeId) {
		return storeRepository.findById(storeId);
	}

	public List<Store> storeFindAllByStoreNameContainingAndActiveTrue(String empty) {
		return storeRepository.findAllByStoreNameContainingAndActiveTrue(empty);
	}

	public void saveJobPosting(JobPosting jobPosting) {
		// parallelService.execute(() -> {
		jobPostingRepository.saveAndFlush(jobPosting);
		// });
	}

	public void deleteJobPosting(Long id) {
		// parallelService.execute(() -> {
		jobPostingRepository.deleteById(id);
		// });
	}

	public List<JobPosting> jobPostringFindAllByBusinessUserId(Long id) {
		return jobPostingRepository.findAllByBusinessUserIdOrderByModifiedOnDesc(id);
	}

	public List<JobPosting> jobPostringFindAll() {
		return jobPostingRepository.findAll();
	}

	public void saveDeliveryDriver(DeliveryDriver driver) {
		// parallelService.execute(() -> {
		Optional<Reviewer> reviewer = reviewerRepository.findByEmailAndFirstNameAndLastNameAndPhoneNumber(
				driver.getReviewer().getEmail(), driver.getReviewer().getFirstName(),
				driver.getReviewer().getLastName(), driver.getReviewer().getPhoneNumber());

		if (reviewer.isPresent()) {
			driver.setReviewer(reviewer.get());
			DeliveryDriver driverTemp = deliveryDriver.findByReviewerId(reviewer.get().getId());
			driver.setId(driverTemp.getId());
		}
		driver.setModifiedOn(new Timestamp(System.currentTimeMillis()));
		driver.setUsername(driver.getReviewer().getEmail());
		deliveryDriver.saveAndFlush(driver);
		// });
	}

	public List<DeliveryDriver> getDeliveryDrivers() {
		return deliveryDriver.findAllByAvailableTrue();
	}

	public DeliveryDriver getDeliveryDriver(String username) {
		return deliveryDriver.findByUsername(username).isPresent() ? deliveryDriver.findByUsername(username).get()
				: null;
	}

	public boolean existsByDeliveryJobApplication(Long jobId, Long driverId) {
		return deliveryJobApplicationRepository.existsByJobPostingIdAndDeliveryDriverId(jobId, driverId);
	}

	public void assignDeliveryJobApplication(Long jobId, Long driverId) {
		// parallelService.execute(() -> {
		JobPosting jobPosting = jobPostingRepository.findById(jobId).get();
		DeliveryDriver deliveryDriverObj = deliveryDriver.findById(driverId).get();
		DeliveryJobApplication job = new DeliveryJobApplication(null, jobPosting, deliveryDriverObj,
				JOBSTATUS.SELECTED);
		job.setBusinessUser(RMUtil.getSessionedUser());
		deliveryJobApplicationRepository.saveAndFlush(job);

		// send notification to public
		this.saveNotificationPublic(deliveryDriverObj.getReviewer().getEmail(), ACCESS_TYPE.DRIVER,
				"Congratulations ! You have been selected for food delivery job." + jobPosting.getName());
		// });
	}

	public void saveDeliveryJobApplication(Long jobId, Long driverId) {
		// parallelService.execute(() -> {
		deliveryJobApplicationRepository
				.saveAndFlush(new DeliveryJobApplication(null, jobPostingRepository.findById(jobId).get(),
						deliveryDriver.findById(driverId).get(), JOBSTATUS.APPLIED));
		// });
	}

	public void saveDeliveryJobApplication(DeliveryJobApplication deliveryJobApplication) {
		// parallelService.execute(() -> {
		deliveryJobApplicationRepository.saveAndFlush(deliveryJobApplication);
		// });
	}

	public DeliveryJobApplication getDeliveryJobApplication(Long id) {
		return deliveryJobApplicationRepository.findById(id).get();
	}

	public List<DeliveryJobApplication> getDeliveryJobApplications(Long driverId, JOBSTATUS status) {
		return deliveryJobApplicationRepository.findByDeliveryDriverIdAndStatusOrderByAppliedOnDesc(driverId, status);
	}

	public List<DeliveryJobApplication> getDeliveryJobApplications(Long driverId, Boolean showAll) {
		List<DeliveryJobApplication> apps = new ArrayList<DeliveryJobApplication>();
		if (showAll) {
			apps.addAll(deliveryJobApplicationRepository.findByDeliveryDriverIdOrderByAppliedOnDesc(driverId));
		} else {
			apps.addAll(this.getDeliveryJobApplications(driverId, JOBSTATUS.APPLIED));
			apps.addAll(this.getDeliveryJobApplications(driverId, JOBSTATUS.SELECTED));
		}

		return apps;
	}

	public List<DeliveryJobApplication> getDeliveryJobApplication(JobPosting job) {
		return deliveryJobApplicationRepository.findByJobPostingIdOrderByAppliedOnDesc(job.getId());
	}

	public List<DeliveryJobApplication> getDeliveryJobApplication(JobPosting job, JOBSTATUS status) {
		return deliveryJobApplicationRepository.findByJobPostingIdAndStatusOrderByAppliedOnDesc(job.getId(), status);
	}

	public void deleteJobApplication(Long id) {
		// parallelService.execute(() -> {
		deliveryJobApplicationRepository.deleteById(id);
		// });
	}

	public List<DeliveryJobApplication> getYourDrivers(Long id) {
		return deliveryJobApplicationRepository.findByBusinessUserIdOrderByAppliedOnDesc(id);
	}

	public Map<String, Object> getReport(Date start, Date end) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		responseData.put("customers",
				subscriptionCustomerRepository.findAllByCreatedOnGreaterThanEqualAndCreatedOnLessThanEqual(start, end));
		responseData.put("reviews", reviewRepository
				.findAllByReviedOnGreaterThanEqualAndReviedOnLessThanEqualAndEnableTrueAndVerifiedTrue(start, end));
		responseData.put("queries", queryRepository
				.findAllByQueriedOnGreaterThanEqualAndQueriedOnLessThanEqualAndEnableTrueAndVerifiedTrue(start, end));
		responseData.put("report", new SimpleDateFormat("MMMM YYYY").format(end));
		return responseData;
	}

	public void subscribePayAsGrowPlan(String customerId, int totalCustomers) throws Exception {
		// parallelService.execute(() -> {
		paymentService.createSubscription(customerId, totalCustomers);
		// });
	}

	public List<BusinessUser> businessUserFindAll() {
		return businessUserRepository.findAll();
	}

	public List<Demo> demoFindAll() {
		return demoRepository.findAll();
	}

	public List<SubscriptionCustomer> subscriptionCustomerFindAll() {
		return subscriptionCustomerRepository.findAll();
	}

	public Demo saveDemoRequest(Demo demo) {
		return demoRepository.saveAndFlush(demo);
	}

	public List<Errors> errorFindAll() {
		return errorRepository.findAll();
	}

	public void saveErrors(Errors error) {
		parallelService.execute(() -> {
			try {
				if (!StringUtils.isBlank(error.getMessage()) && !error.getMessage().equalsIgnoreCase("null"))
					errorRepository.saveAndFlush(error);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
	}

	public Optional<Demo> demoFindById(Long demoId) {
		return demoRepository.findById(demoId);
	}

	public void unsubscribe(String email) {
		parallelService.execute(() -> {
			emailSubscriptionRepository.findByEmail(email).stream().forEach(emails -> {
				emailSubscriptionRepository.deleteById(emails.getId());
			});
		});
	}

	public void confirmQuery(Long id, String email) {
		// parallelService.execute(() -> {
		Query query = this.queryFindById(id).get();
		if (query.getReviewer().getEmail().equalsIgnoreCase(email)) {
			query.setVerified(true);
			this.saveQuery(query);
		}
		// });
	}

	public void confirmReview(Long id, String email) {
		// parallelService.execute(() -> {
		Review review = this.reviewFindById(id).get();
		if (review.getReviewer().getEmail().equalsIgnoreCase(email)) {
			review.setVerified(true);
			this.saveReview(review);
		}
		// });
	}

	public Invitations sendInvitation(Invitations invite) {
		return invitationsRepository.saveAndFlush(invite);
	}

	public void deleteErrors(Errors error) {
		parallelService.execute(() -> {
			errorRepository.deleteById(error.getId());
		});
	}

}
