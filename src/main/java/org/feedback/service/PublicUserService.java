package org.feedback.service;

import java.util.List;
import java.util.Optional;

import org.feedback.pojo.Reviewer;
import org.feedback.repository.IReviewer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


@Service
public class PublicUserService {

	@Autowired(required = true)
	public IReviewer reviewerRepository;

	public Reviewer getReviewer(String email, String firstName, String lastName, String phoneNumber) {
		Optional<Reviewer> reviewer = reviewerRepository.findByEmailAndFirstNameAndLastNameAndPhoneNumber(email,
				firstName, lastName, phoneNumber);
		if (!reviewer.isPresent())
			return reviewerRepository.save(new Reviewer(firstName, lastName, phoneNumber,email));
		else
			return reviewer.get();
	}

	public List<Reviewer> getReviewerByEmail(String email) {
		return reviewerRepository.findByEmail(email);
	}

}
