package org.feedback.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.feedback.pojo.ServiceArea;
import org.feedback.repository.IServiceArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class LocationService {

	@Autowired(required = true)
	private IServiceArea serviceAreaRepository;

	public LocationService() {
		super();
	}

	public List<ServiceArea> getLocations() {
		return serviceAreaRepository.findAll().stream().distinct().collect(Collectors.toList());
	}

	public Set<ServiceArea> getLocations(ServiceArea area) {
		return serviceAreaRepository.findAllByCountryName(area.getCountryName());
	}

	public ServiceArea getLocationById(Long locationId) {
		Optional<ServiceArea> area = serviceAreaRepository.findById(locationId);
		if (area.isPresent())
			return area.get();
		else
			return null;
	}

	public ServiceArea getServiceArea(String cityName, String stateName, String country) {
		List<ServiceArea> areas = serviceAreaRepository.findAllByCityNameAndStateNameAndCountryName(cityName, stateName,
				country);
		if (areas != null && !areas.isEmpty())
			return areas.get(0);
		return null;
	}

	public ServiceArea addLocations(ServiceArea area) {
		if (!serviceAreaRepository.existsByCityNameAndStateNameAndCountryName(area.getCityName(), area.getStateName(),
				area.getCountryName()))
			return serviceAreaRepository
					.save(new ServiceArea(area.getCityName(), area.getStateName(), area.getCountryName()));
		else
			return this.getServiceArea(area.getCityName(), area.getStateName(), area.getCountryName());

	}

}
