package org.feedback.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.feedback.configuration.HomeKitchenCloudSecurity;
import org.feedback.pojo.AccessToken;
import org.feedback.pojo.BusinessUser;
import org.feedback.pojo.DeliveryDriver;
import org.feedback.pojo.Demo;
import org.feedback.pojo.EmailNotificationObject;
import org.feedback.pojo.Errors;
import org.feedback.pojo.Invitations;
import org.feedback.pojo.LabelValue;
import org.feedback.pojo.NotificationPublic;
import org.feedback.pojo.Query;
import org.feedback.pojo.Review;
import org.feedback.pojo.Reviewer;
import org.feedback.pojo.Role;
import org.feedback.pojo.ServiceArea;
import org.feedback.pojo.Store;
import org.feedback.pojo.SubscriptionCustomer;
import org.feedback.pojo.UserTemp;
import org.feedback.repository.IAccessToken;
import org.feedback.service.BusinessService;
import org.feedback.service.EmailService;
import org.feedback.service.LocationService;
import org.feedback.service.PublicUserService;
import org.feedback.service.StripePaymentService;
import org.feedback.utility.RMUtil;
import org.feedback.utility.RMUtil.ACCESS_TYPE;
import org.feedback.utility.RMUtil.CUSTOMER_STATUS;
import org.feedback.utility.RMUtil.DEMO_STATUS;
import org.feedback.utility.RMUtil.MEMBERSHIP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

	private static final String ERROR_MESSAGE = "errorMessage";

	private static final String SUCCESS = "success";

	private static final String TITLE = "title";

	private static final String MESSAGE2 = "message";

	@Autowired(required = true)
	IAccessToken accessTokenRepository;

	@Autowired
	BusinessService businessService;

	@Autowired(required = true)
	public LocationService locationService;

	@Autowired
	public StripePaymentService paymentService;

	@Autowired(required = true)
	PublicUserService publicUserService;

	@Autowired
	public EmailService emailService;
	

	@GetMapping(value = "/about")
	public ModelAndView about(HttpServletRequest request) {
		return new ModelAndView("redirect:/login#!/login/about");
	}

	@GetMapping(value = "/invite")
	public ModelAndView invite(HttpServletRequest request) {
		return new ModelAndView("redirect:/login#!/login/invite");
	}
	@GetMapping(value = "/privacy")
	public ModelAndView privacy(HttpServletRequest request) {
		return new ModelAndView("redirect:/login#!/login/privacy");
	}

	@GetMapping(value = "/terms")
	public ModelAndView terms(HttpServletRequest request) {
		return new ModelAndView("redirect:/login#!/login/terms");
	}

	@GetMapping(value = "/demo")
	public ModelAndView demo(HttpServletRequest request) {
		return new ModelAndView("redirect:/login#!/login/demo");
	}
	
	
	@RequestMapping(value = "/demo", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> demoRequest(HttpServletRequest request, @RequestBody Demo demo) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			demo.setStatus(DEMO_STATUS.NEW);
			
   		    demo.getAddress().setArea(locationService.addLocations(demo.getAddress().getArea()));
			
			responseData.put("result", "We have received your request for product demo. Your reference number is #"+businessService.saveDemoRequest(demo).getId());
			responseData.put(SUCCESS, true);
		}catch (Exception ex) { businessService.saveErrors(new Errors(ex));  
			ex.printStackTrace();
			responseData.put(SUCCESS, false);
			responseData.put("result", "Error while submitting demo request. Please try again.");
		}
		return responseData;
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> addUser(HttpServletRequest request, @RequestBody UserTemp newUserRequest) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Set<Role> authorities = new HashSet<>();
			newUserRequest.setPassword(HomeKitchenCloudSecurity.passwordEncoder().encode(newUserRequest.getPassword()));
			newUserRequest.setAccountNonExpired(true);
			newUserRequest.setAccountNonLocked(true);
			newUserRequest.setCredentialsNonExpired(true);
			newUserRequest.setEnabled(true);

			// Update Service Area,
			ServiceArea area  = locationService.addLocations(newUserRequest.getAddress().getArea());
			newUserRequest.getAddress().setArea(area);

			if (!businessService.businessUserExistsByUsername(newUserRequest.getUsername())) {
				authorities.add(new Role("BUSINESS_USER"));
				newUserRequest.setRoles(authorities);

				// create stripe user
				// Customer stripeCustomer = paymentService.createStripeUser(newUserRequest);
				BusinessUser user = RMUtil.getBusinessUser(newUserRequest, "FREE");
				user.setMembership(MEMBERSHIP.FREE);
				user.setAddStore(true);
				user.setAddMealPlan(true);
				user.setAddCustomer(true);
				user.setAddDriver(false);
				user.setAddJobs(false);
				businessService.saveBusinessUser(user);
				responseData.put(SUCCESS, true);
				responseData.put("result", "Congratulations! Business account has been created successfully. Please login using your credentials.");
			}  else {
				
				responseData.put("result", "Username already exists.");
				responseData.put(SUCCESS, false);
			}

		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			responseData.put("result", "Error while creating user.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@GetMapping(value = "/contact")
	public ModelAndView contact(HttpServletRequest request) {
		return new ModelAndView("redirect:/landing/public#!/public/contact");
	}

	@GetMapping(value = "/feedback")
	public ModelAndView feedback(HttpServletRequest request) {
		return new ModelAndView("redirect:/landing/public#!/public/feedback");
	}

	@GetMapping(value = "/driver")
	public ModelAndView driver(HttpServletRequest request, HttpSession session) {

		if (session.getAttribute("driver_email") != null && session.getAttribute("driver_token") != null)
			return new ModelAndView("redirect:/login#!/login/driver");
		else
			return new ModelAndView("redirect:/login#!/login/authdriver");
	}

	@GetMapping(value = "/public")
	public ModelAndView publicData(HttpServletRequest request, HttpSession session) {
		if (session.getAttribute("public_email") != null && session.getAttribute("public_token") != null)
			return new ModelAndView("redirect:/login#!/login/public");
		else
			return new ModelAndView("redirect:/login#!/login/authpublic");

	}

	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> forgotPassword(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {

			String userName = params.get("clientEmail").toString();
			String pwd = RMUtil.generateToken();
			BusinessUser bUser = businessService.businessUserFindByUsername(userName);
			bUser.setPassword(HomeKitchenCloudSecurity.passwordEncoder().encode(pwd));
			businessService.saveBusinessUser(bUser);
			responseData.put("newPassword", pwd);
			responseData.put("result", "Your password has been changed");
		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			responseData.put("result", "Error while resetting password. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/getToken", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getToken(HttpServletRequest request,
			@RequestBody(required = true) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			String email = params.get("email").toString();
			String type = params.get("type").toString();
			String tokenStr = "";
			Boolean agreeTermsAndCondi = Boolean.valueOf(params.get("agree").toString());
			Optional<AccessToken> aToken = accessTokenRepository.findByEmailAndType(email,
					type.equalsIgnoreCase("public") ? ACCESS_TYPE.PUBLIC : ACCESS_TYPE.DRIVER);
			if (aToken.isPresent()) {
				tokenStr = aToken.get().getToken();
				responseData.put(MESSAGE2, "User profile already exists.");
				responseData.put(SUCCESS, true);
			} else {
				if(agreeTermsAndCondi) {
				AccessToken token = new AccessToken(email, RMUtil.generateToken(),
						new Timestamp(System.currentTimeMillis()),
						type.equalsIgnoreCase("public") ? ACCESS_TYPE.PUBLIC : ACCESS_TYPE.DRIVER);
				token.setAgreeTermsAndCondi(agreeTermsAndCondi);
				accessTokenRepository.saveAndFlush(token);
				tokenStr = token.getToken();
				responseData.put(MESSAGE2, "User profile created successfully.");
				responseData.put(SUCCESS, true); }
				else {
					responseData.put(MESSAGE2, "Error! User profile does not exists.");
					responseData.put(SUCCESS, false);
				}
			}
			
			responseData.put("token", "");
			
			emailService.sendEmailNotification(new EmailNotificationObject(email, "User", "You have received access token.", 
					"Please use below token to log into "+ (type.equalsIgnoreCase("public") ? "<a href='http://www.meaily.com/landing/public#!/public/authpublic'>Public User</a>" : "<a href='http://www.meaily.com/landing/public#!/public/authdriver'>Delivery Driver</a>") + " Portal. "
							+"<br><br>Token&nbsp;:&nbsp;<strong>"+tokenStr+"</strong>" ));

		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			responseData.put(SUCCESS, false);
			responseData.put(MESSAGE2, "Error while resetting password. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}

		return responseData;
	}

	@GetMapping(value = "/help")
	public ModelAndView help(HttpServletRequest request) {
		return new ModelAndView("redirect:/landing/public#!/public/help");
	}
	
	

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return new ModelAndView("redirect:/auth/login?logout");
	}

	@PostMapping("/logoutuser")
	public @ResponseBody Map<String, Object> logoutuser(HttpServletRequest request, HttpSession session,
			@RequestBody(required = true) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			String type = params.get("type").toString().toLowerCase();
			if (type.equalsIgnoreCase("public")) {
				session.removeAttribute("public_email");
				session.removeAttribute("public_token");
			} else {
				session.removeAttribute("driver_email");
				session.removeAttribute("driver_token");
			}
			responseData.put(SUCCESS, true);
		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			responseData.put(ERROR_MESSAGE, "Invalid Email/Token. Please try again.");
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	

	@GetMapping(value = "/")
	public ModelAndView publics(HttpServletRequest request) {
		return new ModelAndView("redirect:/landing/public");
	}

	// Running 1st day of every month at mid night - reset tokens
	@Scheduled(cron = "0 0 12 1 * *")
	public void resetTokens() {
		try {
			List<AccessToken> tokens = accessTokenRepository.findAll();
			for (AccessToken token : tokens) {
				token.setToken(RMUtil.generateToken());
				token.setReset(new Timestamp(System.currentTimeMillis()));
				accessTokenRepository.save(token);
			}
		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			ex.printStackTrace();
		}
	}
	
	// update customer status to inactive if subscription expired
	@Scheduled(cron = "0 0 0 * * *")
	public void updateCustomerStatus() {
		for (SubscriptionCustomer user : businessService.subscriptionCustomerFindAll()) {
			try {
				
				if(user.getSubscriptions().stream().filter(sub -> RMUtil.isExpired(sub.getEndDate())).count() > 0) {
					user.setStatus(CUSTOMER_STATUS.INACTIVE);
					businessService.saveSubscriptionCustomer(user);
				}

				
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	//update billing customers
	@Scheduled(cron = "0 0 3 * * *")
	public void updateBilling() {
		List<SubscriptionCustomer> customers = new ArrayList<SubscriptionCustomer>();
		List<Store> stores = new ArrayList<Store>();
		
		for (BusinessUser user : businessService.businessUserFindAll()) {
			try {
				 stores = businessService.storeFindByBusinessUserId(user.getId());
				 customers.clear();
				stores.stream().forEach(store -> {
					customers.addAll(businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(store.getId(), CUSTOMER_STATUS.ACTIVE));
					customers.addAll(businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(store.getId(), CUSTOMER_STATUS.INACTIVE));
					customers.addAll(businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(store.getId(), CUSTOMER_STATUS.CANCELLED));
				});

				businessService.subscribePayAsGrowPlan(user.getPaymentId(), customers.size());
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	@RequestMapping(value = "/login")
	public ModelAndView reviewAuth(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		ModelAndView landingView = new ModelAndView("login");
		landingView.addObject("error", params.containsKey("error") ? getErrorMessage(request) : "");
		landingView.addObject("logout", params.containsKey("logout") ? "You have been logged out successfully." : "");
		landingView.addObject("public", "");
		return landingView;
	}

	private String getErrorMessage(HttpServletRequest request) {

		StringBuffer errorMessage = new StringBuffer();
		Enumeration<String> keys = request.getSession().getAttributeNames();
		while (keys.hasMoreElements()) {
			try {
				Exception exception = (Exception) request.getSession().getAttribute(keys.nextElement());

				if (exception instanceof BadCredentialsException) {
					errorMessage.append("Invalid username and password!");
				} else if (exception instanceof LockedException) {
					errorMessage.append("Your Business account has been locked.. Please contact support.");
				} else if (exception instanceof DisabledException) {
					errorMessage.append("Your Business account has been deactivated. Please contact support.");
				} else {
					errorMessage.append("Invalid username and password!");
				}
			} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
				ex.printStackTrace();
				// errorMessage.append(ex.getMessage());
			}
		}
		return errorMessage.toString();
	}
	
	

	@PostMapping("/validateauth")
	public @ResponseBody Map<String, Object> validateauth(HttpServletRequest request,
			@RequestBody(required = true) Map<String, Object> params, HttpSession session) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			String email = params.get("email").toString();
			String token = params.get("token").toString();
			String type = params.get("type").toString().toLowerCase();
			Optional<AccessToken> aToken = accessTokenRepository.findByEmailAndType(email,
					type.equalsIgnoreCase("public") ? ACCESS_TYPE.PUBLIC : ACCESS_TYPE.DRIVER);
			if (aToken.isPresent() && aToken.get().getToken().equalsIgnoreCase(token)) {

				session.setAttribute(type + "_email", email);
				session.setAttribute(type + "_token", token);

				responseData.put(SUCCESS, true);

			} else {

				responseData.put(SUCCESS, false);
				responseData.put(ERROR_MESSAGE, "Invalid Token / User does not exists.");

			}

		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			responseData.put(ERROR_MESSAGE, "Invalid Email/Token. Please try again.");
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@GetMapping(value = "/yourdata")
	public @ResponseBody Map<String, Object> yourData(HttpServletRequest request, HttpSession session) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			String email = session.getAttribute("public_email").toString();
			String token = session.getAttribute("public_token").toString();
			Optional<AccessToken> aToken = accessTokenRepository.findByEmailAndType(email, ACCESS_TYPE.PUBLIC);
			if (aToken.isPresent() && aToken.get().getToken().equalsIgnoreCase(token)) {
				List<Reviewer> reviewers = publicUserService.getReviewerByEmail(email);
				List<Review> reviews = new ArrayList<Review>();
				List<Query> queries = new ArrayList<Query>();
				List<SubscriptionCustomer> mealplans = new ArrayList<SubscriptionCustomer>();
				List<LabelValue> emails = new ArrayList<LabelValue>();
				reviewers.stream().forEach(rwier -> {
					reviews.addAll(businessService.reviewFindByReviewerId(rwier.getId(),true));
					queries.addAll(businessService.queryFindByReviewerId(rwier.getId(),true));
					mealplans
							.addAll(businessService.subscriptionPlanFindByCustomerIdOrderByCreatedOnAsc(rwier.getId()));
				});
				responseData.put("profile", reviewers);
				responseData.put("reviews", reviews.stream().filter(plan -> {
					plan.setStore(null);
					return true;
				}).collect(Collectors.toList()));
				responseData.put("queries", queries.stream().filter(plan -> {
					plan.setStore(null);
					return true;
				}).collect(Collectors.toList()));
				responseData.put("mealplans", mealplans.stream().filter(plan -> {
					plan.setStore(null);
					return true;
				}).collect(Collectors.toList()));
				businessService.emailSubscriptionFindByEmail(email).stream().forEach(k -> {
					LabelValue lv = new LabelValue(String.valueOf(k.getId()),
							"You have subscribed to " + k.getStore().getStoreName());
					lv.setLabel(k.getCreatedOn().toString());
					emails.add(lv);
				});

				responseData.put("notifications", businessService.notificationPublic(aToken.get()));
				responseData.put("subscriber", emails);
				responseData.put("email",email);
				responseData.put(SUCCESS, true);
			} else {
				responseData.put(SUCCESS, false);
				responseData.put(ERROR_MESSAGE, "Invalid Token / User does not exists.");
			}
		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			responseData.put(ERROR_MESSAGE, "Invalid Email/Token. Please try again.");
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@PostMapping(value = "/driverdata")
	public @ResponseBody Map<String, Object> driverdata(HttpServletRequest request, HttpSession session,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Boolean showAll = Boolean.valueOf(params.get("showAll").toString());
			String email = session.getAttribute("driver_email").toString();
			String token = session.getAttribute("driver_token").toString();
			Optional<AccessToken> aToken = accessTokenRepository.findByEmailAndType(email, ACCESS_TYPE.DRIVER);
			if (aToken.isPresent() && aToken.get().getToken().equalsIgnoreCase(token)) {
				DeliveryDriver driver = businessService.getDeliveryDriver(email);
				responseData.put("profile", driver);
				if (driver != null)
					responseData.put("applications",
							businessService.getDeliveryJobApplications(driver.getId(), showAll));
				responseData.put("notifications", businessService.notificationPublic(aToken.get()));
				responseData.put("email", email);
				responseData.put(SUCCESS, true);
			} else {
				responseData.put(SUCCESS, false);
				responseData.put(ERROR_MESSAGE, "Invalid Token / User does not exists.");
			}
		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			responseData.put(ERROR_MESSAGE, "Invalid Email/Token. Please try again.");
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@RequestMapping(value = "/readNotification", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> markNotification(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Long notificationId = Long.valueOf(params.get("notificationID").toString());
			NotificationPublic notify = businessService.notificationPublicFindById(notificationId).get();
			notify.setRead(true);
			businessService.saveNotificationPublic(notify);
			responseData.put(SUCCESS, notify);
		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}
	
	@RequestMapping(value = "/sendinvite", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> sendInvite(HttpServletRequest request,
			@RequestBody(required = false) Invitations invite) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Invitations invitations = businessService.sendInvitation(invite);
			emailService.sendEmailNotification(new EmailNotificationObject(invitations.getEmail(), invitations.getInviteeName(),"You have been invited to Meaily by "+invitations.getInviterName(), "You have been invited to Meaily by "+invitations.getInviterName()+" Click here to learn more about our services <a href='http://www.meaily.com'>Meaily</a>. <br><p>"+invitations.getInviterName()+"'s message for you</p><blockquote>"+invitations.getMessage()+"</blockquote>"));
			responseData.put(SUCCESS, true);
		} catch (Exception ex) { businessService.saveErrors(new Errors(ex));
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}
	
	@RequestMapping(value = "/unsubscribe",method = RequestMethod.GET)
	public ModelAndView unsubscribe(HttpServletRequest request,
			@RequestParam(required = true)String q) {
		String message = "You email "+ q+ " has been removed from subscription services. You will not receive any communication emails from store(s) or us.";
		try {
		businessService.unsubscribe(q);
		}catch(Exception ex) {
			message = "Something went wrong while unsubscribing  "+ q+ ". Please try after sometime.";
		}
		ModelAndView model = new ModelAndView("redirect:/landing/public#!/public/message");
		model.addObject(MESSAGE2, message);
		model.addObject(TITLE, "Unsubscribe");
		return model;
	}
	
	@RequestMapping(value = "/confirmr",method = RequestMethod.GET)
	public ModelAndView confirmr(HttpServletRequest request,	@RequestParam(required = true)String e,@RequestParam(required = true)Long q) {
		String message = "Your review has been verified.";
		try {
		businessService.confirmReview(q,e);
		}catch(Exception ex) {
			message = "Something went wrong while confirming review  "+ e+ ". Please try after sometime.";
		}
		ModelAndView model = new ModelAndView("redirect:/landing/public#!/public/message");
		model.addObject(MESSAGE2, message);
		model.addObject(TITLE, "Review Confirmation");
		return model;
	}
	
	@RequestMapping(value = "/confirmq",method = RequestMethod.GET)
	public ModelAndView confirmq(HttpServletRequest request,@RequestParam(required = true)String e,@RequestParam(required = true)Long q) {
		String message = "Your query has been verified.";
		try {
		businessService.confirmQuery(q,e);
		}catch(Exception ex) {
			message = "Something went wrong while confirming query  "+ e+ ". Please try after sometime.";
		}
		ModelAndView model = new ModelAndView("redirect:/landing/public#!/public/message");
		model.addObject(MESSAGE2, message);
		model.addObject(TITLE, "Query Confirmation");
		return model;
	}
	
}
