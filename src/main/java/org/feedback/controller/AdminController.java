package org.feedback.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.feedback.pojo.AccessToken;
import org.feedback.pojo.BusinessUser;
import org.feedback.pojo.Demo;
import org.feedback.pojo.Errors;
import org.feedback.pojo.Store;
import org.feedback.pojo.SubscriptionCustomer;
import org.feedback.service.BusinessService;
import org.feedback.utility.RMUtil.CUSTOMER_STATUS;
import org.feedback.utility.RMUtil.DEMO_STATUS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/meailymaster")
public class AdminController {

	private static final String RESPONSE_SUCCESS = "success";
	private static final String ADMIN_SESSION_ATTR_TOKEN = "admin_token";
	private static final String ADMIN_SESSION_ATTR_EMAIL = "admin_email";
	
	@Value("${admin.password}")
	private String ADMIN_PASSWORD;
	@Value("${admin.username}")
	private String ADMIN_MEALMASTER_COM;
	
	@Autowired
	BusinessService businessService;

	public AdminController() {
		super();
	}

	@RequestMapping(value = { "", "/", "/about" }, method = RequestMethod.GET)
	public ModelAndView indexPage(HttpServletRequest request,HttpSession session) {
		if (session.getAttribute(ADMIN_SESSION_ATTR_EMAIL) != null && session.getAttribute(ADMIN_SESSION_ATTR_TOKEN) != null)
			return new ModelAndView("redirect:/login#!/login/master");
		else
			return new ModelAndView("redirect:/login#!/login/authmaster");

	}
	
	@PostMapping("/validateauth")
	public @ResponseBody Map<String, Object> validateauth(HttpServletRequest request,
			@RequestBody(required = true) Map<String, Object> params, HttpSession session) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			 
			String email = params.get("email").toString();
			String token = params.get("token").toString();
			AccessToken aToken = new AccessToken(ADMIN_MEALMASTER_COM, ADMIN_PASSWORD, null,null); 
			if (aToken.getToken().equalsIgnoreCase(token) && aToken.getEmail().equalsIgnoreCase(email)) {
				session.setAttribute(ADMIN_SESSION_ATTR_EMAIL, aToken.getEmail());
				session.setAttribute(ADMIN_SESSION_ATTR_TOKEN, aToken.getToken());
				responseData.put(RESPONSE_SUCCESS, true);
			} else {
				responseData.put(RESPONSE_SUCCESS, false);
				responseData.put("errorMessage", "Invalid Token / User does not exists.");
			}

		} catch (Exception ex) {
			responseData.put("errorMessage", "Invalid Email/Token. Please try again.");
			responseData.put(RESPONSE_SUCCESS, false);
		}
		return responseData;
	}

	@GetMapping("/platform")
	public @ResponseBody Map<String, Object> getPlatformData(HttpServletRequest request, HttpSession session) {
		Map<String, Object> data = new HashMap<>();
		try {
			
			if(!session.getAttribute(ADMIN_SESSION_ATTR_EMAIL).equals(ADMIN_MEALMASTER_COM) ||  !session.getAttribute(ADMIN_SESSION_ATTR_TOKEN).equals(ADMIN_PASSWORD)) {
				throw new Exception("admin not autheticated.");
			}
			data.put(RESPONSE_SUCCESS,true);
			data.put("demos", businessService.demoFindAll().stream().filter(demo -> demo.getStatus().equals(DEMO_STATUS.NEW)).collect(Collectors.toList()));
			data.put("users", businessService.businessUserFindAll());
			data.put("health", businessService.errorFindAll().stream().limit(100));
		} catch (Exception ex) {
			ex.printStackTrace();
			data.put(RESPONSE_SUCCESS,false);
		}
		return data;
	}

	@PostMapping("/activate")
	public @ResponseBody Map<String, Object> activate(HttpServletRequest request,
			@RequestBody(required = true) Map<String, Object> params, HttpSession session) {
		Map<String, Object> data = new HashMap<>();
		try {
			if(!session.getAttribute(ADMIN_SESSION_ATTR_EMAIL).equals(ADMIN_MEALMASTER_COM) ||  !session.getAttribute(ADMIN_SESSION_ATTR_TOKEN).equals(ADMIN_PASSWORD)) {
				throw new Exception("admin not autheticated.");
			}
			String username = params.get("username").toString();
			BusinessUser user = businessService.businessUserFindByUsername(username);
			user.setEnabled(true);
			businessService.saveBusinessUser(user);
			data.put(RESPONSE_SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			data.put(RESPONSE_SUCCESS, false);
		}
		return data;
	}

	@PostMapping("/billing")
	public @ResponseBody Map<String, Object> billing(HttpServletRequest request,
			@RequestBody(required = true) Map<String, Object> params, HttpSession session) {
		Map<String, Object> data = new HashMap<>();
		List<SubscriptionCustomer> customers = new ArrayList<>();
		List<Store> stores = new ArrayList<>();
		try {
			if(!session.getAttribute(ADMIN_SESSION_ATTR_EMAIL).equals(ADMIN_MEALMASTER_COM) ||  !session.getAttribute(ADMIN_SESSION_ATTR_TOKEN).equals(ADMIN_PASSWORD)) {
				throw new Exception("admin not autheticated.");
			}
			String username = params.get("username").toString();
			BusinessUser user = businessService.businessUserFindByUsername(username);
			stores = businessService.storeFindByBusinessUserId(user.getId());
			customers.clear();
			stores.stream().forEach(store -> {
				customers.addAll(businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(
						store.getId(), CUSTOMER_STATUS.ACTIVE));
				customers.addAll(businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(
						store.getId(), CUSTOMER_STATUS.INACTIVE));
				customers.addAll(businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(
						store.getId(), CUSTOMER_STATUS.CANCELLED));
			});

			businessService.subscribePayAsGrowPlan(user.getPaymentId(), customers.size());
			data.put(RESPONSE_SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			data.put(RESPONSE_SUCCESS, false);
		}
		return data;
	}

	@PostMapping("/delerror")
	public @ResponseBody Map<String, Object> demo(HttpServletRequest request,
			@RequestBody(required = true) Errors error) {
		Map<String, Object> data = new HashMap<>();
		try {
			businessService.deleteErrors(error);
			data.put(RESPONSE_SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			data.put(RESPONSE_SUCCESS, false);
		}
		return data;
	}
	
	@PostMapping("/demo")
	public @ResponseBody Map<String, Object> demo(HttpServletRequest request,
			@RequestBody(required = true) Map<String, Object> params, HttpSession session) {
		Map<String, Object> data = new HashMap<>();
		try {
			if(!session.getAttribute(ADMIN_SESSION_ATTR_EMAIL).equals(ADMIN_MEALMASTER_COM) ||  !session.getAttribute(ADMIN_SESSION_ATTR_TOKEN).equals(ADMIN_PASSWORD)) {
				throw new Exception("admin not autheticated.");
			}
			Long demoId = Long.valueOf(params.get("demo").toString());
			Demo demo = businessService.demoFindById(demoId).get();
			demo.setStatus(DEMO_STATUS.COMPLETED);
			businessService.saveDemoRequest(demo);
			data.put(RESPONSE_SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			data.put(RESPONSE_SUCCESS, false);
		}
		return data;
	}

}
