package org.feedback.controller;

import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.feedback.configuration.HomeKitchenCloudSecurity;
import org.feedback.pojo.ActionItem;
import org.feedback.pojo.BusinessUser;
import org.feedback.pojo.DeliveryDriver;
import org.feedback.pojo.DeliveryJobApplication;
import org.feedback.pojo.EmailNotificationObject;
import org.feedback.pojo.EmailSubscription;
import org.feedback.pojo.Errors;
import org.feedback.pojo.JobPosting;
import org.feedback.pojo.NotificationBusiness;
import org.feedback.pojo.Promotion;
import org.feedback.pojo.PromotionCounter;
import org.feedback.pojo.Query;
import org.feedback.pojo.Reply;
import org.feedback.pojo.Review;
import org.feedback.pojo.ServiceArea;
import org.feedback.pojo.Store;
import org.feedback.pojo.StoreOfferredPlan;
import org.feedback.pojo.StoreTemp;
import org.feedback.pojo.SubscriptionCustomer;
import org.feedback.pojo.UserTemp;
import org.feedback.service.BusinessService;
import org.feedback.service.EmailService;
import org.feedback.service.LocationService;
import org.feedback.service.StripePaymentService;
import org.feedback.utility.RMUtil;
import org.feedback.utility.RMUtil.ACCESS_RIGHT;
import org.feedback.utility.RMUtil.ACCESS_TYPE;
import org.feedback.utility.RMUtil.CUSTOMER_STATUS;
import org.feedback.utility.RMUtil.JOBSTATUS;
import org.feedback.utility.RMUtil.MEMBERSHIP;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.stripe.model.Customer;

@Controller
@RequestMapping("/business")
public class BusinessController {

	private static final String LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN = "limit reached for free membership plan";

	private static final String MESSAGE = "message";

	private static final String ERROR_MESSAGE = "errorMessage";

	private static final String SUCCESS = "success";

	private static final String ERROR = "error";

	private static final String RESULT = "result";

	@Value("${meaily.plan.free.mealplanperstore.limit}")
	private int FREE_CUSTOMER_MEAL_PLANS_LIMIT;
	@Value("${meaily.plan.free.storecustomer.limit}")
	private int FREE_CUSTOMER_MEAL_PLAN_CUSTOMER_LIMIT;
	@Value("${meaily.plan.paid.mealplanperstore.limit}")
	private int SUBSCRIBED_CUSTOMER_MEAL_PLANS_LIMIT;

	@Value("${meaily.stripe.secret}")
	private String STRIPE_SECRET_KEY;
	@Value("${meaily.stripe.publishable}")
	private String STRIPE_PUBLISHABLE_KEY;

	@Value("${meaily.mapbox.secret}")
	private String MAPBOX_SECRET_KEY;
	
	@Value("${meaily.cloudinary.key}")
	private String CLOUDINARY_KEY = "665184582956882";
	@Value("${meaily.cloudinary.secret}")
	private String CLOUDINARY_SECRET = "Khe80I9ljeoacl5q7HsO6YHgzyk";
	@Value("${meaily.cloudinary.cloudname}")
	private String CLOUDINARY_NAME = "mealforest-com";

	@Autowired
	BusinessService businessService;

	@Autowired(required = true)
	public LocationService locationService;

	@Autowired
	public StripePaymentService paymentService;

	@Autowired
	public EmailService emailService;

	public BusinessController() {
		super();
	}

	@RequestMapping(value = "/addActionItem", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> addActionItem(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Long reviewId = Long.valueOf(params.get("reviewId").toString());
			Review review = businessService.reviewFindById(reviewId).get();
			if (!businessService.actionItemFindByReviewId(review.getId()).isPresent()) {
				businessService.saveActionItem(new ActionItem(null, review, RMUtil.getSessionedUser(),
						new Timestamp(System.currentTimeMillis())));
				responseData.put(RESULT, "CREATED");
			} else {
				responseData.put(RESULT, "CREATED");
			}
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while adding action item");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/deliverySchedule", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deliverySchedule(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			Timestamp scheduleDate = Timestamp.valueOf(params.get("schedule").toString());

			List<Store> stores = businessService.storeFindByBusinessUserId(RMUtil.getSessionedUser().getId());
			List<SubscriptionCustomer> customers = new ArrayList<SubscriptionCustomer>();
			stores.stream().forEach(store -> {
				customers.addAll(businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(
						store.getId(), CUSTOMER_STATUS.ACTIVE));
			});

			List<SubscriptionCustomer> filtered = customers.stream()
					.filter(cust -> cust.getSubscriptions().stream()
							.filter(sub -> RMUtil.isBetween(scheduleDate, sub.getStartDate(), sub.getEndDate()))
							.filter(sub -> RMUtil.checkDayAvailability(sub.getStoreOfferredPlan(), scheduleDate))
							.count() > 0)
					.collect(Collectors.toList());

			responseData.put("customers", filtered);
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(MESSAGE, "Error while gettting delivery schedule. Please try again sometime later.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/updateStorePolicy", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateStorePolicy(HttpServletRequest request,
			@RequestBody(required = false) StoreTemp store) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Store newStore = businessService.storeFindById(store.getId()).get();
			newStore.setStorePolicies(store.getStorePolicies());
			businessService.saveStore(newStore);
			responseData.put(MESSAGE, SUCCESS);
		} catch (Exception ex) {
			responseData.put(ERROR_MESSAGE, "Error while updating policies. Please try again sometime later.");
			responseData.put(MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/addStore", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> addStore(HttpServletRequest request,
			@RequestBody(required = false) StoreTemp store) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			Optional<BusinessUser> businessUser = businessService
					.businessUserFindById(RMUtil.getSessionedUser().getId());
			if (store.getId() == null && !RMUtil.isAllowed(ACCESS_RIGHT.ADD_STORE, businessUser.get())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}

			Store newStore = new Store(store);
			// Update Service Area,
			ServiceArea area = locationService.addLocations(newStore.getAddress().getArea());
			newStore.getAddress().setArea(area);
			newStore.addBusinessUserStore(businessUser.get());
			newStore.setServiceArea(store.getStoreServiceArea());
			businessService.saveStore(newStore);

			if (businessUser.get().getMembership().equals(MEMBERSHIP.FREE)) {
				BusinessUser user = businessUser.get();
				user.setAddStore(false);
				businessService.saveBusinessUser(user);
				RMUtil.getSessionedUser().setAddStore(false);
			}

			responseData.put(SUCCESS, true);
			if (store.getId() != null)
				responseData.put(MESSAGE, "Store updated successfully.");
			else
				responseData.put(MESSAGE, "Store created successfully.");
		} catch (Exception ex) {
			responseData.put(ERROR_MESSAGE, "Error while adding store. Please try again sometime later.");
			responseData.put(MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> changePassword(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			String newPasswordAgain = params.containsKey("newPasswordAgain") ? params.get("newPasswordAgain").toString()
					: StringUtils.EMPTY;
			String newPassword = params.containsKey("newPassword") ? params.get("newPassword").toString()
					: StringUtils.EMPTY;
			String oldPassword = params.containsKey("oldPassword") ? params.get("oldPassword").toString()
					: StringUtils.EMPTY;

			if (newPasswordAgain == StringUtils.EMPTY || newPassword == StringUtils.EMPTY
					|| oldPassword == StringUtils.EMPTY || !newPasswordAgain.equalsIgnoreCase(newPassword)) {
				throw new Exception("password doesn't match");
			}
			BusinessUser businessUserFromDB = businessService
					.businessUserFindByUsername(RMUtil.getSessionedUser().getUsername());

			BCryptPasswordEncoder crypt = HomeKitchenCloudSecurity.passwordEncoder();
			if (crypt.matches(oldPassword, businessUserFromDB.getPassword())) {
				businessUserFromDB.setPassword(new BCryptPasswordEncoder(8, new SecureRandom()).encode(newPassword));
				businessService.saveBusinessUser(businessUserFromDB);
				responseData.put(RESULT, "Your password has been changed successfully.");
				responseData.put(SUCCESS, true);
			} else {
				responseData.put(RESULT, "Existing password does not match");
				responseData.put(SUCCESS, false);
			}

		} catch (Exception ex) {
			responseData.put(RESULT, "Error while changing password");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/createPromotion", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> createPromotions(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			String title = (String) params.get("title");
			String detail = (String) params.get("detail");
			Timestamp startDate = RMUtil.getDate(params.get("startDate").toString());
			Timestamp endDate = RMUtil.getDate(params.get("endDate").toString());

			Long id = params.containsKey("id") && params.get("id") != null ? Long.valueOf(params.get("id").toString())
					: null;
			Long storeId = params.containsKey("store") && params.get("store") != null
					? Long.valueOf(params.get("store").toString())
					: null;

			Promotion promotion = new Promotion(id, title, detail, startDate, endDate);
			if (storeId != null) {
				Optional<Store> st = businessService.storeFindById(storeId);
				promotion.setStore(st.isPresent() ? st.get() : null);
			}
			businessService.savePromotion(promotion);
			responseData.put(RESULT, promotion);
		} catch (Exception ex) {
			responseData.put(RESULT, "Error while creating promotion. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/customer", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> customer(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Long custID = Long.valueOf(params.get("id").toString());
			SubscriptionCustomer customer = businessService.subscriptionCustomerFindById(custID).get();

			// update address
			String country = params.get("country").toString();
			String city = params.get("city").toString();
			String province = params.get("province").toString();

			customer.getAddress().setArea(locationService.addLocations(new ServiceArea(city, province, country)));

			customer.getAddress().setPostal(params.get("postal").toString());
			customer.getAddress().setStreet(params.get("street").toString());
			customer.getAddress().setLat(Double.valueOf(params.get("lat").toString()));
			customer.getAddress().setLon(Double.valueOf(params.get("lon").toString()));

			// update customer
			customer.getCustomer().setEmail(params.get("email").toString());
			customer.getCustomer().setFirstName(params.get("firstName").toString());
			customer.getCustomer().setLastName(params.get("lastName").toString());
			customer.getCustomer().setPhoneNumber(params.get("phoneNumber").toString());

			if (!params.get("endDate").toString().contains("Invalid"))
				customer.getSubscriptions().get(0).setEndDate(RMUtil.getDate(params.get("endDate").toString()));
			if (!params.get("startDate").toString().contains("Invalid"))
				customer.getSubscriptions().get(0).setStartDate(RMUtil.getDate(params.get("startDate").toString()));

			businessService.saveSubscriptionCustomer(customer);

			responseData.put(SUCCESS, true);

		} catch (Exception ex) {
			responseData.put(SUCCESS, false);
			responseData.put("data", "Error while getting customerdata. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));

		}

		return responseData;

	}

	@RequestMapping(value = "/customerdata", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> customerData(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Long custID = Long.valueOf(params.get("id").toString());
			SubscriptionCustomer customer = businessService.subscriptionCustomerFindById(custID).get();
			responseData.put("store", customer.getStore().getStoreName());
			responseData.put("currency", customer.getStore().getServiceArea().stream().findFirst().get().getCurrency());
			customer.setStore(null);
			responseData.put("data", customer);
		} catch (Exception ex) {

			responseData.put("data", "Error while getting customerdata. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));

		}

		return responseData;

	}

	@RequestMapping(value = "/customerstatus", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> customerStatus(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Long customerId = Long.valueOf(params.get("customer").toString());
			String status = params.get("status").toString();
			String comment = params.get("comment").toString();
			SubscriptionCustomer customer = businessService.subscriptionCustomerFindById(customerId).get();
			Optional<BusinessUser> businessUser = businessService
					.businessUserFindById(RMUtil.getSessionedUser().getId());
			if (!RMUtil.isAllowed(ACCESS_RIGHT.ADD_CUSTOMER, businessUser.get())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}
			customer.setBusinessComment(comment);
			switch (status) {
			case "new":
				customer.setStatus(CUSTOMER_STATUS.NEW);
				break;
			case "active":
				customer.setStatus(CUSTOMER_STATUS.ACTIVE);
				break;
			case "inactive":

				customer.setStatus(CUSTOMER_STATUS.INACTIVE);
				break;
			case "cancelled":
				customer.setStatus(CUSTOMER_STATUS.CANCELLED);
				break;
			default:
				customer.setStatus(CUSTOMER_STATUS.NEW);
			}

			businessService.saveSubscriptionCustomer(customer);

			// send notification to public
			businessService.saveNotificationPublic(customer.getCustomer().getEmail(), ACCESS_TYPE.PUBLIC,
					"Your meal plan subscription status has been updated to " + customer.getStatus());

			// send email to verify review
			emailService.sendEmailNotification(new EmailNotificationObject(customer.getCustomer().getEmail(),
					customer.getCustomer().getFirstName(),
					"Your meal plan subscription from " + customer.getStore().getStoreName() + " has been updated to "
							+ customer.getStatus(),
					"Your meal plan subscription from " + customer.getStore().getStoreName() + " has been updated to "
							+ customer.getStatus() + ".<br><br>"
							+ "<blockquote> Access Your Meal Plans :<a href='http://www.meaily.com/landing/public#!/public/authpublic'>Manage Meal Plan Subscriptions</a> "
							+ "</blockquote>"));

			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while updating customer status");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			// reviewService.logError("ReportController", "customerStatus",
			// ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/deactivate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deactivate(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			BusinessUser businessUserFromDB = businessService
					.businessUserFindByUsername(RMUtil.getSessionedUser().getUsername());
			businessUserFromDB.setEnabled(false);
			businessService.saveBusinessUser(businessUserFromDB);
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth);
			}
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while deactivating profile");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			// reviewService.logError("ReportController", "deactivate", ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/deletePromotion", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deletePromotion(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			String storeId = params.get("store").toString();
			String promoId = params.get("promo").toString();
			Optional<Store> store = businessService.storeFindById(Long.valueOf(storeId));
			if (store.isPresent()) {
				businessService.promotionDeleteById(Long.valueOf(promoId));
			}
			responseData.put(RESULT, "true");

		} catch (Exception ex) {
			responseData.put(RESULT, "Error while removing promotion. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> report(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Calendar cal = Calendar.getInstance();
			DateTime startDate = new DateTime().minusDays(cal.get(Calendar.DAY_OF_MONTH)).minusMonths(1);
			responseData.putAll(businessService.getReport(startDate.toDate(), startDate.plusMonths(1).toDate()));
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while getting report");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/deplan", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deplan(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Long planId = Long.valueOf(params.get("planId").toString());
			String status = params.get("status").toString();
			StoreOfferredPlan storeOfferredPlan = businessService.storeOfferredPlanFindById(planId).get();
			storeOfferredPlan.setActive(status.equalsIgnoreCase("active"));
			businessService.saveStoreOfferredPlan(storeOfferredPlan);
			responseData.put(SUCCESS, storeOfferredPlan);
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while deactivating store offerred meal plan.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/destore", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> destore(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Long storeId = Long.valueOf(params.get("storeId").toString());
			String status = params.get("status").toString();
			Store store = businessService.storeFindById(storeId).get();
			store.setActive(status.equalsIgnoreCase("active"));
			businessService.saveStore(store);
			responseData.put(SUCCESS, store);
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while deactivating store");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@PostMapping("/endSubscription")
	public @ResponseBody Map<String, Object> endSubscriptioncharge(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> card) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			BusinessUser businessUser = businessService.businessUserFindById(RMUtil.getSessionedUser().getId()).get();
			businessUser.setTrialCompleted(true);
			businessUser.setMembership(MEMBERSHIP.FREE);
			businessUser.setAddDriver(false);
			businessUser.setAddJobs(false);
			businessService.saveBusinessUser(businessUser);
			responseData.put("status", businessService.cancelSubscription(businessUser));
			responseData.put(RESULT,
					"Your subscription is cancelled But you can still access the service untill end of your billing period.");
			responseData.put(SUCCESS, true);

		} catch (Exception ex) {
			responseData.put(ERROR, "Error while cancelling subscription");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			// reviewService.logError("ReportController", "endSubscription",
			// ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/getAccountDetail", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAccountDetail(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			BusinessUser user = businessService.businessUserFindById(RMUtil.getSessionedUser().getId()).get();
			responseData.put("data", user);
			responseData.put("invoice",
					!user.getPaymentId().equalsIgnoreCase("FREE") ? businessService.getInvoices(user) : "");
			responseData.put("subscription",
					!user.getPaymentId().equalsIgnoreCase("FREE")
							? businessService.getSubscriptionStatus(user).isEmpty()
							: true);
			responseData.put("subscriptionMessage",
					!user.getPaymentId().equalsIgnoreCase("FREE") ? businessService.getSubscriptionMessage(user) : "");
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(RESULT, "Error while getting account detail");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			// reviewService.logError("ReportController", "getAccountDetail",
			// ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/getActionItems", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getActionItems(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			List<ActionItem> actionItems = businessService
					.actionItemFindByUserIdOrderByAddedOnDesc(RMUtil.getSessionedUser().getId());
			actionItems.stream().forEach(action -> {
				action.getReview().setStore(null);
			});
			responseData.put("data", actionItems);
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@RequestMapping(value = "/getQueries", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAsk(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			// int startIndex = Integer.parseInt(params.get("start").toString());
			// int pageSize = Integer.parseInt(params.get("length").toString());
			String searchText = params != null && params.containsKey("searchText")
					? String.valueOf(params.get("searchText")).toLowerCase()
					: StringUtils.EMPTY;
			String filterStore = params != null && params.containsKey("filterStore")
					? String.valueOf(params.get("filterStore")).toLowerCase()
					: StringUtils.EMPTY;
			Optional<BusinessUser> user = businessService.businessUserFindById(RMUtil.getSessionedUser().getId());
			List<Query> queries = new ArrayList<>();
			if (user.isPresent()) {
				for (Store st : businessService.storeFindByBusinessUserId(RMUtil.getSessionedUser().getId())) {
					queries.addAll(businessService.queryFindByStoreIdOrderByQueriedOnDesc(st.getId()));
				}
			}
			int totalRecords = queries.size();
			responseData.put("recordsTotal", totalRecords);
			List<Query> filtered = queries.stream()
					.filter(query -> query.getQuery().toLowerCase().contains(searchText)
							&& query.getStore().getStoreName().toLowerCase().contains(filterStore))
					.collect(Collectors.toList());
			responseData.put("recordsFiltered", filtered.size());
			responseData.put("data", filtered);

		} catch (Exception ex) {
			responseData.put(RESULT, "Error while getting query. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/getDashboard", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getDashboardChartData(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Optional<BusinessUser> businessUser = businessService
					.businessUserFindById(RMUtil.getSessionedUser().getId());
			List<Store> stores = businessService.storeFindByBusinessUserId(businessUser.get().getId());
			List<Review> reviews = new ArrayList<Review>();
			List<StoreOfferredPlan> subPlans = new ArrayList<StoreOfferredPlan>();
			List<SubscriptionCustomer> customers = new ArrayList<SubscriptionCustomer>();
			List<PromotionCounter> promotionCounter = new ArrayList<PromotionCounter>();
			stores.stream().forEach(store -> {
				subPlans.addAll(businessService.storeFindByStoreIdAndActiveTrue(store.getId()));
				reviews.addAll(businessService.reviewFindByStoreIdOrderByReviedOnDesc(store.getId()));
				customers.addAll(businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(
						store.getId(), CUSTOMER_STATUS.ACTIVE));
				Optional<PromotionCounter> pCounter = businessService.promotionFindByStoreId(store.getId());
				if (pCounter.isPresent())
					promotionCounter.add(pCounter.get());
			});

			responseData.put("totalStores", stores.size());
			responseData.put("totalSubscriptionPlans", subPlans.size());
			responseData.put("totalCustomers", customers);
			DecimalFormat df = new DecimalFormat("####0.00");
			responseData.put("totalMonthlyRevenue", df.format(
					30 * customers.stream().mapToDouble(cust -> cust.getSubscriptions().stream().mapToDouble(sbr -> {
						if (sbr.getStoreOfferredPlan().getBillingPeriod().equalsIgnoreCase("monthly"))
							return sbr.getStoreOfferredPlan().getPlanPricing() / 30;
						else if (sbr.getStoreOfferredPlan().getBillingPeriod().equalsIgnoreCase("yearly"))
							return sbr.getStoreOfferredPlan().getPlanPricing() / 360;
						else
							return sbr.getStoreOfferredPlan().getPlanPricing() / 7;
					}).reduce(0, Double::sum)).reduce(0, Double::sum)));
			responseData.put("reviews", reviews);
			responseData.put("totalPromotionRequest", promotionCounter.stream().map(p -> p.getRequestCounter())
					.collect(Collectors.summingLong(Long::longValue)));
			responseData.put("positiveReviews", reviews.stream().filter(review -> review.getRating() > 3).count());
			responseData.put("negativeReviews", reviews.stream().filter(review -> review.getRating() <= 2).count());
			responseData.put("neutralReviews", reviews.stream().filter(review -> review.getRating() == 3).count());
			responseData.put("averageRating", reviews.stream().mapToDouble(review -> review.getRating()).average());
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(RESULT, "Error while retrieving dashboard data. Please try again.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
			// reviewService.logError("ReportController", "getDashboardChartData",
			// ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/getNotifications", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getNotifications(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			List<NotificationBusiness> notficiatoins = new ArrayList<NotificationBusiness>();

			businessService.storeFindByBusinessUserId(RMUtil.getSessionedUser().getId()).stream().forEach(st -> {
				notficiatoins.addAll(businessService.notificationFindByStoreIdOrderByTimeDesc(st.getId(), true));
			});
			businessService.jobPostringFindAllByBusinessUserId(RMUtil.getSessionedUser().getId()).stream()
					.forEach(jp -> {
						notficiatoins.addAll(businessService.notificationFindByJobIdOrderByTimeDesc(jp.getId(), true));
					});

			if (notficiatoins != null && !notficiatoins.isEmpty()) {
				List<NotificationBusiness> data = notficiatoins.stream()
						.sorted((n1, n2) -> n2.getTime().compareTo(n1.getTime())).collect(Collectors.toList());
				responseData.put("data", data.stream()
						.limit(params.containsKey("load") ? Integer.valueOf(params.get("load").toString()) : 20));
				responseData.put(SUCCESS, true);
			} else
				responseData.put(SUCCESS, false);
		} catch (Exception ex) {
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@RequestMapping(value = "/getKeys", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getKeys(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			responseData.put("mapbox", MAPBOX_SECRET_KEY);
			responseData.put("stripe", STRIPE_PUBLISHABLE_KEY);
			responseData.put("membership", RMUtil.getSessionedUser().getMembership().equals(MEMBERSHIP.BUSINESS));
		} catch (Exception ex) {
			responseData.put("mapbox", "");
			responseData.put("stripe", "");
			responseData.put("membership", false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/getReviews", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getReviews(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			int startIndex = Integer.parseInt(params.get("start").toString());
			int pageSize = Integer.parseInt(params.get("length").toString());
			String searchText = String.valueOf(params.get("searchText"));
			String filterStore = String.valueOf(params.get("filterStore"));
			String dir = String.valueOf(params.get("order[0][dir]"));
			Optional<BusinessUser> user = businessService.businessUserFindById(RMUtil.getSessionedUser().getId());
			final List<Review> reviews = new ArrayList(), dateFiltered = new ArrayList<Review>();
			if (user.isPresent()) {
				businessService.storeFindByBusinessUserId(RMUtil.getSessionedUser().getId()).stream().forEach(
						st -> reviews.addAll(businessService.reviewFindByStoreIdOrderByReviedOnDesc(st.getId())));
			}

			int totalRecords = reviews.size();
			responseData.put("recordsTotal", totalRecords);

			if (!filterStore.isEmpty())
				businessService.storeFindByBusinessUserId(RMUtil.getSessionedUser().getId()).stream()
						.filter(st -> st.getStoreName().toLowerCase().contains(filterStore.toLowerCase()))
						.forEach(st -> dateFiltered
								.addAll(businessService.reviewFindByStoreIdOrderByReviedOnDesc(st.getId())));
			else
				dateFiltered.addAll(reviews);

			if (!searchText.isEmpty()) {
				List<Review> filteredReviews = new ArrayList<Review>();
				filteredReviews.addAll(dateFiltered);
				dateFiltered.clear();
				dateFiltered.addAll(filteredReviews.parallelStream()
						.filter(re -> re.getReview().toLowerCase().contains(searchText)).collect(Collectors.toList()));

			}
			int filteredRecords = dateFiltered.size();
			int start = startIndex >= filteredRecords ? filteredRecords : startIndex;
			int end = (start + pageSize) >= filteredRecords ? filteredRecords : (start + pageSize);

			responseData.put("recordsFiltered", filteredRecords);
			responseData.put("data", dateFiltered.subList(start, end));
		} catch (Exception ex) {
			responseData.put(RESULT, "Error while retrieving reviews");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			responseData.put("recordsTotal", 0);
			// reviewService.logError("ReportController", "getReviews", ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView indexPage(HttpServletRequest request) {
		return new ModelAndView("index");
	}

	@RequestMapping(value = "/listCustomers", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> listCustomers(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			List<SubscriptionCustomer> customers = new ArrayList<SubscriptionCustomer>(),
					dateFiltered = new ArrayList<SubscriptionCustomer>();
			String customerStatus = params.containsKey("customerstatus") ? params.get("customerstatus").toString()
					: "new";
			if (!params.containsKey("storeId")) {

				businessService.storeFindByBusinessUserId(RMUtil.getSessionedUser().getId()).stream().forEach(store -> {

					switch (customerStatus) {
					case "new":
						customers.addAll(
								businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(
										store.getId(), CUSTOMER_STATUS.NEW));
						break;
					case "active":
						customers.addAll(
								businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(
										store.getId(), CUSTOMER_STATUS.ACTIVE));
						break;
					case "inactive":
						customers.addAll(
								businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(
										store.getId(), CUSTOMER_STATUS.INACTIVE));
						break;
					case "cancelled":
						customers.addAll(
								businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(
										store.getId(), CUSTOMER_STATUS.CANCELLED));
						break;
					default:
						customers.addAll(
								businessService.subscriptionCustomerFindAllByStoreIdOrderByCreatedOnAsc(store.getId()));
					}

				});
			} else {
				Long storeId = Long.valueOf(params.get("storeId").toString());
				customers.addAll(customerStatus.equalsIgnoreCase("active")
						? businessService.subscriptionCustomerFindAllByStoreIdAndStatusOrderByCreatedOnAsc(storeId,
								CUSTOMER_STATUS.ACTIVE)
						: businessService.subscriptionCustomerFindAllByStoreIdOrderByCreatedOnAsc(storeId));
				customers.stream().forEach(cust -> cust.setStore(null));
			}

			int startIndex = Integer.parseInt(params.get("start").toString());
			int pageSize = Integer.parseInt(params.get("length").toString());
			String searchText = String.valueOf(params.get("search[value]"));

			responseData.put("recordsTotal", customers.size());

			// check if its allowed;
			if (customers.size() >= FREE_CUSTOMER_MEAL_PLAN_CUSTOMER_LIMIT) {
				Optional<BusinessUser> businessUser = businessService
						.businessUserFindById(RMUtil.getSessionedUser().getId());
				if (businessUser.get().getMembership().equals(MEMBERSHIP.FREE)) {
					BusinessUser user = businessUser.get();
					user.setAddCustomer(false);
					businessService.saveBusinessUser(user);
				}
			}

			if (!StringUtils.isEmpty(searchText)) {
				dateFiltered = customers.stream()
						.filter(customer -> customer.toString().toLowerCase().contains(searchText.toLowerCase()))
						.collect(Collectors.toList());
			} else {
				dateFiltered = customers;
			}

			int filteredRecords = dateFiltered.size();
			int start = startIndex >= filteredRecords ? filteredRecords : startIndex;
			int end = (start + pageSize) >= filteredRecords ? filteredRecords : (start + pageSize);

			responseData.put("recordsFiltered", filteredRecords);
			responseData.put("data", dateFiltered.subList(start, end));
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while adding store");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			// reviewService.logError("ReportController", "addStore", ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/listStore", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> listStore(HttpServletRequest request) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			Optional<BusinessUser> businessUser = businessService
					.businessUserFindById(RMUtil.getSessionedUser().getId());
			List<Store> stores = businessService.storeFindByBusinessUserId(businessUser.get().getId());
			stores.stream().forEach(store -> store.getServiceArea());
			responseData.put("data", stores);
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while adding store");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			// reviewService.logError("ReportController", "addStore", ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/listSubscriptionPlans", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> listSubscriptionPlans(HttpServletRequest request) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			// List<StoreOfferredPlan> plans = new ArrayList<>();
			// businessUser.getStores().stream().map(store ->
			// plans.addAll(store.getStoreOfferredPlans())).collect(Collectors.toList());
			responseData.put("data", businessService.storeFindByBusinessUserId(RMUtil.getSessionedUser().getId()));
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while getting subscription plan");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			// reviewService.logError("ReportController", "addStore", ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/loadBusiness", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> loadBusiness(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			// responseData.put("result",
			// reviewService.getBusinessCustomers(RMUtil.getSessionedUser().getClientId()));
		} catch (Exception ex) {
			responseData.put(RESULT, "Error while loading business. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/loadPromotions", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> loadPromotions(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			List<Promotion> promotions = new ArrayList<Promotion>();
			List<PromotionCounter> promotionCounters = new ArrayList<PromotionCounter>();
			if (params != null && params.containsKey("storeId")) {
				Long storeId = Long.valueOf(params.get("storeId").toString());
				promotions.addAll(
						businessService.promotionFindByStoreIdOrderByCreatedOnDesc(storeId).stream().filter(promote -> {
							promote.setStore(null);
							return true;
						}).collect(Collectors.toList()));

				Optional<PromotionCounter> pCont = businessService.promotionCounterFindByStoreId(storeId);
				if (pCont.isPresent()) {
					pCont.get().setStoreName("");
					promotionCounters.add(pCont.get());
				}
			} else {
				BusinessUser businessUser = businessService
						.businessUserFindByUsername(RMUtil.getSessionedUser().getUsername());
				businessService.storeFindByBusinessUserId(businessUser.getId()).stream().forEach(store -> {
					promotions.addAll(businessService.promotionFindByStoreIdOrderByCreatedOnDesc(store.getId()));
					Optional<PromotionCounter> pCont = businessService.promotionCounterFindByStoreId(store.getId());
					if (pCont.isPresent()) {
						pCont.get().setStoreName(store.getStoreName());
						promotionCounters.add(pCont.get());
					}
				});
			}

			responseData.put("promotionCounters", promotionCounters);
			responseData.put(RESULT, promotions);
		} catch (Exception ex) {
			responseData.put(RESULT, "Error while loading promotion. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/markNotification", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> markNotification(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Long notificationId = Long.valueOf(params.get("notificationID").toString());
			NotificationBusiness notify = businessService.notificationFindById(notificationId).get();
			notify.setRead(true);
			businessService.saveNotificationBusiness(notify);
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@GetMapping(value = { "", "/" })
	public ModelAndView publics(HttpServletRequest request) {
		return new ModelAndView("redirect:/business/index");
	}

	@RequestMapping(value = "/removeActionItem", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> removeActionItem(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Long reviewId = Long.valueOf(params.get("reviewId").toString());
			businessService.actionItemDeleteById(reviewId);
			responseData.put("data", "Removed action item.");
		} catch (Exception ex) {
			responseData.put(ERROR, "Error while removing action items");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
			// reviewService.logError("ReportController", "removeActionItem",
			// ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/replyAsk", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> replyAsk(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			Long id = Long.parseLong(params.get("queryId").toString());
			String answer = params.get("answer").toString();

			Optional<Query> query = businessService.queryFindById(id);
			if (query.isPresent()) {
				Query q = query.get();

				Reply reply = new Reply();
				reply.setRepliedBy(RMUtil.getSessionedUser());
				reply.setRepliedOn(new Timestamp(System.currentTimeMillis()));
				reply.setReply(answer);
				q.setReply(businessService.saveReply(reply));
				businessService.saveQuery(q);

				// send notification to public
				businessService.saveNotificationPublic(q.getReviewer().getEmail(), ACCESS_TYPE.PUBLIC,
						"You have received reply for query about " + q.getStore().getStoreName());

				// send email to verify review
				emailService.sendEmailNotification(
						new EmailNotificationObject(q.getReviewer().getEmail(), q.getReviewer().getFirstName(),
								"Your query has been answered by " + q.getStore().getStoreName(),
								"Your query has been answered by " + q.getStore().getStoreName() + ".<br><br>"
										+ q.getQuery() + "<blockquote>" + q.getReply().getReply() + "</blockquote>"));

				responseData.put(RESULT, "Saved");
			}

		} catch (Exception ex) {
			responseData.put(RESULT, "Error while getting query. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/replyReview", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> replyReview(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Optional<Review> publicReview = businessService
					.reviewFindById(Long.parseLong(params.get("replyTo").toString()));
			if (publicReview.isPresent()) {
				Review review = publicReview.get();
				Reply reply = new Reply();
				reply.setReply(String.valueOf(params.get("reviewText")));
				reply.setRepliedBy(RMUtil.getSessionedUser());
				reply.setRepliedOn(new Timestamp(System.currentTimeMillis()));
				review.setReply(businessService.saveReply(reply));
				businessService.saveReview(review);

				// send notification to public
				businessService.saveNotificationPublic(review.getReviewer().getEmail(), ACCESS_TYPE.PUBLIC,
						"You have received reply for review about " + review.getStore().getStoreName());

				// send email to verify review
				emailService.sendEmailNotification(new EmailNotificationObject(review.getReviewer().getEmail(),
						review.getReviewer().getFirstName(),
						"Your have received feedback for your review about " + review.getStore().getStoreName(),
						"You have received feedback for your review about " + review.getStore().getStoreName()
								+ ".<br><br>" + review.getReview() + "<blockquote>" + review.getReply().getReply()
								+ "</blockquote>"));

				responseData.put(RESULT, "Replied");
			} else {
				responseData.put(RESULT, "Not found");
			}

		} catch (Exception ex) {
			responseData.put(RESULT, "Error while posting reply. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/addReview", method = RequestMethod.GET)
	public ModelAndView reportDriverPage(HttpServletRequest request) {
		return new ModelAndView("reportincident");
	}

	@RequestMapping(value = "/resetPromotionCounter", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> resetPromotionCounter(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			String storeId = params.get("storeId").toString();
			PromotionCounter promotionCounter = null;
			Optional<PromotionCounter> pCounter = businessService.promotionCounterFindByStoreId(Long.valueOf(storeId));
			if (pCounter.isPresent()) {
				promotionCounter = pCounter.get();
				promotionCounter.resetRequestCounter();
			}

			businessService.savePromotionCounter(promotionCounter);
			responseData.put(RESULT, promotionCounter);

		} catch (Exception ex) {
			responseData.put(RESULT, "Error while loading promotion. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@PostMapping("/resumeSubscription")
	public @ResponseBody Map<String, Object> resumeSubscriptioncharge(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> card) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			BusinessUser businessUser = businessService.businessUserFindById(RMUtil.getSessionedUser().getId()).get();
			businessUser.setTrialCompleted(true);
			businessUser.setMembership(MEMBERSHIP.BUSINESS);
			businessUser.setAddDriver(true);
			businessUser.setAddJobs(true);
			businessService.saveBusinessUser(businessUser);
			responseData.put("status", businessService.resumeSubscription(businessUser));
			responseData.put(RESULT, "Your subscription has been resumed.");
			responseData.put(SUCCESS, true);

		} catch (Exception ex) {
			responseData.put(ERROR, "Error while cancelling subscription");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	/*
	 * @RequestMapping(value = "/startEnterpriseMembership", method =
	 * RequestMethod.GET) public @ResponseBody Map<String, Object>
	 * startEnterPriseMembership(HttpServletRequest request,
	 * 
	 * @RequestParam(required = false) Map<String, Object> params) { Map<String,
	 * Object> responseData = new HashMap<>(); try { RMUtil.PLANS plan =
	 * RMUtil.PLANS.ENTERPRISE; BusinessUser businessUser =
	 * businessService.businessUserFindById(RMUtil.getSessionedUser().getId()).get()
	 * ;
	 * 
	 * // create stripe user
	 * if(businessUser.getPaymentId().equalsIgnoreCase("FREE")) { Customer
	 * stripeCustomer = paymentService.createStripeUser(businessUser);
	 * businessUser.setPaymentId(stripeCustomer.getId());
	 * businessService.saveBusinessUser(businessUser); }
	 * 
	 * responseData.put("payment",
	 * businessService.getStripeSession(businessUser.isTrialCompleted(),plan).getId(
	 * )); responseData.put("success", true); } catch (Exception ex) {
	 * responseData.put("result", "Error while starting membership");
	 * responseData.put("errorMessage", ex.getMessage());
	 * responseData.put("success", false); businessService.saveErrors(new
	 * Errors(ex.getMessage(), "")); //reviewService.logError("ReportController",
	 * "startMembership", ex.getMessage()); } return responseData; }
	 */

	@RequestMapping(value = "/startMembership", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> startMembership(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			RMUtil.PLANS plan = RMUtil.PLANS.SMALL_BUSINESS;
			BusinessUser businessUser = businessService.businessUserFindById(RMUtil.getSessionedUser().getId()).get();

			// create stripe user
			if (businessUser.getMembership().equals(MEMBERSHIP.FREE)) {
				Customer stripeCustomer = paymentService.createStripeUser(businessUser);
				businessUser.setPaymentId(stripeCustomer.getId());
				businessUser.setMembership(MEMBERSHIP.BUSINESS);
				businessUser.setAddDriver(true);
				businessUser.setAddJobs(true);
				businessUser.setAddCustomer(true);
				businessUser.setAddMealPlan(true);
				businessUser.setAddStore(true);
				businessService.saveBusinessUser(businessUser);
				responseData.put("payment",businessService.getStripeSession(businessUser.getPaymentId(),businessUser.isTrialCompleted(), plan,businessUser.getEmail()).getId());
				responseData.put(SUCCESS, true);
			}else {
				responseData.put(RESULT, "Your membership seems already active.");
				responseData.put(SUCCESS, false);
			}

			
		} catch (Exception ex) {
			responseData.put(RESULT, "Error while starting membership");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
			// reviewService.logError("ReportController", "startMembership",
			// ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/createsubscriptionplan", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> subscribe(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		Map<String, Object> uploadResult = new HashMap<>();
		try {
			Optional<BusinessUser> businessUser = businessService
					.businessUserFindById(RMUtil.getSessionedUser().getId());

			if (!RMUtil.isAllowed(ACCESS_RIGHT.ADD_SUBSCRIBE_MEAL, businessUser.get())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}
			Long storeId = Long.valueOf(params.get("storeId").toString());
			Long planId = params.get("planId").toString().length() > 0 ? Long.valueOf(params.get("planId").toString())
					: null;
			String description = String.valueOf(params.get("desc"));
			String name = String.valueOf(params.get("name"));
			;
			Double planPricing = Double.valueOf(params.get("planPricing").toString());
			String planPeriod = String.valueOf(params.get("planPeriod"));
			uploadResult.put("secure_url", String.valueOf(params.get("planImage")));

			Boolean homeDelivered = Boolean.valueOf(params.get("homeDelivered").toString()),
					takeOut = Boolean.valueOf(params.get("takeOut").toString()),
					monday = Boolean.valueOf(params.get("monday").toString()),
					tuesday = Boolean.valueOf(params.get("tuesday").toString()),
					wednesday = Boolean.valueOf(params.get("wednesday").toString()),
					thursday = Boolean.valueOf(params.get("thursday").toString()),
					friday = Boolean.valueOf(params.get("friday").toString()),
					saturday = Boolean.valueOf(params.get("saturday").toString()),
					sunday = Boolean.valueOf(params.get("sunday").toString()),
					foodSample = Boolean.valueOf(params.get("foodSample").toString());

			Optional<Store> storeResult = businessService.storeFindById(storeId);
			if (storeResult.isPresent()) {
				Store store = storeResult.get();
				List<StoreOfferredPlan> plans = businessService.storeOfferredPlanFindByStoreId(store.getId());
				// check if its allowed;
				if (plans.size() >= FREE_CUSTOMER_MEAL_PLANS_LIMIT) {
					if (businessUser.get().getMembership().equals(MEMBERSHIP.FREE)) {
						BusinessUser user = businessUser.get();
						user.setAddMealPlan(false);
						businessService.saveBusinessUser(user);
						RMUtil.getSessionedUser().setAddMealPlan(false);
						throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
					}
				}

				if (plans.size() >= SUBSCRIBED_CUSTOMER_MEAL_PLANS_LIMIT) {
					if (businessUser.get().getMembership().equals(MEMBERSHIP.BUSINESS)) {
						BusinessUser user = businessUser.get();
						user.setAddMealPlan(false);
						businessService.saveBusinessUser(user);
						RMUtil.getSessionedUser().setAddMealPlan(false);
						throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);

					}
				}

				MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;

				if (multipartHttpServletRequest.getFile("planImage") != null) {
					Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap("cloud_name", CLOUDINARY_NAME, "api_key",
							CLOUDINARY_KEY, "api_secret", CLOUDINARY_SECRET));
					Map<String, Object> map = ObjectUtils.asMap("folder", "meal_plans/" + storeId + "/");
					map.putIfAbsent("moderation", "aws_rek");
					uploadResult = cloudinary.uploader()
							.upload(multipartHttpServletRequest.getFile("planImage").getBytes(), map);
					if (uploadResult.toString().toLowerCase().contains("status=rejected")) {
						throw new Exception("Error while uploading image. Please review the image or try after some time.");
					}

				}

				StoreOfferredPlan storeOfferredPlan = new StoreOfferredPlan();
				storeOfferredPlan.setId(planId);
				storeOfferredPlan.setActive(true);
				storeOfferredPlan.setImageUrl(uploadResult.get("secure_url").toString());
				storeOfferredPlan.setDescription(description);
				storeOfferredPlan.setName(name);
				storeOfferredPlan.setPlanPricing(planPricing);
				storeOfferredPlan.setBillingPeriod(planPeriod);
				storeOfferredPlan.setAvailableOnFriday(friday);
				storeOfferredPlan.setAvailableOnMonday(monday);
				storeOfferredPlan.setAvailableOnSaturday(saturday);
				storeOfferredPlan.setAvailableOnSunday(sunday);
				storeOfferredPlan.setAvailableOnThursday(thursday);
				storeOfferredPlan.setAvailableOnTuesday(tuesday);
				storeOfferredPlan.setAvailableOnWednesday(wednesday);
				storeOfferredPlan.setHomeDelivered(homeDelivered);
				storeOfferredPlan.setTakeOut(takeOut);
				storeOfferredPlan.setFoodSample(foodSample);
				storeOfferredPlan.setStore(store);
				storeOfferredPlan.setModifiedOn(new Timestamp(System.currentTimeMillis()));
				businessService.saveStoreOfferredPlan(storeOfferredPlan);

				if (planId == null) {
					responseData.put(MESSAGE, "Store Offerred Plan Created successfully.");
				} else {
					responseData.put(MESSAGE, "Store Offerred Plan Updated successfully.");
				}

				responseData.put(SUCCESS, true);
			} else {
				responseData.put(MESSAGE, "store not found");
				responseData.put(SUCCESS, false);
			}

		} catch (Exception ex) {
			responseData.put(MESSAGE, ex.getMessage());
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/subscribes", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> subscribes(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			Long storeId = Long.valueOf(params.get("clientId").toString());
			List<EmailSubscription> customers = businessService
					.emailSubscriptionFindByStoreIdOrderByCreatedOnDesc(Long.valueOf(storeId)),
					dateFiltered = new ArrayList<EmailSubscription>();
			if (customers != null) {
				customers.stream().forEach(email -> email.setStore(null));
			} else {
				customers = new ArrayList<EmailSubscription>();
			}

			int startIndex = Integer.parseInt(params.get("start").toString());
			int pageSize = Integer.parseInt(params.get("length").toString());
			String searchText = String.valueOf(params.get("search[value]"));

			responseData.put("recordsTotal", customers.size());
			if (!StringUtils.isEmpty(searchText)) {
				dateFiltered = customers.stream()
						.filter(customer -> customer.toString().toLowerCase().contains(searchText.toLowerCase()))
						.collect(Collectors.toList());
			} else {
				dateFiltered = customers;
			}

			int filteredRecords = dateFiltered.size();
			int start = startIndex >= filteredRecords ? filteredRecords : startIndex;
			int end = (start + pageSize) >= filteredRecords ? filteredRecords : (start + pageSize);

			responseData.put("recordsFiltered", filteredRecords);
			responseData.put("data", dateFiltered.subList(start, end));
			responseData.put(SUCCESS, true);

		} catch (Exception ex) {
			responseData.put(RESULT, "Error. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateProfile(HttpServletRequest request,
			@RequestBody UserTemp businessUser) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			BusinessUser businessUserFromDB = businessService
					.businessUserFindByUsername(RMUtil.getSessionedUser().getUsername());
			if (businessUser.getAddress() != null) {
				
				ServiceArea area  = locationService.addLocations(businessUser.getAddress().getArea());
				businessUser.getAddress().setArea(area);
				
				Long id = businessUserFromDB.getAddress().getId();
				businessUserFromDB.setAddress(businessUser.getAddress());
				businessUserFromDB.getAddress()
						.setArea(locationService.addLocations(businessUserFromDB.getAddress().getArea()));
				businessUserFromDB.getAddress().setId(id);
			}
			if (businessUser.getPhoneNumber() != null)
				businessUserFromDB.setClientBusinessPhoneNumber(businessUser.getPhoneNumber());
			if (businessUser.getBusinessUrl() != null)
				businessUserFromDB.setClientBusinessUrl(businessUser.getBusinessUrl());
			if (businessUser.getName() != null)
				businessUserFromDB.setClientName(businessUser.getName());
			if (businessUser.getEmail() != null)
				businessUserFromDB.setClientEmail(businessUser.getEmail());

			businessUserFromDB.setNotifyAddedAsCompetitor(businessUser.isNotifyAddedAsCompetitor());
			businessUserFromDB.setNotifyNewReport(businessUser.isNotifyNewReport());
			businessUserFromDB.setNotifyNewReview(businessUser.isNotifyNewReview());
			businessService.saveBusinessUser(businessUserFromDB);
			responseData.put("data", businessUserFromDB);
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseData.put(ERROR, "Error while updating profile");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
			// reviewService.logError("ReportController", "updateProfile", ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/job", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> jobSubmit(HttpServletRequest request, @RequestBody JobPosting jobPosting) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			if (!RMUtil.isAllowed(ACCESS_RIGHT.GET_JOBS, RMUtil.getSessionedUser())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}

			jobPosting.setBusinessUser(RMUtil.getSessionedUser());
			if (jobPosting.getId() != null) {
				jobPosting.setModifiedOn(new Timestamp(System.currentTimeMillis()));
			}
			businessService.saveJobPosting(jobPosting);
			responseData.put(MESSAGE, "Job created/updated successfully.");
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseData.put(MESSAGE, "Error while creating job posting. Please try sometimes later.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/removejob", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> removejob(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			Optional<BusinessUser> businessUser = businessService
					.businessUserFindById(RMUtil.getSessionedUser().getId());

			if (!RMUtil.isAllowed(ACCESS_RIGHT.GET_JOBS, businessUser.get())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}

			businessService.deleteJobPosting(Long.valueOf(params.get("id").toString()));
			responseData.put(MESSAGE, "Job deleted successfully.");
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseData.put(MESSAGE,
					ex.getMessage().contains("org.hibernate.exception.ConstraintViolationException")
							? "You have active job applications referring to job posting. Job can not be deleted."
							: "Error while deleting job. Please try again");
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@RequestMapping(value = "/jobs", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> jobSubmit(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			if (!RMUtil.isAllowed(ACCESS_RIGHT.GET_JOBS, RMUtil.getSessionedUser())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}

			Boolean showAll = Boolean.valueOf(params.get("showAll").toString());
			List<DeliveryJobApplication> applications = new ArrayList<DeliveryJobApplication>();
			List<JobPosting> jobs = businessService
					.jobPostringFindAllByBusinessUserId(RMUtil.getSessionedUser().getId());

			for (JobPosting job : jobs)
				applications.addAll(showAll ? businessService.getDeliveryJobApplication(job)
						: businessService.getDeliveryJobApplication(job, JOBSTATUS.APPLIED));

			responseData.put("data", jobs);
			responseData.put("applications", applications);
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseData.put(ERROR, "Error while getting job posting");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/yourdrivers", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> yourdrivers(HttpServletRequest request) {
		Map<String, Object> responseData = new HashMap<>();
		try {
			if (!RMUtil.isAllowed(ACCESS_RIGHT.GET_DRIVERS, RMUtil.getSessionedUser())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}
			responseData.put("data", businessService.getYourDrivers(RMUtil.getSessionedUser().getId()));
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseData.put(ERROR, "Error while getting job posting");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/removeapplication", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> removeapplication(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			if (!RMUtil.isAllowed(ACCESS_RIGHT.GET_JOBS, RMUtil.getSessionedUser())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}
			Long id = Long.valueOf(params.get("id").toString());
			businessService.deleteJobApplication(id);

			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseData.put(MESSAGE, "Error while creating job posting. Please try sometimes later.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/rejectapplication", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> rejectapplication(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			if (!RMUtil.isAllowed(ACCESS_RIGHT.GET_JOBS, RMUtil.getSessionedUser())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}

			Long id = Long.valueOf(params.get("id").toString());
			DeliveryJobApplication jobApp = businessService.getDeliveryJobApplication(id);
			jobApp.setStatus(JOBSTATUS.REJECTED);
			jobApp.setBusinessUser(null);
			businessService.saveDeliveryJobApplication(jobApp);

			// send notification to public
			businessService.saveNotificationPublic(jobApp.getDeliveryDriver().getReviewer().getEmail(),
					ACCESS_TYPE.DRIVER, "Your Job Application status has been updated to " + jobApp.getStatus());

			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseData.put(MESSAGE, "Error while creating job posting. Please try sometimes later.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/approveapplication", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> approveapplication(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			if (!RMUtil.isAllowed(ACCESS_RIGHT.GET_JOBS, RMUtil.getSessionedUser())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}

			Long id = Long.valueOf(params.get("id").toString());
			DeliveryJobApplication jobApp = businessService.getDeliveryJobApplication(id);
			jobApp.setStatus(JOBSTATUS.SELECTED);
			jobApp.setBusinessUser(RMUtil.getSessionedUser());
			businessService.saveDeliveryJobApplication(jobApp);

			// send notification to public
			businessService.saveNotificationPublic(jobApp.getDeliveryDriver().getReviewer().getEmail(),
					ACCESS_TYPE.DRIVER, "Congratulations ! You have been selected for food delivery job."
							+ jobApp.getJobPosting().getName());

			// send email to verify review
			emailService.sendEmailNotification(
					new EmailNotificationObject(jobApp.getDeliveryDriver().getReviewer().getEmail(),
							jobApp.getDeliveryDriver().getReviewer().getFirstName(),
							"You have received update on your application for food delivery job.",
							"Congratulation ! Your application for food delivery job has been updated to "
									+ jobApp.getStatus() + "."));

			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseData.put(MESSAGE, "Error while approving job posting. Please try sometimes later.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/assignjob", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> applyjob(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			if (!RMUtil.isAllowed(ACCESS_RIGHT.GET_JOBS, RMUtil.getSessionedUser())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}

			Long jobId = Long.valueOf(params.get("jobId").toString());
			Long driverId = Long.valueOf(params.get("driverId").toString());

			businessService.assignDeliveryJobApplication(jobId, driverId);
			responseData.put(MESSAGE, "Candidate has been selected successfully for the job.");
			responseData.put(SUCCESS, true);

		} catch (Exception ex) {
			responseData.put(SUCCESS, false);
			responseData.put(RESULT, "Error while applying job. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/connect", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> connect(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			String email = params.get("email").toString();
			String message = params.get(MESSAGE).toString();
			if (message != null && !message.isEmpty())
				emailService.sendEmailNotification(new EmailNotificationObject(email, "User", "Thanks you", message));

			responseData.put(MESSAGE, "Email Sent.");
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			responseData.put(SUCCESS, false);
			responseData.put(MESSAGE, "Error while connecting customer.");
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

	@RequestMapping(value = "/pool", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> pool(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<>();
		try {

			if (!RMUtil.isAllowed(ACCESS_RIGHT.GET_DRIVERS, RMUtil.getSessionedUser())) {
				throw new Exception(LIMIT_REACHED_FOR_FREE_MEMBERSHIP_PLAN);
			}

			int startIndex = Integer.parseInt(params.get("start").toString());
			int pageSize = Integer.parseInt(params.get("length").toString());
			String searchText = String.valueOf(params.get("search"));
			String location = String.valueOf(params.get("location"));
			// String dir = String.valueOf(params.get("order[0][dir]"));
			List<DeliveryDriver> jobs = businessService.getDeliveryDrivers();

			List<DeliveryDriver> filtered = new ArrayList<DeliveryDriver>();
			responseData.put("recordsTotal", jobs.size());
			if (!location.isEmpty()) {
				jobs = jobs.stream()
						.filter(jp -> StringUtils.containsIgnoreCase(jp.getDeliveryAreas().stream()
								.map(sa -> sa.getCityName()).collect(Collectors.joining()), location))
						.collect(Collectors.toList());
				filtered.addAll(jobs);
			}

			if (!searchText.isEmpty()) {
				filtered.addAll(jobs.stream().filter(jp -> StringUtils.containsIgnoreCase(jp.toString(), searchText))
						.collect(Collectors.toList()));
			}

			if (location.isEmpty() && searchText.isEmpty()) {
				filtered.clear();
				filtered.addAll(jobs);
			}

			int filteredRecords = filtered.size();
			int start = startIndex >= filteredRecords ? filteredRecords : startIndex;
			int end = (start + pageSize) >= filteredRecords ? filteredRecords : (start + pageSize);

			responseData.put("recordsFiltered", filtered.size());
			responseData.put(MESSAGE, "Showing " + end + " of " + filtered.size() + " jobs");
			responseData.put("data", filtered.stream().skip(start).limit(end).collect(Collectors.toList()));
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseData.put(ERROR, "Error while getting job posting");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			businessService.saveErrors(new Errors(ex));
		}
		return responseData;
	}

}
