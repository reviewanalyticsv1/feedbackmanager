package org.feedback.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;

@ControllerAdvice
@RequestMapping(value = "/error")
public class ExceptionController implements ErrorController {

	@RequestMapping
	public String error(HttpServletRequest request, HttpServletResponse response) {
		return "redirect:/landing/public#!/public/something_went_wrong";
	}

	@Override
	public String getErrorPath() {
		return "null";
	}

}
