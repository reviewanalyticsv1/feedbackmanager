package org.feedback.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.FuzzyScore;
import org.feedback.pojo.Address;
import org.feedback.pojo.DeliveryDriver;
import org.feedback.pojo.DeliveryJobApplication;
import org.feedback.pojo.EmailNotificationObject;
import org.feedback.pojo.EmailSubscription;
import org.feedback.pojo.Errors;
import org.feedback.pojo.JobPosting;
import org.feedback.pojo.NotificationBusiness;
import org.feedback.pojo.Promotion;
import org.feedback.pojo.PromotionCounter;
import org.feedback.pojo.Query;
import org.feedback.pojo.Review;
import org.feedback.pojo.Reviewer;
import org.feedback.pojo.ServiceArea;
import org.feedback.pojo.Store;
import org.feedback.pojo.StoreOfferredPlan;
import org.feedback.pojo.SubscriptionCustomer;
import org.feedback.pojo.SubscriptionPlan;
import org.feedback.service.BusinessService;
import org.feedback.service.EmailService;
import org.feedback.service.LocationService;
import org.feedback.service.PopularService;
import org.feedback.service.PublicUserService;
import org.feedback.utility.RMUtil;
import org.feedback.utility.RMUtil.CUSTOMER_STATUS;
import org.feedback.utility.RMUtil.JOBSTATUS;
import org.feedback.utility.RMUtil.NOTIFICATIONS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

@Controller
@RequestMapping("/landing")
public class LandingController {

	private static final String RESULT = "result";

	private static final String ERROR_MESSAGE = "errorMessage";

	private static final String SUCCESS = "success";

	@Value("${meaily.mapbox.secret}")
	private String MAPBOX_SECRET_KEY;
	@Value("${meaily.cloudinary.key}")
	private String CLOUDINARY_KEY = "665184582956882";
	@Value("${meaily.cloudinary.secret}")
	private String CLOUDINARY_SECRET = "Khe80I9ljeoacl5q7HsO6YHgzyk";
	@Value("${meaily.cloudinary.cloudname}")
	private String CLOUDINARY_NAME = "mealforest-com";

	@Autowired
	BusinessService businessService;

	@Autowired(required = true)
	public LocationService locationService;

	@Autowired(required = true)
	PublicUserService publicUserService;

	@Autowired(required = true)
	PopularService popularService;

	@Autowired(required = true)
	EmailService emailService;

	public LandingController() {
		super();
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@RequestMapping(value = "/cancelmealplan", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> cancelmealplan(HttpServletRequest request,
			@RequestBody(required = true) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Long id = Long.valueOf(params.get("id").toString());
			String comment = params.get("comment").toString();
			SubscriptionCustomer customer = businessService.subscriptionCustomerFindById(id).get();
			customer.setStatus(CUSTOMER_STATUS.CANCELLED);
			customer.setCustomerComment(comment);
			businessService.saveSubscriptionCustomer(customer);

			// add recent review
			NotificationBusiness notify = new NotificationBusiness(RMUtil.NOTIFICATIONS.MEALPLAN,
					customer.getCustomer().getEmail() + " has cancelled mealplan.");
			notify.setStoreId(customer.getStore().getId());
			businessService.saveNotificationBusiness(notify);

			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	private int checkStoreOfferredPlans(Store store, String query) {
		FuzzyScore score = new FuzzyScore(Locale.ENGLISH);
		StringBuffer sbuffer = new StringBuffer();
		businessService.storeOfferredPlanFindByStoreId(store.getId()).stream()
				.forEach(splan -> sbuffer.append(splan.toString()));
		return score.fuzzyScore(sbuffer.toString(), query);

	}

	@GetMapping(value = "/enablelocation")
	public @ResponseBody String enableServiceLocation(HttpServletRequest request,
			@RequestParam(name = "city") String city, @RequestParam(name = "state") String state,
			@RequestParam(name = "stateCode") String stateCode, @RequestParam(name = "country") String country,
			@RequestParam(name = "countryCode") String countryCode) {
		locationService.addLocations(new ServiceArea(country, state, city));
		return "added";
	}

	@RequestMapping(value = "/extendmealplan", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> extendmealplan(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Long id = Long.valueOf(params.get("id").toString());
			String endDate = params.get("endDate").toString();
			SubscriptionPlan plan = businessService.subscriptionPlanFindById(id).get();
			plan.setEndDate(RMUtil.getDate(endDate));
			businessService.saveSubscriptionPlan(plan);

			// add recent review
			NotificationBusiness notify = new NotificationBusiness(RMUtil.NOTIFICATIONS.MEALPLAN,
					plan.getStoreOfferredPlan().getName() + " subscription has been extended.");
			notify.setStoreId(plan.getStoreOfferredPlan().getStore().getId());
			businessService.saveNotificationBusiness(notify);
			responseData.put(RESULT, "extended");
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/getClient", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getClientInformation(HttpServletRequest request,
			HttpServletResponse response, @RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Store store = businessService.storeFindById(Long.valueOf(params.get("clientId").toString())).get();
			responseData.put("data", store);
			responseData.put("storeOfferredPlans",
					params.containsKey("planStatus") ? businessService.storeOfferredPlanFindByStoreId(store.getId())
							: businessService.storeOfferredPlanFindByStoreIdAndActiveTrue(store.getId()));
			List<Review> reviews = businessService.reviewFindByStoreIdOrderByReviedOnDesc(store.getId());
			responseData.put("averageRating", reviews.stream().mapToDouble(review -> review.getRating()).average());

			if (RMUtil.getSessionedUser() != null)
				responseData.put("locations",locationService.getLocations(store.getAddress().getArea()));

		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Error while retrieving client detail.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@RequestMapping(value = "/getReviews", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getReviews(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {

			int startIndex = Integer.parseInt(params.get("start").toString());
			int pageSize = Integer.parseInt(params.get("length").toString());
			String searchText = String.valueOf(params.get("searchText"));
			String filterStore = String.valueOf(params.get("filterStore"));
			String dir = String.valueOf(params.get("order[0][dir]"));

			Store store = businessService.storeFindById(Long.valueOf(params.get("clientId").toString())).get();
			List<Review> reviews = businessService.reviewFindByStoreIdOrderByReviedOnDesc(store.getId());
			int totalRecords = reviews.size();
			responseData.put("recordsTotal", totalRecords);

			if (!StringUtils.isBlank(searchText)) {
				List<Review> filteredReviews = new ArrayList<Review>();
				filteredReviews.addAll(reviews);
				reviews.clear();
				reviews.addAll(filteredReviews.parallelStream()
						.filter(re -> re.getReview().toLowerCase().contains(searchText)).collect(Collectors.toList()));

			}

			int filteredRecords = reviews.size();
			int start = startIndex >= filteredRecords ? filteredRecords : startIndex;
			int end = (start + pageSize) >= filteredRecords ? filteredRecords : (start + pageSize);

			responseData.put("recordsFiltered", filteredRecords);
			responseData.put("data", reviews.subList(start, end));
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Error while retrieving client detail.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@RequestMapping(value = "/getQueries", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getQueries(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {

			int startIndex = Integer.parseInt(params.get("start").toString());
			int pageSize = Integer.parseInt(params.get("length").toString());
			String searchText = String.valueOf(params.get("searchText"));
			String filterStore = String.valueOf(params.get("filterStore"));
			String dir = String.valueOf(params.get("order[0][dir]"));

			Store store = businessService.storeFindById(Long.valueOf(params.get("clientId").toString())).get();
			List<Query> queries = businessService
					.queryFindByStoreIdOrderByQueriedOnDesc(Long.valueOf(params.get("clientId").toString()));
			int totalRecords = queries.size();
			responseData.put("recordsTotal", totalRecords);

			if (!StringUtils.isBlank(searchText)) {
				List<Query> filteredQueries = new ArrayList<Query>();
				filteredQueries.addAll(queries);
				queries.clear();
				queries.addAll(filteredQueries.parallelStream()
						.filter(re -> re.getQuery().toLowerCase().contains(searchText)).collect(Collectors.toList()));

			}

			int filteredRecords = queries.size();
			int start = startIndex >= filteredRecords ? filteredRecords : startIndex;
			int end = (start + pageSize) >= filteredRecords ? filteredRecords : (start + pageSize);

			responseData.put("recordsFiltered", filteredRecords);
			responseData.put("data", queries.subList(start, end));

		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Error while retrieving client detail.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

	@RequestMapping(value = "/locations", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getLocations(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {

			String reg = RMUtil.getCookie(request, "reg");
			if (!StringUtils.isEmpty(reg)) {
				ServiceArea country = locationService.getLocationById(Long.valueOf(reg));
				if (country != null)
					responseData.put("locations", locationService.getLocations(country));
				else
					responseData.put("location", null);
			}
			responseData.put("all", locationService.getLocations());
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Please select country.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/getPromotion", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getPromotion(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			List<Promotion> promotions = new ArrayList<Promotion>();
			if (params != null && params.containsKey("clientId")) {
				Long storeId = Long.valueOf(params.get("clientId").toString());
				promotions.addAll(
						businessService.promotionFindByStoreIdOrderByCreatedOnDesc(storeId).stream().filter(promote -> {
							promote.setStore(null);
							return !RMUtil.isExpired(promote.getEndDate());
						}).collect(Collectors.toList()));
			}
			responseData.put(RESULT, promotions);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Error while getting query. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/getSelectedLocation", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getSelectedLocation(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {

			ServiceArea location = locationService.getLocationById(Long.valueOf(RMUtil.getCookie(request, "loc")));
			responseData.put("selectedLocation", location);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Error while loading 	. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put("selectedLocation", null);
		}

		try {
			ServiceArea country = locationService.getLocationById(Long.valueOf(RMUtil.getCookie(request, "reg")));
			responseData.put("selectedCountry", country);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Error while loading region. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put("selectedCountry", null);
		}

		return responseData;
	}

	@RequestMapping(value = "/popularSearch", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> addPopularSearch(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			String locaionId = RMUtil.getCookie(request, "loc");
			String query = params.get("query").toString();
			popularService.addSearchKeyword(Integer.valueOf(locaionId), query);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put("data", "");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/popularSearch", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> popularSearch(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			String locaionId = RMUtil.getCookie(request, "loc");
			responseData.put("data", popularService.getSearchKeyword(Integer.valueOf(locaionId)));
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put("data", "");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/getKeys", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getKeys(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			responseData.put("mapbox", MAPBOX_SECRET_KEY);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put("mapbox", "");
		}
		return responseData;
	}

	@RequestMapping(value = "/loadResults", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> loadData(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		List<Store> stores = new ArrayList<Store>();
		try {

			String query = params.get("query").toString();
			String locaionId = RMUtil.getCookie(request, "loc");

			if (locaionId != null) {
				ServiceArea location = locationService.getLocationById(Long.valueOf(locaionId));
				stores.addAll(businessService.storeFindAllByStoreNameContainingAndActiveTrue(StringUtils.EMPTY).stream()
						.filter(store -> RMUtil.getServiceArea(store.getServiceArea(), location))
						.collect(Collectors.toList()));
			} else {
				stores.addAll(businessService.storeFindAllByStoreNameContainingAndActiveTrue("*"));
			}
			responseData.put(RESULT, query.isEmpty() ? stores : stores.stream().filter(store -> {
				final int score = this.checkStoreOfferredPlans(store, query);
				store.setStoreDetail(String.valueOf(score));
				return score > 0;
			}).sorted((s1, s2) -> Integer.valueOf(s2.getStoreDetail()).compareTo(Integer.valueOf(s1.getStoreDetail())))
					.collect(Collectors.toList()));

		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			stores.clear();
			responseData.put(RESULT, stores);
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/postAsk", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> postAsk(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) Map<String, Object> query) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			String question = query.get("question").toString();
			String firstName = query.get("firstName").toString();
			String lastName = query.get("lastName").toString();
			String postedEmail = query.get("postedEmail").toString();
			Long storeId = Long.parseLong(query.get("clientId").toString());

			Reviewer reviewerTemp = publicUserService.getReviewer(postedEmail, firstName, lastName, "");

			Query q = new Query();
			q.setQuery(question);
			q.setStore(businessService.storeFindById(storeId).get());
			q.setReply(null);
			q.setReviewer(reviewerTemp);
			q.setQueriedOn(new Timestamp(System.currentTimeMillis()));
			q = businessService.saveQuery(q);
			responseData.put(RESULT, "true");

			// add recent review
			NotificationBusiness notify = new NotificationBusiness(NOTIFICATIONS.QUERY,
					q.getReviewer().getEmail() + " asked question. ");
			notify.setStoreId(storeId);
			businessService.saveNotificationBusiness(notify);

			// send email to verify review
			emailService.sendEmailNotification(new EmailNotificationObject(q.getReviewer().getEmail(),
					q.getReviewer().getFirstName(), "Please verify your review.",
					"We have received your query for " + q.getStore().getStoreName()
							+ ".Please <a href='http://www.meaily.com/confirmq?q=" + q.getId() + "&e="
							+ q.getReviewer().getEmail()
							+ "'>Click Here </a> to confirm so that we can open it for public. "
							+ "We are doing this eveytime when we receive query so that we can maintain legitimacy.<br><br><blockquote>"
							+ q.getQuery() + "</blockquote>"));

		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Error while submitting query. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/postReview", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> postReview(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) Map<String, Object> review) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		Map<String, Object> uploadResult = new HashMap<String, Object>();

		try {
			Optional<Store> store = businessService.storeFindById(Long.valueOf(review.get("store").toString()));

			if (store.isPresent()) {
				Store finalStore = store.get();
				Review reviewObject = new Review();

				MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;

				if (multipartHttpServletRequest.getFile("reviewImage") != null) {
					uploadResult = uploadImage(store, multipartHttpServletRequest);
					if (uploadResult.toString().toLowerCase().contains("status=rejected")) {
						throw new Exception("Error while uploading image. Please review it.");
					}

				} else {
					uploadResult.put("secure_url", "");
				}

				reviewObject.setReviewImage(uploadResult.get("secure_url").toString());
				reviewObject.setReviedOn(new Timestamp(System.currentTimeMillis()));
				reviewObject.setId(null);
				reviewObject.setRating(Double.valueOf(review.get("rating").toString()));
				reviewObject.setReview(review.get("review").toString());

				Reviewer reviewerTemp = publicUserService.getReviewer(review.get("email").toString(),
						review.get("firstName").toString(), review.get("lastName").toString(), "");

				reviewObject.setReviewer(reviewerTemp);
				reviewObject.setStore(store.get());
				reviewObject = businessService.saveReview(reviewObject);

				// send email to verify review
				emailService.sendEmailNotification(new EmailNotificationObject(reviewObject.getReviewer().getEmail(),
						reviewObject.getReviewer().getFirstName(), "Please verify your review.",
						"We have received your review for " + reviewObject.getStore().getStoreName()
								+ ".Please <a href='http://www.meaily.com/confirmr?q=" + reviewObject.getId() + "&e="
								+ reviewObject.getReviewer().getEmail()
								+ "'>Click Here </a> to confirm so that we can open it for public. "
								+ "We are doing this eveytime when we receive review so that we can maintain legitimacy.<br><br><blockquote>"
								+ reviewObject.getReview() + "</blockquote>"));

				responseData.put(SUCCESS, true);
				responseData.put("message",
						"Your review has been submitted successfully. Please check your email and click on review confirmation link to display it on public portal.");

				// add recent review
				NotificationBusiness notify = new NotificationBusiness(RMUtil.NOTIFICATIONS.REVIEW,
						reviewObject.getReviewer().getEmail() + " added new review.");
				notify.setStoreId(reviewObject.getStore().getId());
				businessService.saveNotificationBusiness(notify);

			} else {
				responseData.put(SUCCESS, false);
				responseData.put("message", "Store not found.");
			}

		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Error while posting review. please try again after sometime.");
			responseData.put("message", ex.getMessage());
		}
		return responseData;
	}

	private Map<String, Object> uploadImage(Optional<Store> store,
			MultipartHttpServletRequest multipartHttpServletRequest) throws IOException {
		Map<String, Object> uploadResult;

		Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap("cloud_name", CLOUDINARY_NAME, "api_key",
				CLOUDINARY_KEY, "api_secret", CLOUDINARY_SECRET));
		Map<String, Object> map = ObjectUtils.asMap("folder", "reviews/" + store.get().getId() + "/");
		map.putIfAbsent("moderation", "aws_rek");
		uploadResult = cloudinary.uploader().upload(multipartHttpServletRequest.getFile("reviewImage").getBytes(), map);
		return uploadResult;
	}

	@GetMapping(value = "/public")
	public ModelAndView publiclanding(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		return new ModelAndView("publicsite");

	}

	@GetMapping(value = { "", "/" })
	public ModelAndView publics(HttpServletRequest request) {
		return new ModelAndView("redirect:/landing/public");
	}

	@RequestMapping(value = "/removequery", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> removequery(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Long id = Long.valueOf(params.get("id").toString());
			businessService.queryDeleteById(id);
			responseData.put(RESULT, "removed");
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/removereview", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> removereview(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Long id = Long.valueOf(params.get("id").toString());

			// Add functionality to delete image
			businessService.reviewDeleteById(id);
			responseData.put(RESULT, "removed");
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/requestPromotion", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> requestPromotion(HttpServletRequest request,
			@RequestBody(required = true) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			String storeId = params.get("storeId").toString();
			PromotionCounter promotionCounter = null;
			Optional<PromotionCounter> pCounter = businessService.promotionCounterFindByStoreId(Long.valueOf(storeId));
			if (pCounter.isPresent()) {
				promotionCounter = pCounter.get();
				promotionCounter.setLastRequest(new Timestamp(System.currentTimeMillis()));
				promotionCounter.incrRequestCounter();
			} else {
				promotionCounter = new PromotionCounter(null, Long.valueOf(storeId), 0l,
						new Timestamp(System.currentTimeMillis()));
			}
			businessService.savePromotionCounter(promotionCounter);
			responseData.put(RESULT, promotionCounter);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(RESULT, "Error while getting query. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/setRegion", method = RequestMethod.POST)
	public void setRegion(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) Map<String, Object> params) {
		try {
			response.addCookie(RMUtil.createCookie("reg", params.get("location").toString(), 60 * 60 * 24 * 30));
			response.addCookie(RMUtil.createCookie("loc", null, 0));
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			ex.getMessage();
		}
	}

	@RequestMapping(value = "/setSelectedLocation", method = RequestMethod.POST)
	public void setSelectedLocation(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) Map<String, Object> params) {
		try {
			response.addCookie(RMUtil.createCookie("loc", params.get("location").toString(), 60 * 60 * 24 * 30));
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			ex.getMessage();
		}
	}

	@RequestMapping(value = "/plansubscription", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> subscribe(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {

			Long storeId = Long.valueOf(params.get("storeId").toString());
			Long storeOfferredPlanId = Long.valueOf(params.get("storeOfferredPlanId").toString());
			String description = String.valueOf(params.get("plan_name"));
			String name = String.valueOf(params.get("plan_desc"));
			Timestamp startDate = RMUtil.getDate(params.get("startDate").toString());
			Timestamp endDate = RMUtil.getDate(params.get("endDate").toString());
			;

			String email = String.valueOf(params.get("email"));
			String firstName = String.valueOf(params.get("firstName"));
			String lastName = String.valueOf(params.get("lastName"));

			String city = String.valueOf(params.get("city"));
			String street = String.valueOf(params.get("street"));
			String province = String.valueOf(params.get("province"));
			String postal = String.valueOf(params.get("postal"));
			String country = String.valueOf(params.get("country"));

			String phoneNumber = String.valueOf(params.get("phoneNumber"));

			Double lat = Double.valueOf(params.get("lat").toString());
			Double lon = Double.valueOf(params.get("lon").toString());

			Optional<Store> storeResult = businessService.storeFindById(storeId);
			if (storeResult.isPresent()) {
				Store store = storeResult.get();
				StoreOfferredPlan storeOfferredPlan = businessService.storeOfferredPlanFindById(storeOfferredPlanId)
						.get();

				// create subscription plan
				SubscriptionPlan subscriptionPlan = new SubscriptionPlan();
				subscriptionPlan.setDescription(description);
				subscriptionPlan.setName(name);
				subscriptionPlan.setStartDate(startDate);
				subscriptionPlan.setEndDate(endDate);
				subscriptionPlan.setStoreOfferredPlan(storeOfferredPlan);
				// subscriptionPlanRepository.save(subscriptionPlan);

				// create customer
				SubscriptionCustomer subscriptionCustomer = new SubscriptionCustomer();
				Address address = new Address();
				address.setLat(lat);
				address.setLon(lon);

				address.setArea(locationService.addLocations(new ServiceArea(city, province, country)));

				address.setStreet(street);
				// address.setCountry(country);
				// address.setProvince(province);
				address.setPostal(postal);
				subscriptionCustomer.setAddress(address);
				Reviewer customer = publicUserService.getReviewer(email, firstName, lastName, phoneNumber);
				subscriptionCustomer.setCustomer(customer);
				subscriptionCustomer.setStore(store);
				subscriptionCustomer.setStatus(RMUtil.CUSTOMER_STATUS.NEW);

				subscriptionCustomer.addSubscriptions(subscriptionPlan);

				businessService.saveSubscriptionCustomer(subscriptionCustomer);

				// add recent review
				NotificationBusiness notify = new NotificationBusiness(NOTIFICATIONS.MEALPLAN, customer.getFirstName()
						+ " " + customer.getLastName() + " has sent meal plan subscription request.");
				notify.setStoreId(store.getId());
				businessService.saveNotificationBusiness(notify);

				responseData.put(SUCCESS, true);
				responseData.put("message", "Your meal plan subscription request has been submitted. "
						+ store.getStoreName() + " will reached out to you very soon.");
			} else {
				responseData.put(SUCCESS, false);
				responseData.put("message",
						"Your subscription request can not be completed. Please try again after some time.");
			}

		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
			responseData.put("message",
					"Your subscription request can not be completed. Please try again after some time.");

		}
		return responseData;
	}

	@RequestMapping(value = "/subscribe", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> subscribe(HttpServletRequest request,
			@RequestBody(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Long clientId = Long.valueOf(params.get("clientId").toString());
			String email = String.valueOf(params.get("email"));

			Optional<Store> store = businessService.storeFindById(clientId);
			if (store.isPresent() && !businessService.emailSubscriptionExistsByEmailAndStoreId(email, clientId)) {
				businessService.saveEmailSubscription(new EmailSubscription(email, store.get()));
			}
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(SUCCESS, false);
			responseData.put(RESULT, "Error while loading review. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/unsubscribe", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> unsubscribe(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			businessService.emailSubscriptionDeleteById(Long.valueOf(params.get("id").toString()));
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(SUCCESS, false);
			responseData.put(RESULT, "Error while loading review. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/driverProfile", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> driverProfile(HttpServletRequest request,
			@RequestBody DeliveryDriver deliveryDriver) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			deliveryDriver.getAddress().setArea(locationService.addLocations(deliveryDriver.getAddress().getArea()));
			businessService.saveDeliveryDriver(deliveryDriver);
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(SUCCESS, false);
			responseData.put(RESULT, "Error while updating driver profile. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/applyjob", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> applyjob(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Long jobId = Long.valueOf(params.get("jobId").toString());
			Long driverId = Long.valueOf(params.get("driverId").toString());
			if (!businessService.existsByDeliveryJobApplication(jobId, driverId)) {
				businessService.saveDeliveryJobApplication(jobId, driverId);
				responseData.put("message", "Job application submitted successfully.");
				responseData.put(SUCCESS, true);

				// add recent review
				NotificationBusiness notify = new NotificationBusiness(NOTIFICATIONS.DRIVER,
						"You have received new application for driver job.");
				notify.setJobId(jobId);
				businessService.saveNotificationBusiness(notify);

			} else {
				responseData.put("message", "You have already applied for this job.");
				responseData.put(SUCCESS, false);
			}

		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(SUCCESS, false);
			responseData.put(RESULT, "Error while applying job. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/withdrawjob", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> withdrawjob(HttpServletRequest request,
			@RequestBody Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			Long jobId = Long.valueOf(params.get("jobId").toString());
			DeliveryJobApplication jobApplication = businessService.getDeliveryJobApplication(jobId);
			jobApplication.setBusinessUser(null);
			jobApplication.setStatus(JOBSTATUS.WITHDRAWN);
			businessService.saveDeliveryJobApplication(jobApplication);

			responseData.put("message", "Job application withdrawn successfully.");
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			responseData.put(SUCCESS, false);
			responseData.put(RESULT, "Error while applying job. please try again after sometime.");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
		}
		return responseData;
	}

	@RequestMapping(value = "/jobs", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> jobSubmit(HttpServletRequest request,
			@RequestParam(required = false) Map<String, Object> params) {
		Map<String, Object> responseData = new HashMap<String, Object>();
		try {
			int startIndex = Integer.parseInt(params.get("start").toString());
			int pageSize = Integer.parseInt(params.get("length").toString());
			String searchText = String.valueOf(params.get("search"));
			String location = String.valueOf(params.get("location"));
			// String dir = String.valueOf(params.get("order[0][dir]"));
			List<JobPosting> jobs = businessService.jobPostringFindAll().stream()
					.filter(jp -> !RMUtil.isExpired(jp.getExpiryDate())).collect(Collectors.toList());
			List<JobPosting> filtered = new ArrayList<JobPosting>();
			responseData.put("recordsTotal", jobs.size());
			if (!location.isEmpty()) {
				jobs = jobs.stream()
						.filter(jp -> StringUtils.containsIgnoreCase(
								jp.getServiceArea().stream().map(sa -> sa.getCityName()).collect(Collectors.joining()),
								location))
						.collect(Collectors.toList());
				filtered.addAll(jobs);
			}

			if (!searchText.isEmpty()) {
				filtered.addAll(
						jobs.stream().filter(jp -> StringUtils.containsIgnoreCase(jp.getDescription(), searchText))
								.collect(Collectors.toList()));
			}

			if (location.isEmpty() && searchText.isEmpty()) {
				filtered.clear();
				filtered.addAll(jobs);
			}

			int filteredRecords = filtered.size();
			int start = startIndex >= filteredRecords ? filteredRecords : startIndex;
			int end = (start + pageSize) >= filteredRecords ? filteredRecords : (start + pageSize);

			responseData.put("recordsFiltered", filtered.size());
			responseData.put("message", "Showing " + end + " of " + filtered.size() + " jobs");
			responseData.put("data", filtered.stream().skip(start).limit(end).collect(Collectors.toList()));
			responseData.put(SUCCESS, true);
		} catch (Exception ex) {
			businessService.saveErrors(new Errors(ex));
			ex.printStackTrace();
			responseData.put("error", "Error while getting job posting");
			responseData.put(ERROR_MESSAGE, ex.getMessage());
			responseData.put(SUCCESS, false);
		}
		return responseData;
	}

}
